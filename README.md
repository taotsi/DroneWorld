# Drone World

## Intro

Simulation program for our lab's project, base on Airsim.

This is a windows program, trying to build or run it on other platforms might take some efforts.

## How to build

### Using VS2017 UI

Put this repo **BESIDE** the Airsim directory, not inside! Right-click the solution in vs2017 solution explorer, click "Add", find the .vcxproj file for this project and add it. Right-click this project in vs2017 solution explorer, click "Build". Remember to build this project(as well as the whole solution) in x64 mode.

By the way, if you were to add or delete .cc or .h files, the easiest way is to do this in the airsim solution. Otherwise you'll have to modify the content in  DroneWorld.vcxproj and the AirSim.sln.

### Using terminal

You can use `msbuild` or run build.cmd to build the program. But you'll have to run vcvars64.bat first, please google for where it is.

## How to run

Do operate in such an order:

### Configuration

#### settings.json

Copy `conf/settings.json` to `~/Documents/Airsim/` and override the old one. The program will find settings.json and load it automatically. So **EVERYTIME** you pull the repo, checkout if settings.json has been modified. If so, copy it again.

#### conf/config.json

It's ignored by git, you'll have to modify it to fit your environment. See config_available.json or [the config doc](docs/config.md) for possible options. You can even run your own dataset instead of Airsim using `Standalone` mode, but you'll have to modify some file IO code to fit the dataset.

### Run an AirSim program (optional)

you can download one from [AirSim's releases](https://github.com/Microsoft/AirSim/releases). Or you can build one on your own, go to Airsim's repo for more details. You must run one if you use `AirSim` mode.

### Run DroneWorld.exe

#### Argument

One argument is needed, which is the path of config.json, like this `DroneWorld.exe conf/config.json`. If you were not to give one, the default path is `conf/config.json`, so you can just run DroneWorld.exe from the rood directory of this repo.

#### Input Command

After the program finishes initialization, you can input some commands to terminal, see [the commands doc](docs/commands.md)

### Run Your Python Script (optional)

There two parts of this program. While the C++ part does the major work, the Python part serves mainly for visualization. The C++ part will open an RPC server on its initialization. And the Python part can connect to the server. All apis are defined in `python/rpc_client`. `python/test.py` shows how to use them.


## TODO

- multidrone
- animation for multi-frame plane model
- thread competition for data queues
- embed imgui
