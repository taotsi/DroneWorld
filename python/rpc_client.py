import msgpackrpc


class DataClient:
    def __init__(self):
        self.client_ = msgpackrpc.Client(msgpackrpc.Address("127.0.0.1", 8080))

    def test(self):
        return self.client_.call("test")

    def GetSuperPillarFrame(self):
        return self.client_.call("GetSuperPillarFrame")

    def GetSuperPillarCluster(self):
        return self.client_.call("GetSuperPillarCluster")

    def GetPillarCluster(self):
        return self.client_.call("GetPillarCluster")

    def GetPlanes(self):
        return self.client_.call("GetPlanes")

    def GetRefined(self):
        return self.client_.call("GetRefined")
