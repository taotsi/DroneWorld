import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from mpl_toolkits.mplot3d import Axes3D
from rpc_client import DataClient


client = DataClient()
fig = plt.figure()
ax = fig.add_subplot(111, projection="3d")
ax.axis("equal")
data = np.empty((4, 0))
lines = np.array([])


def update(idx, data, lines, client):
    pillars = np.array(client.GetPillarFrame())
    for pl in pillars:
        np.append(data, pl)
        line = ax.plot([pl[0], pl[0]], [pl[1], pl[1]], [pl[2], pl[3]], alpha=0.4)
        np.append(lines, line)
    return lines


ani = FuncAnimation(fig, update, fargs=(data, lines, client),
                    interval=500, blit=True)

plt.show()
