#pragma once
#include <thread>
#include <stdexcept>
#include <experimental/filesystem>
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <chrono>
#include <cassert>
#include <numeric>
#include <cmath>
#include <nlohmann/json.hpp>

namespace droneworld{

using json = nlohmann::json;
namespace fs = std::experimental::filesystem;

class Msg{
public:
    static void dwmark(const char* file, int line, const char* func){
        if(!is_silent()){
            std::stringstream ss;
            ss << "\033[0;45mMARK\033[0m"
                << " " << file << "(" << line << "), " << func << "\n";
            std::cout << ss.str();
        }
    }
    template<typename ...Args>
    static void dwmsg(const char* file, int line, const char* func, Args... args){
        if(!is_silent()){
            std::stringstream ss;
            ss << "\033[0;45mMESSAGE\033[0m"
                << " " << file << "(" << line << "), " << func << "\n";
            core(ss, args...);
        }
    }
    template<typename ...Args>
    static void dwerr(const char* file, int line, const char* func, Args... args){
        if(!is_silent()){
            std::stringstream ss;
            ss << "\033[0;41mERROR\033[0m"
                << " " << file << "(" << line << "), " << func << "\n";
            core(ss, args...);
        }
    }
    template<typename ...Args>
    static void dwwarn(const char* file, int line, const char* func, Args... args){
        if(!is_silent()){
            std::stringstream ss;
            ss << "\033[0;43mWARNING\033[0m"
                << " " << file << "(" << line << "), " << func << "\n";
            core(ss, args...);
        }
    }
    template<typename ...Args>
    static void dwtrap(const char* file, int line, const char* func, Args... args){
        std::stringstream ss;
        ss << "\033[0;41mTRAP\033[0m"
            << " " << file << "(" << line << "), " << func << "\n";
        core(ss, args...);
        assert(false);
    }
    static void dwpause(const char* file, int line, const char* func){
        if(!is_silent()){
            std::stringstream ss;
            ss << "\033[0;43mPAUSE\033[0m"
                << " " << file << "(" << line << "), " << func << "\n";
            std::cout << ss.str();
            char in;
            std::cin >> in;
        }
    }
    template<typename ...Args>
    static void dwi0(Args... args){
        if(!is_silent()){
            std::stringstream ss;
            ss << "\033[0;35mMESSAGE\033[0m  ";
            core(ss, args...);
        }
    }
    template<typename ...Args>
    static void dwi1(Args... args){
        if(!is_silent()){
            std::stringstream ss;
            ss << "    \033[0;35mMESSAGE\033[0m  ";
            core(ss, args...);
        }
    }
    template<typename ...Args>
    static void dwi2(Args... args){
        if(!is_silent()){
            std::stringstream ss;
            ss << "        \033[0;35mMESSAGE\033[0m  ";
            core(ss, args...);
        }
    }
    template<typename ...Args>
    static void dwinfo(Args... args){
        if(!is_silent()){
            std::stringstream ss;
            ss << "\033[0;42mINFO\033[0m  ";
            core(ss, args...);
        }
    }
    static void dwmute(){
        if(!is_locked_silent()){
            is_silent() = true;
        }
    }
    static void dwunmute(){
        if(!is_locked_silent()){
            is_silent() = false;
        }
    }
    static void dwlockmute(){
        is_silent() = true;
        is_locked_silent() = true;
    }
    static void dwunlockmute(){
        is_silent() = false;
        is_locked_silent() = false;
    }
private:
    static void core(std::stringstream &ss){
        std::cout << ss.str() << "\n";
    }
    template<typename T>
    static void core(std::stringstream &&ss, T val){
        ss << val;
        core(ss);
    }
    template<typename T, typename ...Args>
    static void core(std::stringstream &ss, T val, Args... args){
        ss << val;
        core(ss, args...);
    }
    static bool& is_silent(){
        static bool is_silent_ = false;
        return is_silent_;
    }
    static bool& is_locked_silent(){
        static bool is_locked_silent_ = false;
        return is_locked_silent_;
    }
};
#define dwmark() Msg::dwmark(__FILE__, __LINE__, __FUNCTION__)
#define dwmsg(...) Msg::dwmsg(__FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)
#define dwerr(...) Msg::dwerr(__FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)
#define dwwarn(...) Msg::dwwarn(__FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)
#define dwtrap(...) Msg::dwtrap(__FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)
#define dwpause(...) Msg::dwpause(__FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)
#define dwi0(...) Msg::dwi0(__VA_ARGS__)
#define dwi1(...) Msg::dwi1(__VA_ARGS__)
#define dwi2(...) Msg::dwi2(__VA_ARGS__)
#define dwinfo(...) Msg::dwinfo(__VA_ARGS__)

class Timer{
public:
    Timer()
        : start_{std::chrono::system_clock::time_point::min()}{
        id_ = ++timer_count();
    }
    void Clear(){
        start_ = std::chrono::system_clock::time_point::min();
    }
    bool IsStarted() const{
        return(start_.time_since_epoch() !=
            std::chrono::system_clock::duration(0));
    }
    void tStart(const char* file, int line, const char* func){
        if(!is_silent()){
            if(!is_plain_text()){
                std::stringstream ss;
                ss << "\033[0;44mTimer(" << id_ << ")\033[0m" << " started, "
                    << file << "(" << line << "), " << func << "\n";
                std::cout << ss.str();
            }
        }
        start_ = std::chrono::system_clock::now();
    }
    unsigned int tPutNow(const char* file, int line, const char* func){
        auto t = GetUs();
        if(!is_silent()){
            std::stringstream ss;
            if(!is_plain_text()){
                ss << "\033[0;44mTimer(" << id_ << ")\033[0m" << " ended,   "
                    << file << "(" << line << "), " << func << "\n"
                    << "> " << t << " us passed\n";
                std::cout << ss.str();
            }else{
                ss << t << "\n";
                std::cout << ss.str();
            }
        }
        return t;
    }
    unsigned int GetUs(){
        if(IsStarted()){
            std::chrono::system_clock::duration diff
                = std::chrono::system_clock::now() - start_;
            return static_cast<unsigned int>(
                std::chrono::duration_cast<std::chrono::microseconds>(diff).count());
        }else{
            if(!is_silent()){
                if(is_plain_text()){
                    std::stringstream ss;
                    std::cout << "\033[0;44mTimer(" << id_ << ")\033[0m"
                        << "  You have't started the timer!\n";
                }else{
                    std::cout << "You have't started the timer!\n";
                }
            }
        }
        return 0;
    }
    static void UsePlainText(bool flag){
        is_plain_text() = flag;
    }
    static void Mute(){
        is_silent() = true;
    }
    static void UnMute(){
        is_silent() = false;
    }
private:
    std::chrono::system_clock::time_point start_;
    int id_;
    static int& timer_count(){
        static int count = 0;
        return count;
    }
    static bool& is_plain_text(){
        static bool is_plain_text_ = false;
        return is_plain_text_;
    }
    static bool& is_silent(){
        static bool is_silent_ = false;
        return is_silent_;
    }
};
#define tStart() tStart(__FILE__, __LINE__, __FUNCTION__)
#define tPutNow() tPutNow(__FILE__, __LINE__, __FUNCTION__)

class Recorder{
public:
    static void NewRecord(){
            records().push_back(0.0);
    }
    // this is the only function you'll need in most cases
    template<typename T>
    static void Add(T val){
        if(!records().empty()){
            records().back() += static_cast<double>(val);
        }else{
            records().push_back(static_cast<double>(val));
        }
    }
    static double PutAvr(){
        double avr;
        if(!records().empty()){
            avr = std::accumulate(records().begin(), records().end(), 0.0)/records().size();
            records().clear();
        }else{
            avr = 0.0;
            std::cout << "No records\n";
        }
        return avr;
    }
    static double PutSum(){
        double sum;
        if(!records().empty()){
            sum = std::accumulate(records().begin(), records().end(), 0.0);
            records().clear();
        }else{
            sum = 0.0;
            std::cout << "No records\n";
        }
        return sum;
    }
    static double PutVar(){
        double var;
        if(!records().empty()){
            auto avr = std::accumulate(records().begin(), records().end(), 0.0)/records().size();
            var = std::accumulate(records().begin(), records().end(), 0.0,
                [avr = avr](auto a, auto b){return a+pow(b-avr, 2);});
            records().clear();
        }else{
            var = 0.0;
            std::cout << "No records\n";
        }
        return var;
    }
private:
    static std::vector<double>& records(){
        static std::vector<double> records_;
        return records_;
    }
};

class SettingsJsonHandler{
public:
    SettingsJsonHandler(){
        std::system("reg query \"HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders\" /v Personal > temp.txt");
        std::ifstream file{"temp.txt"};
        std::string line;
        getline(file, line); getline(file, line); getline(file, line);
        file.close();
        fs::remove("temp.txt");
        std::stringstream ss{line};
        std::string path;
        ss >> path; ss >> path; ss >> path;
        json_path_ = path + "\\AirSim\\settings.json";
        std::ifstream json_file{json_path_};
        json_file >> json_data_;
        json_file.close();
    }
    ~SettingsJsonHandler(){};
    json& GetJsonData(){
        return json_data_;
    }
private:
    std::string json_path_;
    json json_data_;
};

enum class ConfigMode{
    kOnline,
    kOffline,
    kRecord,
    kKitti,         // NOTE: this was actually for kitti dataset
    kIdle
};
std::ostream& operator<<(std::ostream &os, ConfigMode mode);
enum class OfflineMode{
    kAirSim,
    kZed
};
enum class PlayMode{
    kAuto,
    kSingleFrame
};
std::ostream& operator<<(std::ostream &os, PlayMode mode);

class Config{
public:
    Config(){
        Init("conf/config.json");
    }
    Config(std::string config_path){
        Init(config_path);
    }
    ~Config() = default;
    Config(const Config &other) = default;
    Config& operator=(const Config &other) = default;
    Config(Config &&other) = default;
    Config& operator=(Config &&other) = default;

    ConfigMode mode() const{
        return mode_;
    }
    double focus() const{
        return focus_;
    }
    double baseline() const{
        return baseline_;
    }
    int width() const{
        return width_;
    }
    int height() const{
        return height_;
    }
    double center_u() const{
        return center_u_;
    }
    double center_v() const{
        return center_v_;
    }
    int stixel_width() const{
        return stixel_width_;
    }
    float online_speed() const{
        return online_speed_;
    }
    PlayMode play_mode() const{
        return play_mode_;
    }
    int start_idx() const{
        return start_idx_;
    }
    int stop_idx() const{
        return stop_idx_;
    }
    std::string path_left() const{
        return path_to_images_left_;
    }
    std::string path_right() const{
        return path_to_images_right_;
    }
    std::string path_index() const{
        return path_to_index_;
    }
    std::string path_pfm() const{
        return path_pfm_;
    }
    std::vector<std::array<float, 3>> record_path() const{
        return record_path_;
    }
    std::vector<std::array<float, 3>> start_path() const{
        return start_path_;
    }
    float record_speed() const{
        return record_speed_;
    }
    int record_fps() const{
        return record_fps_;
    }
    std::string record_path_image() const{
        return record_path_image_;
    }
    std::string record_path_depth() const{
        return record_path_depth_;
    }
    std::string record_path_state() const{
        return record_path_state_;
    }
    OfflineMode offline_mode() const{
        return offline_mode_;
    }
    std::string offline_path_image() const{
        return offline_path_image_;
    }
    std::string offline_path_depth() const{
        return offline_path_depth_;
    }
    std::string offline_path_state() const{
        return offline_path_state_;
    }
private:
    /* data */
    json json_data_;
    ConfigMode mode_ = ConfigMode::kOnline;
    double focus_;
    double baseline_;
    int width_;
    int height_;
    double center_u_;
    double center_v_;
    int stixel_width_;

    PlayMode play_mode_ = PlayMode::kAuto;
    int start_idx_;
    int stop_idx_;

    float online_speed_;

    std::vector<std::array<float, 3>> record_path_; // flight path
    std::vector<std::array<float, 3>> start_path_;
    float record_speed_;
    int record_fps_;
    std::string record_path_image_;
    std::string record_path_depth_;
    std::string record_path_state_;

    OfflineMode offline_mode_;
    std::string offline_path_image_;
    std::string offline_path_depth_;
    std::string offline_path_state_;

    // TODO: names need to be corrected
    std::string path_to_images_left_ = "../left/";
    std::string path_to_images_right_ = "../right/";
    std::string path_to_index_ = "../";
    std::string path_pfm_ = "../";

    /* methods */
    void Init(std::string config_path){
        if(!fs::exists(config_path)){
            dwtrap(config_path, " is not found!");
        }
        std::ifstream json_file{config_path};
        if(json_file.is_open()){
            json_file >> json_data_;
            json_file.close();
        }else{
            dwtrap();
        }
        mode_ = ConfigMode::kIdle;
        if(!json_data_["Mode"].empty()){
            if(json_data_["Mode"] == "Online"){
                mode_ = ConfigMode::kOnline;
            }else if(json_data_["Mode"] == "Offline"){
                mode_ = ConfigMode::kOffline;
            }else if(json_data_["Mode"] == "Record"){
                mode_ = ConfigMode::kRecord;
            }else if(json_data_["Mode"] == "Kitti"){
                mode_ = ConfigMode::kKitti;
            }
        }else{
            dwtrap();
        }
        switch(mode_){
            case ConfigMode::kOnline:{
                focus_ = 0.5;
                baseline_ = 0.25;
                SettingsJsonHandler settings;
                json &settings_json = settings.GetJsonData()
                    ["CameraDefaults"]["CaptureSettings"];
                for(auto &it : settings_json){
                    if(it["ImageType"] == 4){
                        if(!it["Width"].empty()){
                            width_ = it["Width"];
                        }
                        if(!it["Height"].empty()){
                            height_ = it["Height"];
                        }
                        break;
                    }
                }
                LoadJsonItem(json_data_["Online"]["StixelWidth"], stixel_width_, "");
                LoadJsonItem(json_data_["Online"]["Speed"], online_speed_, "");
                auto &json_play_mode = json_data_["PlayMode"];
                if(!json_play_mode.empty()){
                    if(json_play_mode == "Auto"){
                        play_mode_ = PlayMode::kAuto;
                    }else if(json_play_mode == "SingleFrame"){
                        play_mode_ = PlayMode::kSingleFrame;
                    }else{
                        dwtrap("Set play mode please! Auto or SingleFrame");
                    }
                }else{
                    dwtrap("Set play mode please! Auto or SingleFrame");
                }
                break;
            }
            case ConfigMode::kOffline:{
                auto &json_play_mode = json_data_["PlayMode"];
                if(!json_play_mode.empty()){
                    if(json_play_mode == "Auto"){
                        play_mode_ = PlayMode::kAuto;
                    }else if(json_play_mode == "SingleFrame"){
                        play_mode_ = PlayMode::kSingleFrame;
                    }else{
                        dwtrap("Set play mode please! Auto or SingleFrame");
                    }
                }else{
                    dwtrap("Set play mode please! Auto or SingleFrame");
                }
                if(!json_data_["Offline"].empty()){
                    if(!json_data_["Offline"]["Mode"].empty()){
                        if(json_data_["Offline"]["Mode"] == "AirSim"){
                            offline_mode_ = OfflineMode::kAirSim;
                        }else if(json_data_["Offline"]["Mode"] == "Zed"){
                            offline_mode_ = OfflineMode::kZed;
                        }else{
                            dwtrap("no such a offline mode!");
                        }
                    }
                    LoadJsonItem(json_data_["Offline"]["StixelWidth"], stixel_width_, "");
                    LoadJsonItem(json_data_["Offline"]["Width"], width_, "");
                    LoadJsonItem(json_data_["Offline"]["Height"], height_, "");
                    LoadJsonItem(json_data_["Offline"]["Focus"], focus_, "");
                    LoadJsonItem(json_data_["Offline"]["Baseline"], baseline_, "");
                    LoadJsonItem(json_data_["Offline"]["CenterU"], center_u_, "");
                    LoadJsonItem(json_data_["Offline"]["CenterV"], center_v_, "");
                    LoadJsonItem(json_data_["Offline"]["ImagePath"], offline_path_image_, "");
                    LoadJsonItem(json_data_["Offline"]["DepthPath"], offline_path_depth_, "");
                    LoadJsonItem(json_data_["Offline"]["StatePath"], offline_path_state_, "");
                    LoadJsonItem(json_data_["Offline"]["Focus"], focus_, "");
                    LoadJsonItem(json_data_["Offline"]["Baseline"], baseline_, "");
                    LoadJsonItem(json_data_["Offline"]["StartIndex"], start_idx_, "");
                    LoadJsonItem(json_data_["Offline"]["StopIndex"], stop_idx_, "");
                }else{
                    dwtrap();
                }
                break;
            }
            case ConfigMode::kRecord:{
                if(!json_data_["Record"].empty()){
                    auto &path = json_data_["Record"]["Path"];
                    if(!path.empty()){
                        for(const auto &point : path){
                            record_path_.push_back({point[0], point[1], point[2]});
                        }
                    }else{
                        dwwarn("No Path set for Record mode in config.json");
                    }
                    auto &start = json_data_["Record"]["StartPath"];
                    if(!path.empty()){
                        for(const auto &p : start){
                            start_path_.push_back({p[0], p[1], p[2]});
                        }
                    }else{
						start_path_ = {{0, 0, 2}};
                        dwwarn("No StartPath set for Record mode in config.json, use [[0, 0, 2]] by default");
                    }
                    LoadJsonItem(json_data_["Record"]["Speed"], record_speed_,
                        "No velocity set for Record mode in config.json");
                    LoadJsonItem(json_data_["Record"]["Width"], width_,
                        "No Width set for Record mode in config.json");
                    LoadJsonItem(json_data_["Record"]["Height"], height_,
                        "No Height set for Record mode in config.json");
                    LoadJsonItem(json_data_["Record"]["FPS"], record_fps_,
                        "No FPS set for Record mode in config.json");
                    LoadJsonItem(json_data_["Record"]["ImagePath"], record_path_image_,
                        "No ImagePath set for Record mode in config.json");
                    LoadJsonItem(json_data_["Record"]["DepthPath"], record_path_depth_,
                        "No DepthPath set for Record mode in config.json");
                    LoadJsonItem(json_data_["Record"]["StatePath"], record_path_state_,
                        "No StatePath set for Record mode in config.json");
                }else{
                    dwtrap("No configurations for Record mode in config.json");
                }
                break;
            }
            // case ConfigMode::kKitti:{
            //     if(!json_data_["Kitti"].empty()){
            //         LoadJsonItem(json_data_["Kitti"]["PathToImageLeft"], path_to_images_left_, "");
            //         LoadJsonItem(json_data_["Kitti"]["PathToImageRight"], path_to_images_right_, "");
            //         LoadJsonItem(json_data_["Kitti"]["PathToIndex"], path_to_index_);
            //         // and it goes on...
            //     }else{
            //         dwtrap("No configurations for Kitti mode in config.json");
            //     }
            //     break;
            // }
            default:{
                dwtrap("No such a config mode!");
            }
        }
    }
    // TODO: set necessary default values
    template<typename Dst, typename Info, typename ...Args>
    void LoadJsonItem(json &src, Dst &dst, Info &&info, Args& ...args){
        if(!src.empty()){
            dst = src;
        }else{
            dwtrap(std::forward<decltype(info)>(info));
        }
    }
};

} // namespace droneworld