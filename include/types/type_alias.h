#pragma once
#include <vector>
#include <utility>

namespace droneworld {

class Pillar;
class SuperPillar;
class KdePeak;

using DispFrame = std::vector<std::vector<double>>;
using KdeFrame = std::vector<std::vector<double>>;
using KdePeakFrame = std::vector<std::vector<KdePeak>>;

using Pillars = std::vector<Pillar>;
using PillarCluster = std::vector<Pillars>;

using RpcPillar = std::vector<double>;
using RpcPillars = std::vector<RpcPillar>;
using RpcPillarCluster = std::vector<RpcPillars>;

using SuperPillars = std::vector<SuperPillar>;
using SuperPillarCluster = std::vector<SuperPillars>;

using RpcSuperPillar = std::vector<std::pair<double, double>>;
using RpcSuperPillars = std::vector<RpcSuperPillar>;
using RpcSuperPillarCluster = std::vector<RpcSuperPillars>;

using SuperPillarFrame = std::vector<std::vector<SuperPillar>>;

using RpcSuperPillarFrame = std::vector<RpcSuperPillar>;

} // namespace droneworld
