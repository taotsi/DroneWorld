#pragma once
#include <vector>
#include <cmath>
#include <utility>
#include <algorithm>
#include <iomanip>
#include <iostream>

namespace droneworld{


// LEGACY: this class will be deprecated as soon as
//         CompactPlaneComponent::SplitMerge() has
//         no dependence on it
struct BlockedIndex {
	BlockedIndex(int range_length){
		index_.push_back(std::pair<int, int>(0, 0));
		index_.push_back(std::pair<int, int>(
			range_length - 1, range_length - 1));
	}
	BlockedIndex(int start, int end){
		index_.push_back(std::pair<int, int>(0, start));
		index_.push_back(std::pair<int, int>(end, end));
	}
	/* data */
	std::vector<std::pair<int, int>> index_;
	/* methods */
	void AddSegment(int start, int end){
		if(start > end){
			std::cout << "wrong input for BlockedIndex.AddSegment(), swapped start and end already\n";
			auto temp = start;
			start = end;
			end = temp;
		}
		int idx_l = 0;
		int idx_r = static_cast<int>(index_.size()) - 1;
		int idx_mid = (idx_l + idx_r) / 2;
		if(index_.empty()){
			index_.push_back(std::pair<int, int>(start, end));
			return;
		}
		else if(start < index_[0].first){
			index_.insert(index_.begin(), std::pair<int, int>(start, end));
			return;
		}else if(start > index_[index_.size() - 1].first){
			index_.push_back(std::pair<int, int>(start, end));
			return;
		}else{
			while (idx_l < idx_r - 1){
				if(start < index_[idx_mid].first){
					idx_r = idx_mid;
					idx_mid = (idx_l + idx_r) / 2;
				}else if(start > index_[idx_mid].first){
					idx_l = idx_mid;
					idx_mid = (idx_l + idx_r) / 2;
				}else{ // start == index_[idx_mid].first
					idx_r = idx_mid;
					break;
				}
			}
		}
		std::pair<int, int> pair;
		if(start >= index_[idx_r].first
			|| start < index_[idx_r - 1].second){
			return;
		}else{
			if(end > index_[idx_r].first){
				pair = std::pair<int, int>{ start, index_[idx_r].first };
			}else{
				pair = std::pair<int, int>{ start, end };
			}
		}
		index_.insert(index_.begin() + idx_r, pair);
	}
	bool IsFull(){
		for (auto i = 1; i < index_.size(); i++){
			if(index_[i - 1].second < index_[i].first){
				return true;
			}
		}
		return false;
	}
	void Print(){
		for (auto seg : index_){
			std::cout << "(" << seg.first << ", " << seg.second << ")  ";
		}
		std::cout << std::endl;
	}
	std::pair<int, int>& operator[](unsigned int pos){
		if(pos >= index_.size()){
			std::cout << "BlockedIndex: index out of bounds" << std::endl;
			return index_[index_.size() - 1];
		}
		return index_[pos];
	}
	int size(){
		return static_cast<int>(index_.size());
	}
};

} // namespace droneworld