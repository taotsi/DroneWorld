#pragma once
#include <vector>
#include "types/type_alias.h"
#include "types/pillar.h"
#include "droneworld_utility.h"

namespace droneworld {

enum class MatchStat {
	kNotMatch,
	kMatchPartial,
	kMatch
};
std::ostream& operator<<(std::ostream& os, MatchStat stat);

class Complement {
public:
	Complement();
	Complement(double top, double bottom);
	double height();
	double top() const;
	void top(double t);
	double bottom() const;
	void bottom(double b);
	void SetEnds(double top, double bottom);
	void match_stat(MatchStat stat);
	MatchStat match_stat();
	friend std::ostream& operator<<(std::ostream& os, Complement &comp);
private:
	double top_ = 0;
	double bottom_ = 0;
	MatchStat match_stat_ = MatchStat::kNotMatch;
};

// NOTE:
// Only StixelMask has access to constructing Superpillar,
// And StixelMask makes sure that all SuperPillars constructed
// are valid, with two virtual ends.
class SuperPillar {
public:
	~SuperPillar() = default;
	SuperPillar(const SuperPillar &other) = default;
	SuperPillar& operator=(const SuperPillar &other) = default;
	SuperPillar(SuperPillar &&other) = default;
	SuperPillar& operator=(SuperPillar &&other) = default;
	double x() const;
	void x(double x);
	double y() const;
	void y(double y);
	const std::vector<std::pair<double, double>>& z();
	const std::vector<Complement>& complements();
	void AddZPair(double zh, double zl);
	void AddZPair(std::pair<double, double> z);
	void PushComplement(double top, double bottom);
	void FillComplement(int index);
	void NarrowComplement(int index, double top, double bottom);
	void clear();
	void SetVirtualEnds(double top, double bottom);
	// TODO: rename these two to virtual_top() and virtual_bottom,
	//       and remove top_ and bottom_
	double top();
	double bottom();
	double real_top();
	void real_top(double top);
	double real_bottom();
	void real_bottom(double bottom);
	bool empty();
	int NPillar();
	int NComplement();
	void SpawnPillars(Pillars &pls);
	void SetBeyondEnds(bool is_top, bool is_bottom);
	bool is_top_beyond();
	void is_top_beyond(bool flag);
	bool is_bottom_beyond();
	void is_bottom_beyond(bool flag);
	friend std::ostream& operator<<(std::ostream &os, const SuperPillar &sp);
private:
	friend class StixelMask;
	// friend class DynamicWindow;
	friend class SuperDynamicWindow;
	SuperPillar();
	double x_ = 0, y_ = 0;
	std::vector<std::pair<double, double>> z_;
	std::vector<Complement> complements_;
	// TODO: these two member will be deprecated later
	double top_ = 0;
	double bottom_ = 0;
	bool is_top_beyond_ = false;
	bool is_bottom_beyond_ = false;
};

} // namespace droneworld
