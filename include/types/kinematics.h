#pragma once
#include <vector>
#include <cmath>
#include <utility>
#include <algorithm>
#include <iomanip>
#include <iostream>
#include "types/super_pillar.h"

namespace droneworld{

struct Point2D {
	Point2D(){};
	Point2D(double x, double y)
		: x_(x), y_(y){};
	double x_ = 0.0;
	double y_ = 0.0;
};

struct Point3D {
	Point3D(){};
	Point3D(double x, double y, double z)
		: x_(x), y_(y), z_(z){};
	Point3D(Point2D &p, double z)
		:x_(p.x_), y_(p.y_), z_(z){};
	Point3D(const Point3D &other) = default;
	Point3D& operator=(const Point3D &other) = default;
	Point3D(Point3D &&other) = default;
	Point3D& operator=(Point3D &&other) = default;
	~Point3D(){};
	bool operator==(const Point3D &rhs){
		return x_ == rhs.x_ && y_ == rhs.y_ && z_ == rhs.z_;
	}
	/* methods */
	void Set(double x, double y, double z){
		x_ = x;
		y_ = y;
		z_ = z;
	}
	void Reset(){
		x_ = 0.0;
		y_ = 0.0;
		z_ = 0.0;
	}
	void Print(){
		std::cout << "( " << x_ << ", " << y_ << ", " << z_ << " )\n";
	}
	double X_() {
		return x_;
	}
	double Y_() {
		return y_;
	}
	double Z_() {
		return z_;
	}
	Point3D SafeScale(double s){
		return Point3D{x_*s, y_*s, z_*s};
	}
	std::vector<double> GetCoor(){
		return std::vector<double>{x_, y_, z_};
	}
	/* data */
	double x_ = 0.0;
	double y_ = 0.0;
	double z_ = 0.0;
};

struct Quaternion {
	Quaternion() {};
	Quaternion(double w, double x, double y, double z)
		:w_(w), x_(x), y_(y), z_(z){};
	double w_ = 0;
	double x_ = 0;
	double y_ = 0;
	double z_ = 0;
};
struct EulerAngle {
	EulerAngle(){};
	EulerAngle(double pitch, double yaw, double roll)
		: pitch_(pitch), yaw_(yaw), roll_(roll){};
	EulerAngle(Quaternion &quat){
		pitch_ = asin(2 * quat.w_*quat.y_ - 2 * quat.x_*quat.z_);
		yaw_ = atan2(2 * quat.w_*quat.z_,
			1 - 2 * quat.y_*quat.y_ - 2 * quat.z_*quat.z_);
		roll_ = atan2(2 * quat.w_*quat.x_,
			1 - 2 * quat.x_*quat.x_ - 2 * quat.y_*quat.y_);
	}
	/* data */
	double pitch_ = 0.0;
	double yaw_ = 0.0;
	double roll_ = 0.0;
};

struct GPSPosition {
	GPSPosition() {};
	GPSPosition(double latitude, double longtitude)
		: latitude_(latitude), longtitude_(longtitude) {};
	GPSPosition(const GPSPosition &other) {
		latitude_ = other.latitude_;
		longtitude_ = other.longtitude_;
	}
	GPSPosition& operator=(const GPSPosition &other) {
		latitude_ = other.latitude_;
		longtitude_ = other.longtitude_;
		return *this;
	}
	double latitude_;
	double longtitude_;
};
Point3D ImgToCameraCoor(double disp_normalized, int u, int v,
    int u0, int v0, double baseline, double focus, double width);
Point3D CameraToWorldCoor(Point3D p_pos_camera_coor,
	Point3D camera_pos_world_coor, EulerAngle angle);
void ImgToWorldCoor(SuperPillar &sp, double disp_normalized,
	int u, std::vector<std::pair<int, int>> v, int u0, int v0,
	double baseline, double focus, double width,
	Point3D camera_pos, EulerAngle angle);
Point3D ImgToWorldCoor(double disp_normalized, int u, int v, int u0, int v0,
	double baseline, double focus, double width,
	Point3D camera_pos, EulerAngle angle);
} // namespace droneworld
