#pragma once
#include <iostream>

namespace droneworld{

class KdePeak {
public:
	KdePeak(){};
	/* data */
	// pos in a scaled disparity frame
	int window_height_ = 2;
	double mean_ = 0.0;
	double right_ = 0.0;
	double left_ = 0.0;
	/* methods */
	void SetVal(double mean, double right, double left){
		mean_ = mean;
		if(right < left){
			auto temp = right;
			right = left;
			left = temp;
		}
		right_ = right;
		left_ = left;
	}
	void SetWindowHeight(int height){
		window_height_ = height > 1 ? height : 2;
	}
	void Print(){
		std::cout << "mean = " << mean_ << ", right = " << right_ << ", left = " << left_ << "\n";
	}
};

} // namespace droneworld