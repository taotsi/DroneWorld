#pragma once
#include <stack>
#include <iostream>
#include <cmath>
#include "types/super_pillar.h"
#include "types/type_alias.h"
#include "droneworld_utility.h"

namespace droneworld {

class DynamicWindow{
public:
    DynamicWindow();
    DynamicWindow(double top, double bottom, int left, double lx, double ly);
    DynamicWindow(const DynamicWindow &other) = default;
    DynamicWindow& operator=(const DynamicWindow &other) = default;
    DynamicWindow(DynamicWindow &&other) = default;
    DynamicWindow& operator=(DynamicWindow &&other) = default;
    ~DynamicWindow();
    double height();
    double width();
    double top();
    void top(double t);
    double bottom();
    void bottom(double b);
    int left();
    int right();
    double lx();
    // void lx(double x);
    double ly();
    // void ly(double y);
    double rx();
    // void rx(double x);
    double ry();
    // void ry(double y);
    void TopDescend(double step);
    void BottomAscend(double step);
    void MarchRight(int r, double rx, double ry);
    void MarchRight(double rx, double ry);
    bool IsFixed();
    void Fix();
    MatchStat MatchComplement(Complement &comp);
    friend std::ostream& operator<<(std::ostream &os, const DynamicWindow &dw);
private:
    MatchStat MatchComplement(double top, double bottom);
    void right(int r);
    // friend class SuperDynamicWindow;
    double top_ = 0;
    double bottom_ = 0;
    int left_ = 0;
    int right_ = 0;
    double lx_ = 0;
    double ly_ = 0;
    double rx_ = 0;
    double ry_ = 0;
    bool is_fixed_ = false;
};

class SuperDynamicWindow{
public:
    SuperDynamicWindow();
    SuperDynamicWindow(double safe_hight, double safe_width);
    SuperDynamicWindow(const SuperDynamicWindow&) = default;
    SuperDynamicWindow(SuperDynamicWindow&&) = default;
    SuperDynamicWindow& operator=(const SuperDynamicWindow&) = default;
    SuperDynamicWindow& operator=(SuperDynamicWindow&&) = default;
    ~SuperDynamicWindow();
    void clear();
    int size();
    void SetSafeParam(double height, double width);
    void Cluster(SuperPillars &src, PillarCluster &dst, double epsilon=0.1);
    friend std::ostream& operator<<(std::ostream &os, const SuperDynamicWindow &sdw);
private:
    void SpawnWindwos(int index, SuperPillar &sp);
    void ScanSuperPillar(int index, SuperPillar &sp);
    void RefineSuperPillar(int index, SuperPillar &sp);
    void ClusterPillars(std::vector<std::vector<Pillar>> &src,
        PillarCluster &dst, double epsilon=0.1);
    void SortWindows();
    void MergeWindows(double epsilon);
    double safe_height_ = 0.5;
    double safe_width_ = 1;
    std::vector<DynamicWindow> windows_;
};
} // namespace droneworld