#pragma once
#include <vector>
#include <cmath>
#include <utility>
#include <algorithm>
#include <iomanip>
#include <iostream>
#include "types/pillar.h"
#include "types/kinematics.h"

namespace droneworld{

class Line2dFitted {
public:
	Line2dFitted();
	Line2dFitted(std::vector<Pillar> &pillars);
	Line2dFitted(std::vector<Pillar> &pillars, int start, int end);
	Line2dFitted(std::vector<double> &vec, int start, int end);
	Line2dFitted(double x1, double y1, double x2, double y2);
	Line2dFitted(Point3D &p1, Point3D &p2);
	Line2dFitted(Pillar &pl1, Pillar &pl2);
	Line2dFitted(const Line2dFitted &other);
	// Line2dFitted(Line2dFitted &&other);
	Line2dFitted& operator=(const Line2dFitted &other);
	// Line2dFitted& operator=(Line2dFitted &&other);
	~Line2dFitted();
	void AddPoint(double x, double y);
	void AddPoints(std::vector<Pillar> &pillars, int start, int end);
	double EstimateY(double x, double y);
	double EstimateX(double x, double y);
	double SignedDist(double x, double y);
	double Dist(double x, double y);
	double DistClipped(double x, double y, double epsilon = 0.3);
	double slope();
	double intercept();
	void update(double n_new, double x_avr_new,
		double y_avr_new, double xy_avr_new, double xx_avr_new);
	void Reset();
	void Reset(double x1, double y1, double x2, double y2);
	void Reset(std::vector<Pillar> &pillars, int start, int end);
	void Reset(std::vector<double> vec, int start, int end);
	int n();
	double x_avr();
	double y_avr();
	double xx_avr();
	double xy_avr();
	double a();
	double b();
	double c();
	void Print();

private:
	/* data */
	double n_ = 0.0;
	double a_ = 0.0;
	double b_ = 0.0;
	double c_ = 0.0;
	double sqrt_a2b2 = 0;
	double x_avr_ = 0.0;
	double y_avr_ = 0.0;
	double xx_avr_ = 0.0;
	double xy_avr_ = 0.0;
	/* methods */
	void CalcCoef();
	bool IsZero(double x);
};

}