#pragma once
#include <iostream>
#include <string>
#include <utility>
#include <vector>
#include <map>
#include <algorithm>
#include "types/kinematics.h"
#include "types/super_pillar.h"
#include "droneworld_utility.h"

namespace droneworld{

enum class MergeStat{
    kLeft,
    kRight,
    kIntersect
};
enum class DelStat{
    kLeft,
    kRight,
    kSplit,
    kTruncLeft,
    kTruncRight,
    kAll
};

class BaseMask{
public:
    BaseMask();
    BaseMask(int start, int end);
    BaseMask(int range_len);
    BaseMask(const BaseMask &other) = default;
    BaseMask(BaseMask &&other) = default;
    BaseMask& operator=(const BaseMask &other) = default;
    BaseMask& operator=(BaseMask &&other) = default;
    virtual ~BaseMask();
    void Mark(int left, int right);
    void UnMark(int left, int right);
    int start() const;
    void start(int s);
    int end() const;
    void end(int e);
    bool empty();
    size_t size();
    virtual void clear();
    std::pair<int, int> operator[](int i);
    BaseMask& operator+=(BaseMask &other);
    friend BaseMask operator+(BaseMask &a, BaseMask &b);
    friend std::ostream& operator<<(std::ostream &os, const BaseMask &mask);
    /* data */
    std::vector<std::pair<int, int>> mask_;
private:
    int start_ = 0;
    int end_ = 0;
    MergeStat Merge(std::pair<int, int> pair, int &left, int &right);
    DelStat Del(std::pair<int, int> pair, int left, int right);
};

class ObjectMask : public BaseMask{
public:
    ObjectMask();
    ObjectMask(int start, int end,  double d=0);
    ObjectMask(int range_len, double d=0);
    ~ObjectMask();
    double disp() const;
    void disp(double d);
    void clear();
private:
    double disp_ = 0;
};

// NOTE: one StixelMask can only store masks from
//       one column of pixels
class StixelMask{
public:
    StixelMask(int start, int end);
    StixelMask(int range_len);
    StixelMask(const StixelMask &other) = default;
    StixelMask(StixelMask &&other) = default;
    StixelMask& operator=(const StixelMask &other) = default;
    StixelMask& operator=(StixelMask &&other) = default;
    ~StixelMask();

    void MarkSky(int left, int right);
    void MarkGnd(int left, int right);
    void MarkWall(double disp, int left, int right);
    void MarkObj(double disp, int left, int right);
    void UnMarkSky(int left, int right);
    void UnMarkGnd(int left, int right);
    void UnMarkWall(double disp, int left, int right);
    void UnMarkObj(double disp, int left, int right);
    void ChangeWallDisp(double old_disp, double new_disp);
    void ChangeObjDisp(double old_diso, double new_disp);
    std::vector<double> ListWall();
    std::vector<double> ListObj();
    int FindWall(double disp);
    int FindObj(double disp);

    void SpawnSuperPillars(std::vector<SuperPillar> &nest,
        int u0, int v0, double baseline, double focus, int width, int height,
        Point3D camera_pos, EulerAngle angle);
    void SpawnPointCloud(std::vector<Point3D> &dst, int u0, int v0, double baseline,
        double focus, int width, int height, Point3D camera_pos, EulerAngle angle,
        std::vector<std::vector<double>> &disp_frame, int stixel_width);

    int start();
    int end();
    void u(int u);
    int u();
    friend std::ostream& operator<<(std::ostream &os, const StixelMask &masks);

    /* some stupid interation interfaces */
    // NOTE: the interation is based on all kinds of masks, aka all_masks_
    int it_i();
    int it_m();
    int ItInit();
    bool ItIsIn();
    bool ItIsIn(int i);
    int ItInc();
    int ItInc(int step);
    int ItJump();
    int ItBlockLen();

    const BaseMask& all_mask();
    const BaseMask& sky();
    const BaseMask& gnd();
    const ObjectMask& wall(double disp);
    const ObjectMask& obj(double disp);
    void clear();
    bool empty();
private:
    /* data */
    int start_ = 0, end_ = 0;
    // NOTE: image coordinate u(horizontally), which means
    //       one StixelMask can only store masks from one column of pixels
    int u_ = -1;
    // well, won't work for parallel code
    int it_i_ = 0;
    int it_m_ = 0;
    BaseMask all_masks_;
    BaseMask sky_;
    BaseMask gnd_;
    std::map<double, ObjectMask> wall_;
    std::map<double, ObjectMask> obj_;

    /* methods */
    void RefreshAllMasks();
};

} // namespace droneworld