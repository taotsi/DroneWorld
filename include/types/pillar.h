#pragma once
#include <vector>
#include <cmath>
#include <utility>
#include <algorithm>
#include <iomanip>
#include <iostream>
#include "types/kinematics.h"

namespace droneworld{

class Pillar {
public:
	Pillar();
	Pillar(double x, double y, double z1, double z2);
	Pillar(const Pillar &other);
	Pillar(Pillar &&other);
	Pillar& operator=(const Pillar &other);
	Pillar& operator=(Pillar &&other);
	double DistHorizon(Pillar &other);
	void x(double x);
	double x();
	void y(double y);
	double y();
	void z1(double z);
	double z1();
	void z2(double z);
	double z2();
	friend std::ostream& operator<<(std::ostream &os, const Pillar &pl);
	std::vector<double> GetCoor();
private:
	/* data */
	double x_ = 0.0;
	double y_ = 0.0;
	double z1_ = 0.0;
	double z2_ = 0.0;
};

class SinglePillarCluster {
public:
	SinglePillarCluster(Pillar const &pillar){
		Push(pillar);
	}
	~SinglePillarCluster(){};
	/* methods */
	Pillar operator[](int pos) const {
		return data_[pos];
	}
	void Push(Pillar pl){
		data_.push_back(pl);
		if(!z1_vec_.empty()){
			z1_mean_ += (pl.z1() - z1_mean_) / static_cast<double>(z1_vec_.size() + 1);
			z2_mean_ += (pl.z2() - z2_mean_) / static_cast<double>(z2_vec_.size() + 1);
		}else{
			z1_mean_ = pl.z1();
			z2_mean_ = pl.z2();
		};
		z1_vec_.push_back(pl.z1());
		z2_vec_.push_back(pl.z2());
	}
	auto back(){ return data_.back(); }
	int size() const { return static_cast<int>(data_.size()); }
	double z1_mean(){ return z1_mean_; }
	double z2_mean(){ return z2_mean_; }
	bool empty(){ return data_.empty(); }
	double z_max() const {
		return *(std::max_element(z2_vec_.begin(), z2_vec_.end()));
	}
	double z_min() const {
		return *(std::min_element(z1_vec_.begin(), z1_vec_.end()));
	}
private:
	/* data */
	std::vector<Pillar> data_;
	// not a good design, redundant data. just in order to make fast
	std::vector<double> z1_vec_;
	std::vector<double> z2_vec_;
	double z1_mean_;
	double z2_mean_;
};

class PillarClustersHorizon {
public:
	PillarClustersHorizon(){};
	~PillarClustersHorizon(){};
	/* data */
	std::vector<SinglePillarCluster> data_;
	/* methods */
	SinglePillarCluster& operator[](int pos){
		return data_[pos];
	}
	// basic cluster
	void BringIn(Pillar pillar, double xy_max, double z_max){
		if(!data_.empty()){
			bool is_brought_in = false;
			int n_cluster = static_cast<int>(data_.size());
			for (auto i = n_cluster - 1; i >= 0; i--){
				double dist = pow(data_[i].back().x() - pillar.x(), 2)
					+ pow(data_[i].back().y() - pillar.y(), 2);
				if(dist <= pow(xy_max, 2)){
					data_[i].Push(pillar);
					is_brought_in = true;
					break;
				}
			}
			if(!is_brought_in){
				data_.push_back(SinglePillarCluster{ pillar });
			}
		}else{
			data_.push_back(SinglePillarCluster{ pillar });
		}
		return;
	}
	int size(){
		return static_cast<int>(data_.size());
	}
};

} // namespace droneworld