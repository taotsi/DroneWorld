#pragma once
#include <vector>
#include <cmath>
#include <utility>
#include <algorithm>
#include <iomanip>
#include <iostream>
#include <vehicles/multirotor/api/MultirotorRpcLibClient.hpp>
#include "types/kinematics.h"
#include "types/pillar.h"
#include "types/line2dfitted.h"

typedef ImageCaptureBase::ImageResponse ImageResponse;

namespace droneworld{
// safe distance in xy direction
#define  width_refine_plane 0.2

// the minmum distance and angle if the two planes belong to the same plane
#define  width_conplane 0.5
#define  kCoplane_angle 5
// overlap in z axis thred
#define Thresh_difference_planes 0.5
enum PlanetoPlaneStatus {
	// related
	kConnected,
	kIntersection,
	kUnIntersection,
	kAdjacent,
	kCoplane,
	kCoplaneMerge,
	kInsert,
	// unrelated
	kUnrelated,

	ERROR_plane
};

class Plane {
public:
	Plane(){};
	Plane(Point3D &p1, Point3D &p2)
		: p1_(p1), p2_(p2), line_{ p1, p2 } {};
	Plane(Pillar &pl1, Pillar &pl2)
		: line_{ pl1, pl2 } {
		FromPillars(pl1, pl2);
	}
	Plane(const Plane &other) = default;
	Plane& operator=(const Plane &other) = default;
	Plane(Plane &&other) = default;
	Plane& operator=(Plane &&other) = default;
	~Plane(){};

	void FromPillars(Pillar &pl1, Pillar &pl2){
		p1_ = Point3D{ pl1.x(), pl1.y(), pl1.z1() };
		p2_ = Point3D{ pl2.x(), pl2.y(), pl2.z2() };
	}
	void FromPoints(Point3D &p1, Point3D &p2){
		p1_ = p1;
		p2_ = p2;
	}
	void Reset(){
		p1_.Reset();
		p2_.Reset();
	}
	bool IsReset(){
		Point3D p_temp;
		return p1_ == p_temp && p2_ == p_temp;
	}
	void Print(){
		std::cout << "plane: \n";
		p1_.Print();
		p2_.Print();
	}
	std::vector<std::vector<double>> GetCoor(){
		std::vector<std::vector<double>> coor{
			p1_.GetCoor(), p2_.GetCoor() };
		return coor;
	}
	Point3D p1() {
		return p1_;
	}
	Point3D p2() {
		return p2_;
	}
	// NOTE: Safe means you dont change the Plane itself, const function may be a better choice
	Plane SafeScale(double s){
		return Plane{p1_.SafeScale(s), p2_.SafeScale(s)};
	}

	Line2dFitted line_;

	friend void RefineGridPlane(std::vector<Plane> &planes_refined);
	friend void RefinePlanes(std::vector<Plane> &planes_refined,
		std::vector<Plane> &planes_to_refine);
	friend void RefinePlanesTwins(std::vector<Plane> &planes_refined,
		std::vector<Plane> &planes_to_refine);
	friend PlanetoPlaneStatus RefineTwoPlane(Plane &plane1, Plane &plane2, double command);
	friend PlanetoPlaneStatus JudgeP2P(Plane &plane1, Plane &plane2, double &i_x, double &i_y);
	friend bool IsConnectedCoplane(Plane &plane1, Plane &plane2);
	friend double PointToLine(Point3D &point, Plane  &testplane);
	friend double PointToPlane(Point3D &point, Plane  &testplane);
	friend double GetLineIntersection(Plane plane1, Plane plane2, double &i_x, double &i_y);
	friend double AngleBetweenPlanes(Plane &plane1, Plane &plane2);
	friend double AngleBetweenSmallPlanes(Plane &plane1, Plane &plane2);
	friend double FitTwoPlanes(Plane &plane1, Plane &plane2);
	friend void MergeNewPlane(Plane &plane1, Plane &plane2);
	friend void EnlargePlane(Plane &plane1, Plane &plane2);
	friend void RefineIntersectionPlanes(Plane &plane1, Plane &plane2, double &i_x, double &i_y);
	friend double RefineAdjacentPlanes(Plane &plane1, Plane &plane2);
	friend void InsertAdjacentPlanes(Plane &plane1, Plane &plane2, Plane &addplane);
	friend inline double DistancePointToPoint(Point3D &point1, Point3D &point2);
	friend inline void ChangeValue(double &value1, double &value2);
	friend inline double AverageValue(double &value1, double &value2);

private:
	/* data */
	Point3D p1_;
	Point3D p2_;
	std::vector<Plane*> left_ = std::vector<Plane*>{ nullptr };
	std::vector<Plane*> right_ = std::vector<Plane*>{ nullptr };

};

class PlaneWorld {
public:
	std::vector<Plane> data_;
	std::vector<Pillar> pillars_alone_;
};

} // namespace droneworld
