#pragma once

#include <vector>
#include <algorithm>
#include <iostream>
#include "droneworld_type.h"
#include <nlohmann/json.hpp>
#include <fstream>
#include <sstream>
#include <experimental/filesystem>
#include <opencv2/opencv.hpp>
#include "droneworld_utility.h"

// the parameters for ground extraction
#define THRESHOLD_DISTANCE_HORIZON 6.0e-1    // need to try
#define THRESHOLD_SLOPE_HORIZON    5.0e-6
// the munmun number pixels to mark object
#define MINIMUM_MUN_PIXELS_OBJ     5

// m   the threshold of distance to discretize the object
#define THRESHOLD_DISCRETIZE       1  // m
#define K_SENSOR_ERROR             0.0025
#define B_SENSOR_ERROR             0.5 // m
#define MINIMUM_HEIGHT_OBJ         0.5 // m


// sampling rate for column of image
#define STIXEL_WIDTH               7
// clustering distance threshold
#define THRESHOLD_CLUSTER_DIS      1   // m

// safe heght and width of UAV
#define SAFE_HEIGHT                0.2  // m
#define SAFE_WIDTH                 0.4  // m

// add noise on disparity image and the resulution of grid map
#define NOISE_AMP                  0.0 // m
#define RESOLUTION_GRIDMAP         0.1 // m

// refine plane parameter
#define THRESHOLD_ANGLE_COPLANE    5   // du

#define DISTANCE_MAX               20  // m
#define DISTANCE_MIN               0.5 // m

#define ERROR_FITTED_PLANE         0.1 // m
namespace droneworld{
// guass sigma = 1

// caculate error for our algorithm and contrast method
enum error_type {
	compact_plane,
	tracing_grid
};

static std::vector<double> KERNEL_GAUSS_17 = {
    0.00678462, 0.0133395, 0.0239336, 0.0392686, 0.0588726,
    0.0806656, 0.101032, 0.115629, 0.12095, 0.115629,
    0.101032, 0.0806656, 0.0588726, 0.0392686, 0.0239336,
    0.0133395, 0.00678462};

// guass sigma = 0.5
static std::vector<double> KERNEL_GAUSS_9 = {
0.00678462,0.0239336,0.0588726,0.101032,0.12095,
0.101032,0.0588726,0.0239336,0.00678462};

void RetreiveKde(std::vector<double> const &src,
    std::vector<double> &dst, double max, double min, int kde_width,
    const std::vector<double> &kernel= KERNEL_GAUSS_17);

void RetreiveKdePeak(std::vector<double> const &kde,
    std::vector<KdePeak> &peaks, double max, double min, double bound_val_weight,
    double kde_slope, double kde_y_intercept, double window_height_weight);

// FilterStatus Filter(std::vector<double> const &vec, int start, int step,
//     double mean, double min, double max, double disp_max_, double disp_min_);

FilterFlag FilterWal(std::vector<double> &stx, int start, int step,
    double mean, double min, double max, double disp_max, double disp_min);
double DispAverage(std::vector<double> &stx, int start, int end, double min, double max);
double WallDispAverage(std::vector<double> &stx, const ObjectMask& mask_wall, double min, double max);
void RefineWal(std::vector<double> &stx, KdePeak &peaks,
	StixelMask &mask, double center_v_);

void CluterSuperPillars(std::vector<std::vector<SuperPillar>> &src,
	SuperPillarCluster &dst);

void SetSuperPillarTopBottom(SuperPillarCluster &super_pillar_clusters);
void FlatWall(SuperPillarCluster &super_pillar_clusters);

Point3D TransformAirsimCoor(double x, double y, double z);
Point3D TransformGPS2Plane(GPSPosition basePoint, GPSPosition point);

void DynamicWindowCluster(std::vector<SuperPillar> &sps,
    std::vector<std::vector<Pillar>> &clusters);

void TracingPrism(ScaledDisparityFrame &disparity_frame, int width_, int height_, double disp_max_, double disp_min_,
	double focus_, double baseline_, double center_u_, double center_v_, std::vector<Plane> &plane_dst,
	int stixel_width, double noise_amp_, double resolution_gridmap_);

void SavePlaneData(std::vector<Plane> &plane_dst, std::string path);

void ErrorCaculate(ScaledDisparityFrame &disparity_frame, int width_, int height_, double disp_max_, double disp_min_,
	double focus_, double baseline_, double center_u_, double center_v_, std::vector<Plane> &plane_dst,
	double& error_ave, double radius, double& per_error_in_radius, int stixel_width, int error_type);

cv::Point WorldToImg(Point3D point, double width_, double height_,
	double focus_, double baseline_, double center_u_, double center_v_);

void DrawPlaneImage(std::vector<Plane> &planes, double height_uav,
	double width_, double height_, double focus_, double baseline_, double center_u_, double center_v_);
} // namespace droneworld


