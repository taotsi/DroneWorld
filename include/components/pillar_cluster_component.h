#pragma once
#include "components/base_component.h"
#include "droneworld_algorithm.h"
#include "droneworld_utility.h"

namespace droneworld{

class PillarClusterComponent : public BaseComponent {
public:
    PillarClusterComponent(
        std::queue<SuperPillarFrame>* super_pillar_frame_queue);
    ~PillarClusterComponent();
    void Begin();
    void Update(double delta_time);
    void QueuePop();
    // for rpclib server
    RpcSuperPillarCluster GetSuperPillarCluster(int idx=-1);
    RpcPillarCluster GetPillarCluster();
    /* data */
    std::queue<SuperPillarFrame>* super_pillar_frame_queue_;
    std::queue<SuperPillarCluster> super_pillar_cluster_queue_;
    std::queue<PillarCluster> pillar_cluster_queue_;
private:
    void RunCluster();
};
}
