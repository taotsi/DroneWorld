#pragma once
#include <mutex>
#include "components/base_component.h"

namespace droneworld{

class DronePose{
public:
    DronePose(){};
    DronePose(float x, float y, float z, double yaw, double pitch, double roll)
        : x_{x}, y_{y}, z_{z}, yaw_{yaw}, pitch_{pitch}, roll_{roll} {};
    DronePose(const DronePose&) = default;
    DronePose(DronePose&&) = default;
    DronePose& operator=(const DronePose&) = default;
    DronePose& operator=(DronePose&&) = default;
    ~DronePose(){};
    float x(){return x_;}
    float y(){return y_;}
    float z(){return z_;}
    double yaw(){return yaw_;}
    double pitch(){return pitch_;}
    double roll(){return roll_;}
private:
    float x_ = 0;
    float y_ = 0;
    float z_ = 0;
    double yaw_ = 0;
    double pitch_ = 0;
    double roll_ = 0;
};

class MovementComponent : public BaseComponent {
public:
    MovementComponent(std::shared_ptr<Config> conf);
    ~MovementComponent();
    void Begin();
    void Update(double delta_time);
    void AddPathPoint(const std::array<float, 3> &point);
    void AddPath(const std::vector<std::array<float, 3>> &path);
    void SetSpeed(float speed);
    void PrintPose();
    DronePose Pose();
    void TakeOff();
    void Land();
private:
    void MoveTest();
    void MoveThreadMain();
    void ResetPath(const std::queue<std::array<float, 3>> &new_path);
    /* data */
    msr::airlib::MultirotorRpcLibClient client_;
    std::thread move_thread_;
    std::shared_ptr<Config> config_;

    float speed_;
    std::queue<std::array<float, 3>> path_to_go_;
    std::vector<std::array<float, 3>> start_path_;
    std::mutex path_mutex_;
};
}
