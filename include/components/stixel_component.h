#pragma once
#include <utility>
#include <opencv2/opencv.hpp>
#include "droneworld_type.h"
#include "components/base_component.h"
#include "droneworld_utility.h"
#include "droneworld_algorithm.h"

namespace droneworld{

class StixelComponent :public BaseComponent {
public:
	// TODO: bad idea to use naked pointer, shared_ptr would be better
	StixelComponent(std::queue<ScaledDisparityFrame>* scaled_disparity_frame_queue,
		std::queue<cv::Mat>* scnen_mat_queue, std::vector<Point3D> *point_cloud,
		std::shared_ptr<Config> conf, std::string drone_name);
	~StixelComponent();
    void Begin();
	void Update(double delta_time);
	void QueuePop();

	double CompactErrorCaculate(std::vector<Plane> &plane_dst, int error_type);

	/* about ocv drawing */
	cv::Scalar ComputeColor(float val);
	cv::Scalar DispToColor(float disp, float maxdisp);
	void DrawStixelImage(cv::Mat& img, const DrawStixel& draw_stixel, cv::Scalar color);
	void DrawStixelsImage(cv::Mat& scene_image,
		std::vector<DrawStixel>& draw_stixel, int update_time=100);
	void DrawStixelShow(int update_time=100);

	/* data access */
    DispFrame GetDisparityFrame();
    RpcSuperPillarFrame GetSuperPillarFrame();

	/* data */
	std::queue<cv::Mat>* scene_mat_queue_;
	std::queue<ScaledDisparityFrame>* scaled_disparity_frame_queue_;
	std::queue<SuperPillarFrame> super_pillar_frame_queue_;
	std::queue<std::vector<StixelMask>> mask_frame_queue_;
	std::vector<Point3D>* point_cloud_;
	std::queue<std::vector<Plane>> plane_frame_gridmap;
	//std::queue<std::vector<DrawStixel>> draw_stixel_queue_;

	/* parameter knob */
	void stixel_width(int w){
		stixel_width_ = w;
	}
private:
	friend class Drone;
	/* methods */
	void RunStixel();
	void GetSuperPillar();
	void FindWall(std::vector<double> &stixel, std::vector<KdePeak> &peaks, StixelMask &mask);
	void FindGnd(std::vector<double> &stixel,  StixelMask &mask);
	void FindSky(std::vector<double> &stixel,  StixelMask &mask);
	void FindObjByD(std::vector<double> &stixel, StixelMask &mask);
	void FindObjByH(std::vector<double> &stixel, StixelMask &mask);
	void RefineObjByD(StixelMask &mask);
	/* data */
	std::string drone_name_;
	std::shared_ptr<Config> config_;
	int width_;
	int height_;
    double fov_ = 1.5707963267948966;
	int kde_width_ = 1000;
	int stixel_width_;
	double baseline_;
    double focus_; // seems bo be a fixed value, unrelated to fov (in an airsim program)
	double baseline_x_focus_; // baseline * focus
	double center_u_;
	double center_v_;
	int idx_frame_;
	// normalized disparity
	double disp_max_ = 0.25;               // 0.5m
	double disp_min_ = 0.00625;            // 20m
	double distance_min_ = DISTANCE_MIN;   // 0.5m
	double distance_max_ = DISTANCE_MAX;   // 20m
	const double PI = 3.141592653589793;
	double noise_amp_ = 0.0;
};
}
