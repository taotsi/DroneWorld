#pragma once
#include "components/base_component.h"
#include "droneworld_type.h"

namespace droneworld{

class RefinePlaneComponent : public BaseComponent{
public:
    RefinePlaneComponent(std::queue<std::vector<Plane>> *planes_queue);
    ~RefinePlaneComponent();
    void Begin();
    void Update(double delta_time);
    void QueuePop();
    std::queue<std::vector<Plane>> *planes_queue_;
    std::vector<Plane> planes_refined_;
    void RunRefine();
    // for rpc server
    std::vector<std::vector<std::vector<double>>> GetRefined();
private:

};

} // namespace droneworld
