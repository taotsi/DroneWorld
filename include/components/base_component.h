#pragma once
#include <thread>

#include "common/common_utils/StrictMode.hpp"
STRICT_MODE_OFF
#ifndef RPCLIB_MSGPACK
#define RPCLIB_MSGPACK clmdep_msgpack
#endif // !RPCLIB_MSGPACK
#include "rpc/rpc_error.h"
STRICT_MODE_ON

#include "vehicles/multirotor/api/MultirotorRpcLibClient.hpp"
#include "common/common_utils/FileSystem.hpp"

#include "droneworld_type.h"
#include "droneworld_utility.h"

typedef ImageCaptureBase::ImageRequest ImageRequest;
typedef ImageCaptureBase::ImageResponse ImageResponse;
typedef ImageCaptureBase::ImageType ImageType;

namespace droneworld{

class BaseComponent {
public:
    BaseComponent() = default;
    BaseComponent(const BaseComponent&) = default;
    BaseComponent(BaseComponent&&) = default;
    BaseComponent& operator=(const BaseComponent&) = default;
    BaseComponent& operator=(BaseComponent&&) = default;
    ~BaseComponent() = default;
    virtual void Update(double delta_time) {}
    virtual void Begin() {}
    void Start() { is_on_ = true; }
    void Stop() { is_on_ = false; }
    bool IsOn() { return static_cast<bool>(is_on_); };
    bool IsBusy() { return static_cast<bool>(is_busy_); }
protected:
    std::atomic<bool> is_on_ = true;
    std::atomic<bool> is_busy_ = false;
private:
    std::mutex mtx_msg_;
    std::queue<std::string> messages_in_;
    std::queue<std::string> messages_out_;
    friend class Transceiver;

    bool Read(std::string &msg){
        std::lock_guard<std::mutex> lg{mtx_msg_};
        if(messages_out_.empty()){
            // std::cout << "no message from component\n";
            return false;
        }
        msg = messages_out_.front();
        messages_out_.pop();
        return true;
    }
    void Write(std::string &msg){
        std::lock_guard<std::mutex> lg{mtx_msg_};
        messages_in_.push(msg);
        // consume the message in
        dwi0("component get msg: ", msg);
        messages_in_.pop();
    }
};
}