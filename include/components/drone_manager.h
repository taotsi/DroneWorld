#ifndef DRONE_MANAGER_H_
#define DRONE_MANAGER_H_
#include <string>
#include <map>
#include <array>
#include <memory>
#include "components/base_component.h"
#include "drone.h"

namespace droneworld {

class DroneManager : public BaseComponent {
public:
    DroneManager(){};
    DroneManager(std::shared_ptr<Config> config);
    DroneManager(const DroneManager&) = default;
    DroneManager(DroneManager&&) = default;
    DroneManager& operator=(const DroneManager&) = default;
    DroneManager& operator=(DroneManager&&) = default;
    ~DroneManager(){};

    void Begin();
    void Update(double delta_time);
    void GoOn();

    std::shared_ptr<Drone> GetDrone();
    std::shared_ptr<Drone> GetDrone(std::string drone);

    void Select(std::string drone);
    void List();
    DronePose Pose();
    DronePose Pose(std::string drone);
    void GoTo(std::array<float, 3> pos);
    void GoTo(std::string drone, std::array<float, 3> pos);
private:
    std::string selected_drone_;
    std::map<std::string, std::shared_ptr<Drone>> drone_list_;
    std::shared_ptr<Config> config_;

    void SpawnDrones();
};

}

#endif // DRONE_MANAGER_H_