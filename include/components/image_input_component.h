#pragma once
#include "components/base_component.h"
#include <opencv2/opencv.hpp>
#include <queue>
#include <algorithm>
#include "droneworld_algorithm.h"

namespace droneworld{

class ImageInputComponent : public BaseComponent {
public:
    ImageInputComponent(std::shared_ptr<Config> conf);
    ~ImageInputComponent();
    void Update(double delta_time);
    void Begin();
    int idx_frame();
    std::queue<ScaledDisparityFrame> scaled_disparity_frame_queue_;
    std::queue<cv::Mat> scene_mat_queue_;

    /* parameter knob */
    void stixel_width(int w){
        stixel_width_ = w;
    }
private:
    friend class Drone;
    /* methods */
	void RetreiveFrame();
    void RecordFrame();
    void RetreivePixelColumn(const ImageResponse &frame_raw);
    void LoadDataOffline();

    /* data */
    msr::airlib::MultirotorRpcLibClient client_;
    int width_;
    int height_;
    double focus_;
    double baseline_;
    int stixel_width_;
	double base_line_;

    std::shared_ptr<Config> config_;

    ConfigMode mode_;
    PlayMode play_mode_;

    GPSPosition gps_base_position_;

    int record_fps_;
    fs::path record_path_image_;
    fs::path record_path_depth_;
    fs::path record_path_state_;

    int idx_frame_;
	int start_idx_;
    int stop_idx_;
    OfflineMode offline_mode_;
    fs::path offline_path_image_;
    fs::path offline_path_depth_;
    fs::path offline_path_state_;
};
} // namespace droneworld