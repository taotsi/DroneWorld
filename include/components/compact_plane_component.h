#pragma once
#include "components/base_component.h"

namespace droneworld{

class CompactPlaneComponent : public BaseComponent {
public:
    CompactPlaneComponent(
        std::queue<std::vector<std::vector<Pillar>>>* pillar_cluster_queue, std::vector<Plane>* planes);
    ~CompactPlaneComponent();
    void Begin();
    void Update(double delta_time);
    void QueuePop();
    std::vector<Plane> GetPlanes(); // TODO: maybe return const
    // for rpc server
    std::vector<std::vector<std::vector<double>>> GetRpcPlanes();
    /* data */
    std::queue<std::vector<std::vector<Pillar>>>* pillar_cluster_queue_;

private:
    /* model data */
    std::vector<Plane>* planes_;
    std::mutex mtx_;
    /* methods */
    void RunCompactPlane();
    void CompactPlane();
    void ClusterTo3DModel(std::vector<Pillar> &cluster,
        std::vector<Plane> &planes);
    void SplitMerge(std::vector<Pillar> &cluster, int start, int end,
        BlockedIndex &block);
    int CheckoutMidpoint(std::vector<Pillar> &cluster,
        int start, int end, double dist_thh);
    Plane ClusterToPlane(std::vector<Pillar> &cluster, int start, int end);
    void FillConcave(std::vector<Pillar> &cluster,int start, int end,
        std::vector<Plane> &planes, double width_thh=1.0);
// void PillarToPlaneIfPossible(std::vector<Pillar> &cluster,
//     std::vector<Plane> &planes);
// bool GetSignedDistIfNecessary(Line2dFitted &line,
//     std::vector<Pillar> &pillars, int start, int end,
//     std::vector<double> &clipped_dist, double dist_epsilon=0.3);

};

} // namespace droneworld
