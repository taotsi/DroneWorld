#ifndef HOSTGUI_H_
#define HOSTGUI_H_

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>
#include <GL/gl3w.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <thread>
#include <iostream>
#include <vector>
#include <atomic>
#include <mutex>
#include <queue>
#include <string>
#include "shader.h"
#include "components/base_component.h"
#include "components/drone_manager.h"

// TODO: switch Auto and SingleFrame

namespace droneworld{

class World;
class Drone;

//TODO: 3d plane render
//TODO: mouse click response
//TODO: image play, RGB, disparity/pointcloud
//TODO: 2d plot, vectors
//TODO: seperate ogl window

/*
    coordinate system from this to OpenGL:
    this:
    z
    |
    |   y
    | /
    0------x
    OpenGL:
        y
        |
        |
        |
        0------x
      /
    z
    so it's like from (x, y, z) to (x, z, -y)
*/

/*
    a rectangle is composed of two triangles
    p3-----*p2*
    | \     |
    |   \   |
    |     \ |
   *p1*-----p4
*/

/*
    OpenGL window coordinate:
    0,H ------------ W,H
     |                |
     |                |
     |                |
    0,0 ------------ W,0
*/

class Gui : public BaseComponent{
public:
    Gui(std::shared_ptr<Config> config);
    Gui(const Gui&) = default;
    Gui(Gui&&) = default;
    Gui& operator=(const Gui&) = default;
    Gui& operator=(Gui&&) = default;
    ~Gui();

    void Begin();
    void Update(double delta_time);

    void AddPlane(Plane p);
    void AddPlane(float x1, float y1, float z1, float x2, float y2, float z2);
    void FlushPlanes();
    void AddPointToGo(float x, float y, float z);
    void FlushPointToGo();
    void AddPointBeen(float x, float y, float z);
    void FlushPointBeen();
    void AddPlotVal(int no, float value);

    void TurnOff();
    void TurnOn();

    std::shared_ptr<DroneManager> drone_manager_;
private:
    std::shared_ptr<Config> config_;
    std::thread thread_;
    void ThreadMain();
    std::mutex mtx_;

    void MainPanel();

    static unsigned int win_width_;
    static unsigned int win_height_;

    bool render_planes_ = true;
    bool render_path_goto_ = false;
    bool render_path_been_ = false;
    bool render_image_window_ = false;
    bool render_point_cloud_ = false;

    float map_range_ = 10.f;

    /* compact model */
    // data format: [x11, y11, z11, x12, y12, z12, x21, y21, z21, x22, y22, z22,...]
    std::vector<float> planes_;
    std::vector<Plane>& GetCompactModelData();
    void UpdatePlanes();
    void RenderCompactModel(Shader &shader, glm::mat4 &model, glm::mat4 &view, glm::mat4 &projection);

    /* flight path */
    // data format: [x1, y1, z1, x2, y2, z2, ...]
    std::vector<float> path_togo_ = {0.f, 0.f, 0.f};
    std::vector<float> path_been_ = {0.f, 0.f, 0.f};
    std::atomic<bool> is_flight_path_updated_ = false;
    static constexpr size_t FLIGHT_PATH_CAPACITY = 5000;
    void UpdatePath(float x, float y, float z, float epsilon);
    void RenderPathToGo(Shader &shader, glm::mat4 &model, glm::mat4 &view, glm::mat4 &projection);
    void RenderPathBeen(Shader &shader, glm::mat4 &model, glm::mat4 &view, glm::mat4 &projection);

    /* plots */
    std::vector<std::vector<float>> plots_;

    /* node */
    std::vector<Point3D> node_position;
	const int OFF_SET_X = 0;
	const int OFF_SET_Y = 0;
    int nodes_num = 0;

    /* point cloud */
    // data format: [x1, y1, z1, x2, y2, z2, ...]
    std::vector<float> point_cloud_ = {0.f, 0.f, 0.f};
    void UpdatePointCloud();
    std::vector<Point3D>& GetPointCloudData();
    void RenderPointCloud(Shader &shader, glm::mat4 &model, glm::mat4 &view, glm::mat4 &projection);

    /* image window */
    void RenderImageWindow();

    /* TODO: code to be merged */
    std::thread gui_traject_;
    const float WINDOW_SIZE = 20;
	void RenderNodePosition(Shader &shader, glm::mat4 &model, glm::mat4 &view, glm::mat4 &projection);
	void RenderTrajectory(Shader &shader, glm::mat4 &model, glm::mat4 &view, glm::mat4 &projection);
    void GuiTrajectoryMain();
	void TransformPosition(Point3D node_position);
	void GuiNodeInput();
	void SensorSample();

    /* opengl callbacks */
    static void framebuffer_size_callback(GLFWwindow* window, int width, int height);
    static void glfw_error_callback(int error, const char* description);

    /* TODO: opengl input process. use imgui to process input instead of opengl */
    void processInput(GLFWwindow *window);
};

}

#endif // HOSTGUI_H_