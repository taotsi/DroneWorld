#pragma once
#include <vector>
#include <cmath>
#include <utility>
#include <algorithm>
#include <iomanip>
#include <iostream>
#include "types/kinematics.h"
#include "types/plane.h"
#include "types/pillar.h"
#include "types/super_pillar.h"
#include "types/line2dfitted.h"
#include "types/stixel_mask.h"
#include "types/dynamic_window.h"
#include "types/kde.h"
#include "types/type_alias.h"
#include "types/blocked_index.h" // LEGACY: 

/*
 * Including Relations
 *
 * kinematics.h     : super_pillar.h
 * pillar.h         : kinematics.h
 * super_pillar.h   : type_alias.h, pillar.h
 * line2dfitted.h   : kinematics.h, pillar.h
 * plane.h          : kinematics.h, pillar.h, line2dfitted.h
 * stixel_mask.h    : kinematics.h, super_pillar.h
 * dynamic_window.h : super_pillar.h, type_alias.h
 * kde.h            :
 * type_alisas.h    :
 * blocked_index.h  :
 */

namespace droneworld {

struct ScaledDisparityFrame {
    ScaledDisparityFrame(){};
    ScaledDisparityFrame(Point3D &position, Quaternion &quaternion)
        : pos_camera_{position}, angle_camera_{quaternion}{

    }
    ScaledDisparityFrame(Point3D &position, Quaternion &quaternion,
        std::vector<std::vector<double>> &data)
        : pos_camera_{position}, angle_camera_{quaternion},
          data_{std::move(data)}{

    }
    ScaledDisparityFrame(Point3D &position, EulerAngle &angle)
        : pos_camera_{position}, angle_camera_{angle} {};
    /* data */
    std::vector<std::vector<double>> data_;
    Point3D pos_camera_;
    EulerAngle angle_camera_;
    /* methods */
    std::vector<double>& operator[](int pos){
        return data_[pos];
    }
    void PushStixel(std::vector<double> &stixel){
        data_.push_back(stixel);
    }
    void PushStixel(std::vector<double> &&stixel){
        data_.push_back(std::move(stixel));
    }
    int size(){
        return static_cast<int>(data_.size());
    }
};

enum FilterFlag {
    kNone = (uint8_t)0,
    kWall = (uint8_t)1,
    kGnd = (uint8_t)2,
    kSky = (uint8_t)4,
    kObj = (uint8_t)8
};

enum RefineFlag{
    kUp = (uint8_t)0,
    kDowm = (uint8_t)1
};
struct FilterStatus {
    FilterStatus(FilterFlag &flag){
        flag_ = flag;
    }
    FilterStatus(FilterFlag &flag, int val){
        value_ = val;
        flag_ = flag;
    }
    int value_ = 0;
    FilterFlag flag_ = kNone;
};

struct DrawStixel{
    int u;                        //!< stixel center x position
    int vT;                       //!< stixel top y position
    int vB;                       //!< stixel bottom y position
    int width=7;                    //!< stixel width
    float disp;                   //!< stixel average disparity
};

} // namespace droneworld
