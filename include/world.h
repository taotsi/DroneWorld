#pragma once

#include <GL/gl3w.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <sstream>
#include <chrono>
#include <thread>
#include <random>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <memory>
#include <utility>
#include <algorithm>
#include "components/drone_manager.h"
#include "droneworld_utility.h"
#include "shader.h"
#include "transceiver.h"
#include "components/gui.h"

namespace droneworld{

using namespace std::chrono_literals;

class World {
public:
    World();
    World(const World&) = delete;
    World& operator=(const World&)  = delete;
    World(World&&) = delete;
    World& operator=(World&&) = delete;
    ~World();
    static void Configure(std::string config_path);
    void Loop();
    friend class Drone;
    friend class RpcServer;
private:
    /* data */
    std::atomic<bool> is_on_ = false;
    static std::string config_path_;
    // TODO: forward shared_ptr of config_ to sub-class, instead of its copies
    std::shared_ptr<Config> config_;

    /* components */
    Transceiver tc_;
    std::shared_ptr<Gui> gui_;

    /* framework methods */
    void Begin();
    void LoopOnline();
    void LoopOffline();
    void Update(double delta_time);

    template<typename F, typename Arithmetic>
    void Test(F knob, Arithmetic beg, Arithmetic end, Arithmetic step){
        for(auto val=beg; val<=end; val+=step){
            knob(val);
            auto start_idx = config_->start_idx();
            auto stop_idx = config_->stop_idx();
            for(auto i=start_idx; i<=stop_idx; i++){
                Recorder::NewRecord();
                Update(0);
            }
            std::cout << Recorder::PutAvr() << "\n";
        }
    }
    template<typename F, typename Arithmetic, typename ...Args>
    void Test(F knob, Arithmetic beg, Arithmetic end, Arithmetic step, Args&&... args){
        for(auto val=beg; val<=end; val+=step){
            knob(val);
            Test(std::forward<decltype(args)>(args)...);
        }
    }
};
}
