#pragma once
#include <vector>
#include <utility>
#include <memory>
#include "rpc/server.h"
#include "world.h"
#include "Drone.h"

namespace droneworld{

std::vector<std::pair<double, double>> test(){
    return {{1.3, 2.5}, {3.7, 4.8}};
}

class RpcServer{
public:
    RpcServer(std::shared_ptr<World> world)
        : world_{world}{
        PackServer(world_);
        server_.async_run();
    }
    ~RpcServer() {
        server_.stop();
    };
    void PackServer(std::shared_ptr<World> world) {
        server_.bind("GetSuperPillarFrame", [&world](){
            return world->gui_->drone_manager_->GetDrone()->stixel_->GetSuperPillarFrame();
        });
        server_.bind("GetSuperPillarCluster", [&world](){
            return world->gui_->drone_manager_->GetDrone()->cluster_->GetSuperPillarCluster();
        });
        server_.bind("GetPillarCluster", [&world](){
            return world->gui_->drone_manager_->GetDrone()->cluster_->GetPillarCluster();
        });
        server_.bind("GetPlanes", [&world](){
            return world->gui_->drone_manager_->GetDrone()->compact_->GetRpcPlanes();
        });
        server_.bind("GetRefined", [&world](){
            return world->gui_->drone_manager_->GetDrone()->refine_->GetRefined();
        });
        server_.bind("test", [](){return test();});
    }
private:
    rpc::server server_{8080};
    std::shared_ptr<World> world_;
};

}