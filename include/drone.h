#pragma once
#include <memory>

#include "common/common_utils/StrictMode.hpp"
STRICT_MODE_OFF
#ifndef RPCLIB_MSGPACK
#define RPCLIB_MSGPACK clmdep_msgpack
#endif // !RPCLIB_MSGPACK
#include "rpc/rpc_error.h"
STRICT_MODE_ON

#include "vehicles/multirotor/api/MultirotorRpcLibClient.hpp"
#include "common/common_utils/FileSystem.hpp"

#include "components/movement_component.h"
#include "components/image_input_component.h"
#include "components/stixel_component.h"
#include "components/pillar_cluster_component.h"
#include "components/compact_plane_component.h"
#include "components/refine_plane_component.h"
#include "droneworld_utility.h"

namespace droneworld{

class World;
class Drone {
public:
    Drone(std::string name, std::shared_ptr<Config> conf);
    ~Drone();
    void Begin();
    void Update(double delta_time);

    /* run loop control */
    void GoOn();
    void QueuePop();
    bool is_dataset_all_run();
    // void SaveData();

    /* data access */
    // NOTE: should've returned const
    std::vector<Plane>& GetCompactModelData();
    std::vector<Point3D>& GetPointCloudData();
    bool is_point_cloud_updated();

    void TestCode();

	/* class info */
	std::string name_;
	/* components */
    std::shared_ptr<MovementComponent> movement_;
	std::shared_ptr<ImageInputComponent> image_input_;
	std::shared_ptr<StixelComponent> stixel_;
    std::shared_ptr<PillarClusterComponent> cluster_;
    std::shared_ptr<CompactPlaneComponent> compact_;
    std::shared_ptr<RefinePlaneComponent> refine_;

    /* parameter knob */
    void stixel_width(int w);
    void pillar_fit_max_distance(int d);
    void disparity_noise(double n);

private:
    std::shared_ptr<Config> config_;
    std::atomic<bool> should_go_on_ = true;
    std::mutex mtx_;

    // TODO: move all data into Drone, instead within components themselves
    std::vector<Plane> compact_model_;
    std::vector<Point3D> point_cloud_ = {{0, 0, 0}};
    std::atomic<bool> is_point_cloud_updated_ = false;
};
}
