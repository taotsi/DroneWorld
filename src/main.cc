#include "world.h"
#include "drone.h"
#include "rpc_server.h"
#include "droneworld_utility.h"
#include "droneworld_type.h"

namespace dw = droneworld;

namespace fs = std::experimental::filesystem;

int main(int argc, char* argv[]) {

    if(argc == 2){
        dw::World::Configure(std::string(argv[1]));
    }else if(argc > 2){
        std::cout << "just give the path of config.json\n";
    }

    static auto world = std::make_shared<dw::World>();
    dw::RpcServer server{world};
    world->Loop();

    return 0;
}

// TODO: data-racing protection for data fifo
