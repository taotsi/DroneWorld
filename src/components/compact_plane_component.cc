#include "components/compact_plane_component.h"
#include "droneworld_algorithm.h"
#include <iostream>

namespace droneworld {

CompactPlaneComponent::CompactPlaneComponent(
	std::queue<std::vector<std::vector<Pillar>>>* pillar_cluster_queue,
	std::vector<Plane>* planes)
	: pillar_cluster_queue_(pillar_cluster_queue), planes_{planes} {

}

CompactPlaneComponent::~CompactPlaneComponent() {

}

void CompactPlaneComponent::Begin() {

}

void CompactPlaneComponent::Update(double DeltaTime) {
	RunCompactPlane();
}
void CompactPlaneComponent::QueuePop(){
	if(!pillar_cluster_queue_->empty()){
		pillar_cluster_queue_->pop();
	}
}
void CompactPlaneComponent::RunCompactPlane() {
	if (!pillar_cluster_queue_->empty()) {
		CompactPlane();
	}
	else {
		dwwarn("pillar_cluster_queue_ is empty");
	}
}

void CompactPlaneComponent::CompactPlane() {
	auto clusters = pillar_cluster_queue_->front();
	auto n_cluster = clusters.size();
	std::vector<std::vector<Plane>> planes_temp;
	planes_temp.reserve(n_cluster);
	planes_temp.resize(n_cluster);
//#pragma omp parallel for
	for (auto i = 0; i < n_cluster; i++) {
		auto n_pillar = clusters[i].size();
		if (n_pillar >= 3){
			ClusterTo3DModel(clusters[i], planes_temp[i]);
		}else if(n_pillar == 2){
			// only two pillars, no need to do linear fitting
			planes_temp[i].push_back(Plane{ clusters[i][0], clusters[i][1] });
		}else if(n_pillar == 1){ // n_pillar = 1
			// TODO: deal with single pillars, can't just ignore it
			//std::cout << "one single pillar\n";
		}
		// if there's no pillar, do nothing
	}
	{
		std::lock_guard<std::mutex> lg{mtx_};
		auto size_planes = planes_temp.size();
		for (int i = 0; i < size_planes; i++) {
			planes_->insert(planes_->end(), planes_temp[i].begin(), planes_temp[i].end());
		}
	}
}

void CompactPlaneComponent::ClusterTo3DModel(std::vector<Pillar> &cluster,
	std::vector<Plane> &planes) {
	int n_pillar = static_cast<int>(cluster.size());
	BlockedIndex block{ n_pillar };
	int end = static_cast<int>(cluster.size()) - 1;
	SplitMerge(cluster, 0, end, block);
	int n_block = block.size();
	for (int i = 1; i < n_block - 1; i++) {
		planes.push_back(ClusterToPlane(cluster, block[i].first, block[i].second));
	}
	for (int i = 0; i < n_block - 1; i++) {
		int temp_start = block[i].second;
		int temp_end = block[i + 1].first;
		if (temp_end - temp_start > 0) {
			FillConcave(cluster, temp_start, temp_end, planes, 1);
		}
	}
}

void CompactPlaneComponent::SplitMerge(std::vector<Pillar> &cluster,
	int start, int end, BlockedIndex &block) {
	if (end - start <= 3) {
		return;
	}
	// the error fix in line is 0.2m
	int mid = CheckoutMidpoint(cluster, start, end, ERROR_FITTED_PLANE);
	if (mid == -1) {
		block.AddSegment(start, end);
		return;
	}
	//std::cout << "mid = " << mid << std::endl;
	SplitMerge(cluster, start, mid == end ? mid - 1 : mid, block);
	SplitMerge(cluster, mid == start ? mid + 1 : mid, end, block);
}

// // there are always 3 pillars at least in parameter cluster due to CompactPlane().
int CompactPlaneComponent::CheckoutMidpoint(std::vector<Pillar> &cluster,
	int start, int end, double dist_thh) {
	int idx_mid = -1;
	double dist_max = 0, dist_temp = 0, dist_left = 0 , dist_right = 0;
	Line2dFitted line{ cluster[start], cluster[end] };
	dist_left = line.Dist(cluster[start].x(), cluster[start].y());
	dist_temp = line.Dist(cluster[start + 1].x(), cluster[start + 1].y());
	//std::cout << " line.x_avr = " << line.x_avr();
	//std::cout << " line.y_avr = " << line.y_avr();
	//std::cout << " line.a = " << line.a();
	//std::cout << " line.b = " << line.b();
	//std::cout << " line.c = " << line.c() << std::endl;
	//std::cout << " line.slope = " << line.slope() << std::endl;
	for (int i = start + 1; i <= end - 1; i++) {
		dist_right = line.Dist(cluster[i + 1].x(), cluster[i + 1].y());
		if (dist_temp > dist_thh) {
			if (dist_temp > dist_max) {
				if ((dist_temp > dist_left) && (dist_temp > dist_right)) {
					dist_max = dist_temp;
					idx_mid = i;
				}
			}
		}
		//std::cout << " dist = " << dist_temp ;
		dist_left = dist_temp;
		dist_temp = dist_right;
	}
	//std::cout << "dist_max = " << dist_max << std::endl;

	return idx_mid;
}

Plane CompactPlaneComponent::ClusterToPlane(std::vector<Pillar> &cluster, int start, int end) {
	// TODO: add the camera pose to keep the direction p1 => p2 is free space at left from the top view
	Line2dFitted line{ cluster, start, end };
	double z_max = -10000;
	double z_min = 10000;
	int clusteri_num = cluster.size();
	for (int i = 0; i < clusteri_num; i++) {
		if (z_max < cluster[i].z2()) {
			z_max = cluster[i].z2();
		}
		if (z_min > cluster[i].z1()) {
			z_min = cluster[i].z1();
		}
	}
	auto p1x = cluster[start].x();
	auto p1y = cluster[start].y();
	auto p1z = z_min;
	auto p2x = cluster[end].x();
	auto p2y = cluster[end].y();
	auto p2z = z_max;
	if (abs(line.slope()) < 1) {
		Point3D p1{ p1x, line.EstimateY(p1x, p1y), p1z };
		Point3D p2{ p2x, line.EstimateY(p2x, p2y), p2z };
		return Plane{ p1, p2 };
	}else {
		Point3D p1{ line.EstimateX(p1x, p1y), p1y, p1z };
		Point3D p2{ line.EstimateX(p2x, p2y), p2y, p2z };
		return Plane{ p1, p2 };
	}
}

// fills the concave which are too narrow and turn the rest of pillars into planes
void CompactPlaneComponent::FillConcave(std::vector<Pillar> &cluster,
	int start, int end, std::vector<Plane> &planes, double width_thh) {
	bool is_done = false;
	int count_fill = 0;
	if (end - start <= 0) {
		return;
	}
	else if (end - start == 1) {
		planes.push_back(ClusterToPlane(cluster, start, end));
	}
	std::vector<Pillar> temp_cluster{ cluster.begin() + start, cluster.begin() + end + 1 };
	while (!is_done) {
		size_t idx = 1;
		count_fill = 0;
		// the size of temp_cluster changes dynamicly
		while (idx < temp_cluster.size() - 1) {
			double x1 = temp_cluster[idx - 1].x() - temp_cluster[idx].x();
			double y1 = temp_cluster[idx - 1].y() - temp_cluster[idx].y();
			double x2 = temp_cluster[idx + 1].x() - temp_cluster[idx].x();
			double y2 = temp_cluster[idx + 1].y() - temp_cluster[idx].y();
			double cross_product = x1 * y2 - x2 * y1;
			if (cross_product >= 0) {
				auto width = temp_cluster[idx + 1].DistHorizon(temp_cluster[idx - 1]);
				if (width <= width_thh) {
					temp_cluster.erase(temp_cluster.begin() + idx);
					count_fill++;
				}
				else {
					idx++;
				}
			}
			else {
				idx++;
			}
		}
		if (count_fill == 0) {
			is_done = true;
		}
	}
	for (auto i = 0; i < temp_cluster.size() - 1; i++) {
		planes.push_back(ClusterToPlane(temp_cluster, i, i + 1));
	}
}

std::vector<Plane> CompactPlaneComponent::GetPlanes(){
	std::lock_guard<std::mutex> lg{mtx_};
	return *planes_;
}

// for rpc server
std::vector<std::vector<std::vector<double>>>
	CompactPlaneComponent::GetRpcPlanes() {
	if (!planes_->empty()) {
		std::vector<std::vector<std::vector<double>>> temp_planes;
		for (auto &plane : *planes_) {
			temp_planes.push_back(plane.GetCoor());
		}
		return temp_planes;
	}
	else {
		std::cout << "planes_ is empty\n";
		return std::vector<std::vector<std::vector<double>>>{};
	}
}

} // namespace droneworld
