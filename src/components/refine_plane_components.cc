#include "components/refine_plane_component.h"

namespace droneworld{

RefinePlaneComponent::RefinePlaneComponent(
std::queue<std::vector<Plane>> *planes_queue)
: planes_queue_{planes_queue}{

}
RefinePlaneComponent::~RefinePlaneComponent(){

}

void RefinePlaneComponent::Begin(){

}
void RefinePlaneComponent::Update(double delta_time){
	RunRefine();
}
void RefinePlaneComponent::QueuePop(){
	if(!planes_queue_->empty()){
		planes_queue_->pop();
	}
	planes_refined_.clear();
}
void RefinePlaneComponent::RunRefine(){
	if(!planes_queue_->empty()){
		auto planes_to_refine = planes_queue_->front();
		RefinePlanes(planes_refined_, planes_to_refine);
	}else{

	}
}

// for rpc server
std::vector<std::vector<std::vector<double>>>
RefinePlaneComponent::GetRefined(){
	std::vector<std::vector<std::vector<double>>> temp_planes;
	for(auto &it : planes_refined_){
		temp_planes.push_back(it.GetCoor());
	}
	return temp_planes;
}

} // namespace droneworld