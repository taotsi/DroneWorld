#include "components/pillar_cluster_component.h"
#include <omp.h>

namespace droneworld{

extern FilterStatus Filter(
    std::vector<double> &vec, int start, int step,
    double mean, double min, double max, double disp_max_, double disp_min_);

PillarClusterComponent::PillarClusterComponent(
    std::queue<SuperPillarFrame>* super_pillar_frame_queue)
    : super_pillar_frame_queue_{super_pillar_frame_queue} {

}

PillarClusterComponent::~PillarClusterComponent(){

}

void PillarClusterComponent::Begin(){

}

void PillarClusterComponent::Update(double delta_time){
    RunCluster();
}

void PillarClusterComponent::RunCluster(){
    if(!super_pillar_frame_queue_->empty()){
        // Timer t;
        // t.tStart();
        SuperPillarCluster super_pillar_cluster;
        PillarCluster pillar_cluster;
        PillarCluster temp;
        auto &super_pillar_frame = super_pillar_frame_queue_->front();
        CluterSuperPillars(super_pillar_frame, super_pillar_cluster);
        super_pillar_cluster_queue_.push(super_pillar_cluster);
        // dwmsg("super pillar cluster size: ", super_pillar_cluster.size());

        SuperDynamicWindow super_window{0.5, 1.0};
        // super_window.SetSafeParam(0.05, 0.1);
        for(auto &plcl : super_pillar_cluster){
            super_window.clear();
            temp.clear();
            super_window.Cluster(plcl, temp);
            // dwmsg("super window size ", super_window.size());
            // std::cout << super_window;
            pillar_cluster.insert(pillar_cluster.end(), temp.begin(), temp.end());
        }

        pillar_cluster_queue_.push(std::move(pillar_cluster));
        // Recorder::Add(t.tPutNow());
    }else{
        dwwarn("super_pillar_frame_queue_ is empty");
    }
}
void PillarClusterComponent::QueuePop(){
    if(!super_pillar_frame_queue_->empty()){
        super_pillar_frame_queue_->pop();
    }
    if(!super_pillar_cluster_queue_.empty()){
        super_pillar_cluster_queue_.pop();
    }
}

/* for rpclib server */
RpcSuperPillarCluster PillarClusterComponent::GetSuperPillarCluster(int idx){
    if(!super_pillar_cluster_queue_.empty()){
        auto& cluster = super_pillar_cluster_queue_.front();
        auto n_cluster = cluster.size();
        RpcSuperPillarCluster result;
        RpcSuperPillars temp_pillars;
        RpcSuperPillar temp_pillar;
        result.reserve(n_cluster);
        for(auto i=0; i<n_cluster; i++){
            temp_pillars.clear();
            auto n_sp = cluster[i].size();
            for(auto j=0; j<n_sp; j++){
                const auto& temp_z = cluster[i][j].z();
                temp_pillar.clear();
                temp_pillar.push_back({cluster[i][j].x(), cluster[i][j].y()});
                temp_pillar.insert(temp_pillar.end(), temp_z.begin(), temp_z.end());
                temp_pillars.push_back(temp_pillar);
            }
            result.push_back(temp_pillars);
        }
        return result;
    }else{
        std::cout << "super_pillar_cluster_queue_ is empty\n";
        return {};
    }
}
RpcPillarCluster PillarClusterComponent::GetPillarCluster(){
    if(!pillar_cluster_queue_.empty()){
        auto &cluster = pillar_cluster_queue_.front();
        auto n_cluster = cluster.size();
        RpcPillarCluster result;
        result.reserve(n_cluster);
        RpcPillars temp_pillars;
        for(auto i=0; i<n_cluster; i++){
            auto n_pl = cluster[i].size();
            temp_pillars.clear();
            for(auto j=0; j<n_pl; j++){
                temp_pillars.push_back(cluster[i][j].GetCoor());
            }
            result.push_back(temp_pillars);
        }
        return result;
    }else{
        std::cout << "pillar_cluster_queue_ is empty()\n";
        return {};
    }
}

} // namespace droneworld
