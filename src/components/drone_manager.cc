#include "components/drone_manager.h"

namespace droneworld {

DroneManager::DroneManager(std::shared_ptr<Config> config)
    : config_{config}{
    SpawnDrones();
};

void DroneManager::Begin(){
    if(!drone_list_.empty()){
        for(const auto &itr : drone_list_){
            itr.second->Begin();
        }
    }else{
        dwwarn("No drones in drone_map_!");
    }
}
void DroneManager::Update(double delta_time){
    if(!drone_list_.empty()){
        for(const auto &itr : drone_list_){
            itr.second->Update(delta_time);
        }
    }else{
        dwwarn("No drones in drone_map_!");
    }
}
void DroneManager::GoOn(){
	if (config_->mode() == ConfigMode::kOnline) {
		GetDrone()->GoOn();
	}else{ // ConfigMode::kOffline
		if (!GetDrone()->is_dataset_all_run()) {
			GetDrone()->GoOn();
		}
		else {
			dwinfo("already reached the last sample!");
		}
	}
}
std::shared_ptr<Drone> DroneManager::GetDrone(){
    return drone_list_[selected_drone_];
}
std::shared_ptr<Drone> DroneManager::GetDrone(std::string drone){
    if(drone_list_.find(drone) != drone_list_.end()){
        return drone_list_[drone];
    }else{
        dwwarn("No such a drone named as ", drone, ", return current selected drone instead");
        return drone_list_[selected_drone_];
    }
}
void DroneManager::SpawnDrones(){
    // TODO: future feature, multi-drone support
    std::string drone1_name{"drone1"};
    selected_drone_ = drone1_name;
    drone_list_[drone1_name] = std::make_shared<Drone>(drone1_name, config_);
}
void DroneManager::Select(std::string drone){
    if(drone_list_.find(drone) != drone_list_.end()){
        selected_drone_ = drone;
        return;
    }
    dwwarn("No such a drone named as ", drone);
}
void DroneManager::List(){
    std::string list; // for thread safe
    for(auto& [drone_name, drone_ptr] : drone_list_){
        list.append(drone_name+"\n");
    }
    list.append("Current selected: "+selected_drone_);
    std::cout << list;
}
DronePose DroneManager::Pose(){
    return GetDrone()->movement_->Pose();
}
DronePose DroneManager::Pose(std::string drone){
    return GetDrone(drone)->movement_->Pose();
}
void DroneManager::GoTo(std::array<float, 3> pos){
    GetDrone()->movement_->AddPathPoint(pos);
}
void DroneManager::GoTo(std::string drone, std::array<float, 3> pos){
    GetDrone(drone)->movement_->AddPathPoint(pos);
}

}