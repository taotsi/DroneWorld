#define STB_IMAGE_IMPLEMENTATION
// don't include stb_image.h anywhere else.
// don't include stb_image.h in a header file.
// anyway, stb_image.h can only be included once, and it must be in a source file.
#include <algorithm>
#include <stb/stb_image.h>
#include "components/gui.h"

namespace droneworld{

Gui::Gui(std::shared_ptr<Config> config)
    :config_{config}{
    drone_manager_ = std::make_shared<DroneManager>(config_);
    thread_ = std::thread{&Gui::ThreadMain, this};
    path_togo_.reserve(FLIGHT_PATH_CAPACITY);
    path_been_.reserve(FLIGHT_PATH_CAPACITY);
}
Gui::~Gui(){
    if(thread_.joinable()){
        thread_.join();
    }
}

void Gui::Begin(){
    drone_manager_->Begin();
}
void Gui::Update(double delta_time){
    drone_manager_->Update(delta_time);
}

void Gui::TurnOff(){
	is_on_ = false;
}

void Gui::TurnOn(){
	is_on_ = true;
}
unsigned int Gui::win_width_ = 1280;
unsigned int Gui::win_height_ = 720;

void Gui::ThreadMain(){
    glfwSetErrorCallback(glfw_error_callback);
	if (!glfwInit()){
		std::cout << "Failed to init glfw\n";
        return;
    }

	const char* glsl_version = "#version 130";
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	GLFWwindow* window = glfwCreateWindow(win_width_, win_height_, "Host", NULL, NULL);
	if (window == NULL) {
		glfwTerminate();
		std::cout << "Failed to create glfw window\n";
		return;
	}
	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, Gui::framebuffer_size_callback);
	glfwSwapInterval(1); // Enable vsync

	bool err = gl3wInit() != 0;
	if (err){
		std::cout << "Failed to initialize OpenGL loader!\n";
		return;
	}

	glEnable(GL_DEPTH_TEST);

	Shader shader_planes{"./src/shader/compact_model.vs", "./src/shader/compact_model.fs"};
    Shader shader_point_cloud{"./src/shader/point_cloud.vs", "./src/shader/point_cloud.fs"};

	// glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	// TODO: class Texture
    unsigned int texture1;
    glGenTextures(1, &texture1);
    glBindTexture(GL_TEXTURE_2D, texture1);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    int width, height, nrChannels;
    stbi_set_flip_vertically_on_load(true); // tell stb_image.h to flip loaded texture's on the y-axis.
    // NOTE: I failed to load .png that I made using photoshop 2019, .jpg works though
    unsigned char *data = stbi_load("./misc/yb256.jpg", &width, &height, &nrChannels, 0);
    if(data){
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);
    }else{
        std::cout << "Failed to load texture" << std::endl;
    }
    stbi_image_free(data);
    shader_planes.use();
    shader_planes.setInt("texture1", 0);

	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;
	//io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;  // Enable Keyboard Controls
	//io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;   // Enable Gamepad Controls

	ImGui_ImplGlfw_InitForOpenGL(window, true);
	ImGui_ImplOpenGL3_Init(glsl_version);

	io.Fonts->AddFontFromFileTTF("./misc/DroidSans.ttf", 20.0f);

	while (!glfwWindowShouldClose(window) && is_on_){
		processInput(window);

		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();

		/* Render ImGui here */
		MainPanel();

		ImGui::Render();
		// glfwMakeContextCurrent(window);
		glClearColor(0.9f, 0.9f, 0.9f, 1.f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture1);

		// TODO: package this section about transformation
        glm::mat4 model         = glm::mat4(1.0f);
        glm::mat4 view;//          = glm::mat4(1.0f);
        glm::mat4 projection    = glm::mat4(1.0f);
        model = glm::rotate(model, /*(float)glfwGetTime()*/30.f, glm::vec3(0.f, 1.0f, 0.0f));
		float cam_x = sin(glfwGetTime()) * map_range_;
		float cam_z = cos(glfwGetTime()) * map_range_;
		view = glm::lookAt(glm::vec3(cam_x, 2.f, cam_z), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        projection = glm::perspective(glm::radians(60.0f), (float)win_width_ / (float)win_height_, 0.1f, 100.0f);

		/* Render OpenGL primitives here */
		if(render_planes_){
			RenderCompactModel(shader_planes, model, view, projection);
		}
		// if(render_point_cloud_){
		// 	RenderPointCloud(shader_point_cloud, model, view, projection);
		// }
		// if(render_path_goto_){
		// 	RenderPathToGo();
		// }
		// if(render_path_been_){
		// 	RenderPathBeen();
		// }
		// if(render_image_window_){
		// 	RenderImageWindow();
		// }

		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

		// glfwMakeContextCurrent(window);
		glfwSwapBuffers(window);
		// Poll and handle events (inputs, window resize, etc.)
		// You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
		// - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
		// - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
		// Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
		glfwPollEvents();
	}

	// Cleanup
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();

	glfwDestroyWindow(window);
	glfwTerminate();

    is_on_ = false;
}

void Gui::MainPanel(){
	if(!ImGui::Begin("Panel")){
        dwerr("failed to create imgui panel");
        ImGui::End();
        return;
    }
    static bool show_test = true;
    ImGui::Checkbox("test", &show_test);
    if(show_test){
        /* add primitives */
        static float x1 = 0, y1 = 0, z1 = 0, x2 = 1, y2 = 1, z2 = 1;
        ImGui::PushItemWidth(100);
        ImGui::InputFloat("x1", &x1, 0.5, 2, "%.1f"); ImGui::SameLine();
        ImGui::InputFloat("y1", &y1, 0.5, 2, "%.1f"); ImGui::SameLine();
        ImGui::InputFloat("z1", &z1, 0.5, 2, "%.1f");
        ImGui::InputFloat("x2", &x2, 0.5, 2, "%.1f"); ImGui::SameLine();
        ImGui::InputFloat("y2", &y2, 0.5, 2, "%.1f"); ImGui::SameLine();
        ImGui::InputFloat("z2", &z2, 0.5, 2, "%.1f");
        ImGui::PopItemWidth();
        if(ImGui::Button("Add Plane")){
            AddPlane(x1, y1, z1, x2, y2, z2);
        }
    }
    /* flight control */
    if(config_->mode() == ConfigMode::kOnline){
        if(ImGui::Button("Take Off")){
            drone_manager_->GetDrone()->movement_->TakeOff();
        }
        ImGui::SameLine();
        if(ImGui::Button("Land")){
            drone_manager_->GetDrone()->movement_->Land();
        }
        static double pi = 3.141502654;
        static DronePose pose;
        pose = drone_manager_->Pose();
        ImGui::Text("yaw = %.1f, pitch = %.1f, roll = %.1f",
            pose.yaw()*180/pi, pose.pitch()*180/pi, pose.roll()*180/pi);
        static float speed = config_->online_speed();
        ImGui::PushItemWidth(150);
        ImGui::InputFloat("", &speed, 0.5, 1, "%.1f");
        ImGui::SameLine();
        ImGui::PopItemWidth();
        if(ImGui::Button("Set Speed")){
            drone_manager_->GetDrone()->movement_->SetSpeed(speed);
        }
        static float x = 0, y = 0, z = 2;
        ImGui::Text("Go to");
        ImGui::PushItemWidth(180);
        ImGui::InputFloat("x", &x, 1, 3, "%.1f");
        ImGui::SameLine();
        ImGui::Text("now at %.1f", pose.x());
        ImGui::InputFloat("y", &y, 1, 3, "%.1f");
        ImGui::SameLine();
        ImGui::Text("now at %.1f", pose.y());
        ImGui::InputFloat("z", &z, 1, 3, "%.1f");
        ImGui::SameLine();
        ImGui::Text("now at %.1f", pose.z());
        ImGui::PopItemWidth();
        if(ImGui::Button("GO!")){
            drone_manager_->GoTo({x, y, z});
        }
    }

    /* play mode control */
    ImGui::Separator();
    static int frame_count = 0;
    // TEMP:
    if(config_->play_mode() == PlayMode::kSingleFrame){
        if(ImGui::Button("Next Frame")){
            drone_manager_->GoOn();
            frame_count++;
        }
        ImGui::SameLine();
        ImGui::Text("frame count %d", frame_count);
    }

    /* parameter control */
    ImGui::Separator();
    ImGui::PushItemWidth(180);
    static float pre_map_range = map_range_;
	static float crt_map_range = map_range_;
    pre_map_range = crt_map_range;
    if((ImGui::InputFloat("Map Range(m)", &crt_map_range, 1.0, 2.0, "%.0f"))){
		std::lock_guard<std::mutex> guard{mtx_};
		if(crt_map_range <= 0.f){
			crt_map_range = pre_map_range;
		}
		map_range_ = crt_map_range;
        auto scale = [coe = pre_map_range/map_range_](auto &ele){return coe*ele;};
		std::transform(planes_.begin(), planes_.end(), planes_.begin(), scale);
        std::transform(path_togo_.begin(), path_togo_.end(), path_togo_.begin(), scale);
		std::transform(path_been_.begin(), path_been_.end(), path_been_.begin(), scale);
        std::transform(point_cloud_.begin(), point_cloud_.end(), point_cloud_.begin(), scale);
    }
    ImGui::PopItemWidth();

    /* render option */
    ImGui::Separator();
    ImGui::Text("Render Option");
    ImGui::Checkbox("Compact Model", &render_planes_);
    ImGui::SameLine();
    ImGui::Text("%d rectangles", planes_.size()/6);
    ImGui::Checkbox("Point Cloud", &render_point_cloud_);
    ImGui::SameLine();
    ImGui::Text("%d points", point_cloud_.size()/3);
    ImGui::Checkbox("Flight Path To Go", &render_path_goto_);
	ImGui::Checkbox("Flight Path Been", &render_path_been_);

    /* other */
    ImGui::Separator();
    static bool show_demo_window = false;
    ImGui::Checkbox("Imgui Demo Window", &show_demo_window);
    if(show_demo_window){
        ImGui::ShowDemoWindow(&show_demo_window);
    }
    // NOTE: this is NOT the fps of the main application
    ImGui::Text("%.1f ms/frame (%.0f FPS)", 1000.0f/ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

    ImGui::End();
}

void Gui::RenderCompactModel(Shader &shader, glm::mat4 &model, glm::mat4 &view, glm::mat4 &projection) {
	shader.use();
	shader.setMat4("model", model);
	shader.setMat4("view", view);
	shader.setMat4("projection", projection);

	UpdatePlanes();
	/* draw model data */
    auto n = planes_.size();
	for(auto i=0; i<n; i+=6){
		auto &x1 = planes_[i];
        auto &y1 = planes_[i+1];
        auto &z1 = planes_[i+2];
        auto &x2 = planes_[i+3];
        auto &y2 = planes_[i+4];
        auto &z2 = planes_[i+5];

		/*
			a rectangle is composed of two triangles
			p3-----*p2*
			| \     |
			|   \   |
			|     \ |
		   *p1*-----p4
		*/
		float texture_width = sqrt(pow(x1 - x2, 2) + pow(y1 - y2, 2)) * map_range_ / 2.f;
		float texture_height = abs(z1 - z2) * map_range_ / 2.f;
		float rectangle[] = {
			x1, z1, -y1, 0.f,           0.f,             // p1
			x1, z2, -y1, 0.f,           texture_height,  // p3
			x2, z1, -y2, texture_width, 0.f,             // p4,
			x2, z2, -y2, texture_width, texture_height,  // p2
			x1, z2, -y1, 0.f,           texture_height,  // p3
			x2, z1, -y2, texture_width, 0.f              // p4
		};

		unsigned int VAO, VBO;
		glGenVertexArrays(1, &VAO);
		glGenBuffers(1, &VBO);
		glBindVertexArray(VAO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(rectangle), rectangle, GL_STATIC_DRAW);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
		glEnableVertexAttribArray(1);
		glBindVertexArray(VAO);
		glDrawArrays(GL_TRIANGLES, 0, sizeof(rectangle)/sizeof(float));
		glDeleteVertexArrays(1, &VAO);
		glDeleteBuffers(1, &VBO);
	}
}
void Gui::UpdatePlanes(){
    auto compact = GetCompactModelData();
    auto n_plane_new = compact.size();
    auto n_plane_old = planes_.size()/6;
    if(n_plane_new > n_plane_old){
        for(auto i=n_plane_old+1; i<=n_plane_new; i++){
            AddPlane(compact[i]);
        }
    }
}
std::vector<Plane>& Gui::GetCompactModelData(){
    return drone_manager_->GetDrone()->GetCompactModelData();
}
void Gui::AddPlane(float x1, float y1, float z1, float x2, float y2, float z2){
	std::lock_guard<std::mutex> guard{mtx_};
	planes_.push_back(x1/map_range_);
	planes_.push_back(y1/map_range_);
	planes_.push_back(z1/map_range_);
	planes_.push_back(x2/map_range_);
	planes_.push_back(y2/map_range_);
	planes_.push_back(z2/map_range_);
}
void Gui::AddPlane(Plane p){
    AddPlane(p.p1().x_, p.p1().y_, p.p1().z_,
        p.p2().x_, p.p2().y_, p.p2().z_);
}
void Gui::FlushPlanes(){
	std::lock_guard<std::mutex> guard{mtx_};
	planes_.clear();
}

void Gui::RenderPointCloud(Shader &shader, glm::mat4 &model, glm::mat4 &view, glm::mat4 &projection){
    // shader.use();
	// shader.setMat4("model", model);
	// shader.setMat4("view", view);
	// shader.setMat4("projection", projection);

    // if(drone_manager_->GetDrone()->is_point_cloud_updated()){
    //     auto &point_cloud = GetPointCloudData();
    //     std::vector<float> point_cloud_gl_temp;
    //     auto n_point = point_cloud.size();
    //     for(auto i=0; i<n_point; i++){
    //         point_cloud_gl_temp.push_back(point_cloud[i].x_/map_range_);    // ogl world X
    //         point_cloud_gl_temp.push_back(point_cloud[i].z_/map_range_);    // ogl world Y
    //         point_cloud_gl_temp.push_back(-point_cloud[i].y_/map_range_);   // ogl world Z
    //     }
    //     point_cloud_ = std::move(point_cloud_gl_temp);
    // }
	// auto size = point_cloud_.size();

	// unsigned int vao, vbo;
	// glGenVertexArrays(1, &vao);
	// glGenBuffers(1, &vbo);
	// glBindVertexArray(vao);
	// glBindBuffer(GL_ARRAY_BUFFER, vbo);
	// glBufferData(GL_ARRAY_BUFFER, size*sizeof(float), point_cloud_gl_.data(), GL_STATIC_DRAW);
	// glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(float), (void*)0);
	// glEnableVertexAttribArray(0);

	// glPointSize(3);

	// glBindVertexArray(vao);
	// glDrawArrays(GL_POINTS, 0, size/3);
	// glDeleteVertexArrays(1, &vao);
	// glDeleteBuffers(1, &vbo);
}
std::vector<Point3D>& Gui::GetPointCloudData(){
    return drone_manager_->GetDrone()->GetPointCloudData();
}
void Gui::UpdatePointCloud(){
    // TODO:
}

void Gui::RenderPathToGo(Shader &shader, glm::mat4 &model, glm::mat4 &view, glm::mat4 &projection){

}
void Gui::RenderPathBeen(Shader &shader, glm::mat4 &model, glm::mat4 &view, glm::mat4 &projection){

}
// TODO:
void Gui::AddPointToGo(float x, float y, float z){

}
void Gui::FlushPointToGo(){

}
void Gui::AddPointBeen(float x, float y, float z){

}
void Gui::FlushPointBeen(){

}

void Gui::RenderImageWindow(){

}


void Gui::AddPlotVal(int no, float value){

}

void Gui::framebuffer_size_callback(GLFWwindow* window, int width, int height){
	// glViewport(0, 0, width, height);
	win_width_ = width;
	win_height_ = height;
}
void Gui::processInput(GLFWwindow *window){
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
}
void Gui::glfw_error_callback(int error, const char* description){
	fprintf(stderr, "Glfw Error %d: %s\n", error, description);
}

}