#include <experimental/filesystem>
#include <iostream>
#include <fstream>
#include <sstream>
#include <thread>
#include <chrono>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "components/image_input_component.h"
#include "image_codec/pfm.hpp"
#include "droneworld_algorithm.h"

namespace droneworld{

using namespace pfm;
namespace fs = std::experimental::filesystem;
using namespace std::chrono_literals;

ImageInputComponent::ImageInputComponent(std::shared_ptr<Config> conf)
	:config_{conf}{
	// TODO: shouldn't have used copies of Config. One Config instance and several shared_ptrs would be better
	switch(config_->mode()){
		case ConfigMode::kOnline:{
			width_ = config_->width();
			height_ = config_->height();
			stixel_width_ = config_->stixel_width();
			break;
		}
		case ConfigMode::kRecord:{
			width_ = config_->width();
			height_ = config_->height();
			record_fps_ = config_->record_fps();
			record_path_image_ = config_->record_path_image();
			record_path_depth_ = config_->record_path_depth();
			record_path_state_ = config_->record_path_state();
			break;
		}
		case ConfigMode::kOffline:{
			width_ = config_->width();
			height_ = config_->height();
			stixel_width_ = config_->stixel_width();
			offline_mode_ = config_->offline_mode();
			focus_ = config_->focus();
			baseline_ = config_->baseline();
			offline_path_image_ = config_->offline_path_image();
			offline_path_depth_ = config_->offline_path_depth();
			offline_path_state_ = config_->offline_path_state();
			idx_frame_ = config_->start_idx();
			start_idx_ = config_->start_idx();
			stop_idx_ = config_->stop_idx();
			break;
		}
		default:{
			dwtrap("No such a mode!");
		}
	}
}
ImageInputComponent::~ImageInputComponent(){

}
void ImageInputComponent::Begin(){
	switch(config_->mode()){
		case ConfigMode::kOnline:{
			dwinfo("image input client, ", config_->mode(), " mode");
			client_.confirmConnection();
			break;
		}
		case ConfigMode::kRecord:{
			dwinfo("image input client, ", config_->mode(), " mode");
			client_.confirmConnection();
			fs::remove_all(record_path_image_);
			fs::remove_all(record_path_depth_);
			fs::remove(record_path_state_);
			fs::create_directories(record_path_image_);
			fs::create_directories(record_path_depth_);
			std::ofstream state_file{record_path_state_, std::ios::app};
			if(state_file.is_open()){
				state_file.close();
			}else{
				dwtrap("couldn't create state file, which you set in config.json is ", record_path_state_);
			}
			break;
		}
		case ConfigMode::kOffline:{
			// nothing to do
			break;
		}
		default:{
			dwtrap();
		}
	}
}
void ImageInputComponent::Update(double delta_time){
	switch(config_->mode()){
		case ConfigMode::kOnline:{
			RetreiveFrame();
			// dwmsg("RetreiveFrame once");
			break;
		}
		case ConfigMode::kRecord:{
			RecordFrame();
			static std::chrono::duration<double, std::milli>
				time_per_frame = 1000ms/record_fps_;
			if(delta_time < time_per_frame.count()){
				std::this_thread::sleep_for(
					std::chrono::duration<double, std::milli>(
					time_per_frame.count() - delta_time));
			}
			break;
		}
		case ConfigMode::kOffline:{
			LoadDataOffline();
			break;
		}
		default:{
			dwtrap();
			break;
		}
	}
}

void ImageInputComponent::RetreiveFrame(){
	static std::vector<ImageRequest> requests{
		ImageRequest("0", ImageType::DisparityNormalized, true),
		ImageRequest("0", ImageType::Scene) };
	const std::vector<ImageResponse> &responses =
		client_.simGetImages(requests);
	RetreivePixelColumn(responses[0]);
	// NOTE: comment below to make it a bit faster, uncomment below if you need them
	// cv::Mat scene_left = cv::Mat(responses[1].height, responses[1].width, CV_8UC3);
	// scene_left = cv::imdecode(responses[1].image_data_uint8, cv::IMREAD_GRAYSCALE);
	// scene_left.convertTo(scene_left, CV_8U);
	// scene_mat_queue_.push(scene_left);
}
void ImageInputComponent::RecordFrame(){
	static std::vector<ImageRequest> requests{
		ImageRequest("0", ImageType::DisparityNormalized, true),
		ImageRequest("0", ImageType::Scene)};
	static int index = 0;
	const std::vector<ImageResponse> &responses =
		client_.simGetImages(requests);
	fs::path path_depth_file{
		record_path_depth_/(std::to_string(index)+".pfm")};
	Utils::writePfmFile(responses[0].image_data_float.data(),
		width_, height_, path_depth_file.string());

	std::ofstream state_file{record_path_state_, std::ios::app};
	Point3D pos = TransformAirsimCoor(
        responses[0].camera_position.x(),
        responses[0].camera_position.y(),
        responses[0].camera_position.z());
	state_file << index << " "
		<< pos.x_ << " " << pos.y_ << " " << pos.z_ << " "
		<< responses[0].camera_orientation.x() << " "
		<< responses[0].camera_orientation.y() << " "
		<< responses[0].camera_orientation.z() << " "
		<< responses[0].camera_orientation.w() << "\n";
	state_file.close();

	fs::path path_image_file{
		record_path_image_/(std::to_string(index)+".png")};
	std::ofstream image_file{path_image_file, std::ios::binary};
	if(image_file.is_open()){
		image_file.write(reinterpret_cast<const char*>(
			responses[1].image_data_uint8.data()),
		responses[1].image_data_uint8.size());
		image_file.close();
	}else{
		dwtrap();
	}

	dwinfo("one frame recorded, index = ", index);
	index++;
}
void ImageInputComponent::LoadDataOffline(){
	std::ifstream state_file{offline_path_state_};
	if(!state_file.is_open()){
		dwtrap("couldn't open state file");
	}
	std::string line;
	Point3D camera_pos;
	Quaternion quat;
	bool is_idx_found = false;
	std::string file_idx_str;
	while(getline(state_file, line)){
		std::stringstream ss{line};
		int file_idx_temp;
		ss >> file_idx_temp;
		if(file_idx_temp == idx_frame_){
			file_idx_str = std::to_string(file_idx_temp);
			ss >> camera_pos.x_ >> camera_pos.y_ >> camera_pos.z_;
			ss >> quat.x_ >> quat.y_ >> quat.z_ >> quat.w_;
			is_idx_found = true;
			break;
		}
	}
	state_file.close();
	if(is_idx_found){
		ScaledDisparityFrame frame_scaled{camera_pos, quat};
		fs::path depth_path{offline_path_depth_/(file_idx_str+".pfm")};
		if(fs::exists(depth_path)){
			Pfm depth_data{offline_path_depth_/(file_idx_str+".pfm")};
			for(auto u=stixel_width_/2; u<width_; u+=stixel_width_){
				std::vector<double> stixel;
				stixel.reserve(height_);
				if(offline_mode_ == OfflineMode::kAirSim){ // it's actually disparity
					for(int v=height_-1; v>=0; v--){
						stixel.push_back(depth_data.Data_[v][u]);
					}
				}else if(offline_mode_ == OfflineMode::kZed){
					for(int v=height_-1; v>=0; v--){
						stixel.push_back(
							focus_*baseline_/(depth_data.Data_[v][u]));
					}
				}else{
					dwtrap("no such a offline mode!");
				}
				frame_scaled.PushStixel(std::move(stixel));
			}
			scaled_disparity_frame_queue_.push(std::move(frame_scaled));
		}else{
			dwwarn(depth_path, " does not exist!");
		}
		std::string image_path;
		if (offline_mode_ == OfflineMode::kAirSim) { // it's actually disparity
			image_path = (offline_path_image_/(file_idx_str+".png")).string();
		}else if(offline_mode_ == OfflineMode::kZed){
			image_path = (offline_path_image_/(file_idx_str+".jpg")).string();
		}
		if(fs::exists(image_path)){
			cv::Mat image_data = cv::imread(image_path, -1);
			cv::normalize(image_data, image_data, 0, 255, cv::NORM_MINMAX);
			image_data.convertTo(image_data, CV_8U);
			cv::cvtColor(image_data, image_data, cv::COLOR_BGR2GRAY);
			scene_mat_queue_.push(std::move(image_data));
		}else{
			dwwarn(image_path, " does not exist!");
		}
	}else{
		dwwarn("couldn't find file ", idx_frame_, ", maybe missing, skipping to next...");
	}
	// dwinfo("One frame loaded offline, index = ", idx_frame_);
	if (idx_frame_ < stop_idx_) {
		idx_frame_++;
	}else{
		idx_frame_ = start_idx_;
	}
}

// NOTE: If the drone is changing its pose too fast,
//		 the raw angle retreived wont be quite accurate.
//		 You can hold up your retreiving untill it hovers or flies straight
// TODO: maybe I should find a way to get the accurate pose angle
/*
	image coor:
	0----- u
	|
	|
	v
 */
void ImageInputComponent::RetreivePixelColumn(const ImageResponse &frame_raw){
    Point3D pos_camera = TransformAirsimCoor(
        frame_raw.camera_position.x(),
        frame_raw.camera_position.y(),
        frame_raw.camera_position.z()
    );
    double quat_w = frame_raw.camera_orientation.w();
    double quat_x = frame_raw.camera_orientation.x();
    double quat_y = frame_raw.camera_orientation.y();
    double quat_z = frame_raw.camera_orientation.z();
    Quaternion angle_camera{ quat_w, quat_x, quat_y, quat_z };
    ScaledDisparityFrame frame_scaled{ pos_camera, angle_camera };
    // the disparity frame received from Airsim is a one-dimenssion array
	// and counts from the bottom row of the image and we flip it up-side-down
    for (auto i=stixel_width_/2; i<width_; i+=stixel_width_) {
        std::vector<double> stixel;
    	stixel.reserve(height_);
        for (int j = height_-1; j >= 0; j--) {
            double pix_val = frame_raw.image_data_float[j*width_ + i];
            stixel.push_back(pix_val);
        }
        frame_scaled.PushStixel(std::move(stixel));
    }
    scaled_disparity_frame_queue_.push(std::move(frame_scaled));
}

int ImageInputComponent::idx_frame(){
	return idx_frame_;
}
} // namespace droneworld
