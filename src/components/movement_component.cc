#include "components/movement_component.h"

namespace droneworld{

MovementComponent::MovementComponent(std::shared_ptr<Config> conf)
    :config_{conf}{
    switch(config_->mode()){
        case ConfigMode::kOnline:{
            speed_ = config_->online_speed();
            break;
        }
        case ConfigMode::kRecord:{
            speed_ = config_->record_speed();
            AddPath(config_->record_path());
            start_path_ = config_->start_path();
            break;
        }
        case ConfigMode::kOffline:{

            break;
        }
        default:{
            dwtrap("no such a mode or this mode is not fit");
            break;
        }
    }
}
MovementComponent::~MovementComponent(){
    Stop();
    if(move_thread_.joinable()){
        move_thread_.join();
    }
    // client_.landAsync()->waitOnLastTask();
    if(config_->mode() == ConfigMode::kOnline){
        client_.armDisarm(false);
        client_.enableApiControl(false);
    }
}
void MovementComponent::Begin(){
    Start();
    dwinfo("movement client ", config_->mode(), " mode");
    client_.confirmConnection();
    client_.enableApiControl(true);
    client_.armDisarm(true);
    client_.takeoffAsync(4)->waitOnLastTask();
    // client_.hoverAsync()->waitOnLastTask();
    std::vector<Vector3r> path;
    for(auto &p : start_path_){
        path.push_back({p[1], p[0], -p[2]});
		//std::cout << "start_path" << p[1] << " " << p[0] << " " << -p[2] << std::endl;
    }
    client_.moveOnPathAsync(path, speed_, 300,
        DrivetrainType::ForwardOnly, YawMode(false, 0), -1.0)
        ->waitOnLastTask();
    move_thread_ = std::thread{&MovementComponent::MoveThreadMain, this};
}
void MovementComponent::Update(double delta_time){
    // if(path_to_go_.empty()){
    //     is_busy_ = false;
    // }else{
    //     is_busy_ = true;
    // }
}
// The movement management is put in a seperated thread, instead of Update(). MoveThreadMain() keeps checking out if there is a path the drone should follow.
void MovementComponent::MoveThreadMain(){
    while(is_on_){
        std::vector<Vector3r> path;
        while(!path_to_go_.empty()){
            std::lock_guard<std::mutex> lg{path_mutex_};
            auto &point = path_to_go_.front();
            path_to_go_.pop();
            path.push_back({point[1], point[0], -point[2]});
        }
        if(path.size() != 0){
            is_busy_ = true;
            client_.moveOnPathAsync(path, speed_, 120,
                DrivetrainType::ForwardOnly, YawMode(false, 0), -1.0)
                ->waitOnLastTask();
            is_busy_ = false;
        }
    }
}
// Adds a target point to the path the drone would follow, it will fly there after all the past target points have been reached.
void MovementComponent::AddPathPoint(const std::array<float, 3> &point){
    std::lock_guard<std::mutex> lg{path_mutex_};
    path_to_go_.push(point);
}
// Adds a series of target points for the drone to follow.
void MovementComponent::AddPath(const std::vector<std::array<float, 3>> &path){
    auto n_point = path.size();
    for(auto i=0; i<n_point; i++){
        AddPathPoint(path[i]);
    }
}
void MovementComponent::ResetPath(
    const std::queue<std::array<float, 3>> &new_path){
    path_to_go_ = new_path;
}

void MovementComponent::SetSpeed(float speed){
    speed_ = speed;
}
void MovementComponent::PrintPose(){
    auto &pose = client_.simGetGroundTruthKinematics().pose;
    auto &pos = pose.position;
    auto &ang = pose.orientation;
    std::cout << "drone position: ("<< pos.x() << ", "
        << pos.y() << ", " << -pos.z() << ")";
    EulerAngle el_ang{Quaternion{ang.w(), ang.x(), ang.y(), ang.z()}};
    std::cout << "; drone angle: yaw = " << el_ang.yaw_ << ", pitch = "
        << el_ang.pitch_ << ", roll = " << el_ang.roll_ << "\n";
}
/*
	coordinate system from AirSim to this
	AirSim:
        y
      /
    0------x
    |
	|
	z
	this:
	z
    |
    |   y
    | /
    0------x
 */
DronePose MovementComponent::Pose(){
    auto &pose = client_.simGetGroundTruthKinematics().pose;
    auto &pos = pose.position;
    auto &ang = pose.orientation;
    EulerAngle el_ang{Quaternion{ang.w(), ang.x(), ang.y(), ang.z()}};
    DronePose p{pos.y(), pos.x(), -pos.z(), el_ang.yaw_, el_ang.pitch_, el_ang.roll_};
    return p;
}
void MovementComponent::TakeOff(){
    client_.takeoffAsync(4)->waitOnLastTask();
}
void MovementComponent::Land(){
    client_.landAsync()->waitOnLastTask();
}

void MovementComponent::MoveTest(){
    float z = -2, vel = 2;
    std::vector<Vector3r> path{Vector3r(0, 0, z), Vector3r(0, -8, z),Vector3r(-18, -8, z)};
    client_.moveOnPathAsync(path, vel, 300, DrivetrainType::ForwardOnly,
        YawMode(false, 0), -1, 0)->waitOnLastTask();
    client_.hoverAsync()->waitOnLastTask();
    std::this_thread::sleep_for(std::chrono::seconds(5));
}
}
