#include "droneworld_algorithm.h"

namespace droneworld {

void RetreiveKde(std::vector<double> const &src, std::vector<double> &dst,
	double max, double min, int kde_width, const std::vector<double> &kernel){
	if (!dst.empty()) {
		dst.clear();
	}
	dst.reserve(kde_width);
	dst.resize(kde_width, 0.0);
	static int kernel_size = static_cast<int>(kernel.size());
	static int kernel_half_size = kernel_size >> 1;
	static double ratio = (max-min) / (kde_width-1);
	static double min_cor = ratio * (kernel_half_size+1) + min;
	static double max_cor = ratio * (kde_width-1-kernel_half_size);
	auto src_size = src.size();
	for (auto i = 0; i < src_size; i++) {
		if (src[i] < max_cor && src[i] > min_cor) {
			int kde_x = static_cast<int>((src[i]-min) / ratio);
			for (auto j = 0; j < kernel_size; j++) {
				dst[kde_x - kernel_half_size + j] += kernel[j];
			}
		}
	}
	dst.front() = dst.back() = 0;
}

void RetreiveKdePeak(std::vector<double> const &kde,
	std::vector<KdePeak> &peaks, double max, double min, double bound_val_weight,
	double kde_slope, double kde_y_intercept, double window_height_weight) {
	if (!peaks.empty()) {
		peaks.clear();
	}
	if (bound_val_weight > 1) {
		bound_val_weight = 1;
		std::cout << "bound_val_weight for RetreiveKdePeak is bigger than 1\n";
	}
	else if (bound_val_weight < 0) {
		bound_val_weight = 0;
		std::cout << "bound_val_weight for RetreiveKdePeak is less than 0\n";
	}
	auto kde_width = kde.size();
	int offset = static_cast<int>(
		static_cast<double>(kde_width - 1) / (max - min)*min);
	// 1 for ascending and -1 for descending
	int prev_dir = kde[1] > kde[0] ? 1 : -1;
	for (auto i = 1; i < kde_width - 1; i++) {
		int crt_dir = kde[i + 1] > kde[i] ? 1 : -1;
		if (prev_dir == 1 && crt_dir == -1) {
			if (kde[i] > kde_slope*i + kde_y_intercept) {
				KdePeak peak;
				auto thh = kde[i] * bound_val_weight;
				int il = i, ir = i;
				while (kde[il] > thh/* && il > 0*/) {
					if (kde[il] > kde[il - 1]) {
						il--;
					}else{
						break;
					}
				}
				while (kde[ir] > thh/* && ir < kde_width - 1*/) {
					if (kde[ir] > kde[ir + 1]) {
						ir++;
					}else{
						break;
					}
				}
				int window_height = static_cast<int>(
					(i + offset) * window_height_weight);
				peak.SetWindowHeight(window_height);
				double mean = static_cast<double>(i)
					/ static_cast<double>(kde_width - 1)*(max - min) + min;
				double left = static_cast<double>(il)
					/ static_cast<double>(kde_width - 1)*(max - min) + min;
				double right = static_cast<double>(ir)
					/ static_cast<double>(kde_width - 1)*(max - min) + min;
				peak.SetVal(mean, right, left);
				peaks.push_back(std::move(peak));
				//std::cout << "width = " << 0.12* 0.36/left - 0.12* 0.36 / right << std::endl << std::endl;
			}
		}
		prev_dir = crt_dir;
	}
}

FilterFlag FilterWal(std::vector<double> &stx, int start, int step,
	double mean, double min, double max, double disp_max, double disp_min) {
	double sum = 0.0;
	double average = 0.0;
	double count_inside = 0.0;
	double count_unbeyond = 0.0;
	if (step == 0) {
		step = 1;
		std::cout << "step = 0\n";
	}
	// caculate the average disparity value in filter block
	for (int i = start; i < start + step; i++) {
		if (stx[i] <= disp_max && stx[i] >= disp_min) {
			sum += stx[i];
			count_unbeyond += 1.0;
			// the number of pixel inside the block
			if (stx[i] <= max && stx[i] >= min) {
				count_inside += 1.0;
			}
		}
	}
	average = sum > 0.000001 ? (sum / count_unbeyond) : 0.0;
	double percent_inside = count_inside / count_unbeyond;
	// std::cout << sum << " " << count << " " << average << " " << mean << " " << min << " " << max << "\n";
	if ((average <= max && average >= min)) { // && (percent_inside>0.1)
		auto flag = kWall;
		return flag;
	}
	else {
		auto flag = kNone;
		return flag;
	}
}

double DispAverage(std::vector<double> &stx, int start, int end, double min, double max) {
	int step = end - start + 1;
	double sum = 0.0;
	double average = 0.0;
	double count_inside = 0.0;
	// caculate the average disparity value in filter block
	int start_temp = start + floor(step / 10);
	int end_temp = end - floor(step / 10);
	for (int i = (start_temp); i < end_temp; i++) {
		// the number of pixel inside the block
		if (stx[i] <= max && stx[i] >= min) {
			count_inside += 1.0;
			sum += stx[i];
		}
	}
	average = sum / count_inside;
	return average = average;
}

double WallDispAverage(std::vector<double> &stx, const ObjectMask& mask_wall, double min, double max) {
	double sum = 0.0;
	double average = 0.0;
	double count_inside = 0.0;
	for (int wall_i = mask_wall.mask_.size() - 1; wall_i >= 0; wall_i--) {
		int wall_first = (mask_wall.mask_[wall_i].first);
		int wall_second = (mask_wall.mask_[wall_i].second);
		int step = wall_second - wall_first + 1;
		// caculate the average disparity value in filter block
		int start_temp = wall_first + floor(step / 10);
		int end_temp = wall_second - floor(step / 10);
		for (int i = (start_temp); i < end_temp; i++) {
			// the number of pixel inside the block
			if (stx[i] <= max && stx[i] >= min) {
				count_inside += 1.0;
				sum += stx[i];
			}
		}
	}
	if (count_inside > 0) {
		average = sum / count_inside;
		return average = average;
	}
}

// height_ is the height of image
void RefineWal(std::vector<double> &stx, KdePeak &peak,
	StixelMask &mask, double center_v_) {
	double min = peak.left_;
	double max = peak.right_;
	auto disp = peak.mean_;
	int height_ = stx.size();
	int window_height = peak.window_height_;
	int wall_first;
	int wall_second;
	auto& temp_wall = mask.wall(disp);
	//auto n_mask_wall = temp_wall.mask_.size();
	for (int wall_i = 0; wall_i < temp_wall.mask_.size(); wall_i++) {
		wall_first = temp_wall.mask_[wall_i].first;
		wall_second = temp_wall.mask_[wall_i].second;

		int wall_first_refined = wall_first;
		int wall_second_refined = wall_second;
		// refine the first pixel
		if (stx[wall_first] <= max && stx[wall_first] >= min) {
			int end_i = (wall_first - window_height) > 0 ? wall_first - window_height : 0;
			for (int i = wall_first - 1; i >= end_i; i--) {
				if (stx[i] <= max && stx[i] >= min) {
					wall_first_refined = i;
				}
			}
			mask.MarkWall(disp, wall_first_refined, wall_first);
		}
		else {
			int end_i = (wall_first + window_height) < height_ ? wall_first + window_height : height_;
			for (int i = wall_first + 1; i < end_i; i++) {
				if (stx[i] <= max && stx[i] >= min) {
					wall_first_refined = i;
					break;
				}
			}
			mask.UnMarkWall(disp, wall_first, wall_first_refined - 1);
		}
		// refine the second pixel
		if (stx[wall_second] <= max && stx[wall_second] >= min) {
			int end_i = (wall_second + window_height) < height_ ? wall_second + window_height : height_;
			for (int i = wall_second + 1; i < end_i; i++) {
				if (stx[i] <= max && stx[i] >= min) {
					wall_second_refined = i;
				}
			}
			mask.MarkWall(disp, wall_second, wall_second_refined);
		}
		else {
			int end_i = (wall_second - window_height) > 0 ? wall_second - window_height : 0;
			for (int i = wall_second - 1; i >= end_i; i--) {
				if (stx[i] <= max && stx[i] >= min) {
					wall_second_refined = i;
					break;
				}
			}
			mask.UnMarkWall(disp, wall_second_refined + 1, wall_second);
		}

		Line2dFitted line{ stx, wall_first_refined, wall_second_refined };
		auto distance2horizon = line.Dist(center_v_, 0);
		// TODO (the angle is not sure)
		if ((distance2horizon < THRESHOLD_DISTANCE_HORIZON) &&
			(abs(line.slope()) > THRESHOLD_SLOPE_HORIZON)) {
			mask.UnMarkWall(disp, wall_first_refined, wall_second_refined);
		}
	}
	double average_disp;
	if (temp_wall.mask_.size() >= 1) {
		average_disp = WallDispAverage(stx, temp_wall, min, max);
		mask.ChangeWallDisp(disp, average_disp);

		auto& average_temp_wall = mask.wall(average_disp);
		if (average_temp_wall.mask_.size() >= 2) {
			for (int wall_i = 0; wall_i < (average_temp_wall.mask_.size() - 1); wall_i++) {
				// get the adjacent mask wall distance
				auto wall_second_last = average_temp_wall.mask_[wall_i].second;
				auto wall_first_next = average_temp_wall.mask_[wall_i + 1].first;
				if ((wall_first_next - wall_second_last) < 2 * window_height) {
					mask.MarkWall(average_disp, wall_second_last, wall_first_next);
					wall_i = wall_i - 1;
				}
			}
		}
	}
}

// cluters the super pillars in xy dimensions
// faster than CluterSuperPillarsOpti (0.2-0.3ms)
void CluterSuperPillars(std::vector<std::vector<SuperPillar>> &src,
	SuperPillarCluster &dst) {
	bool is_brought_in = false;
	auto n_col = src.size();
	for (int i_col = 0; i_col <= n_col - 1; i_col++) {
		if (!src[i_col].empty()) {
			auto n_sp = src[i_col].size();
			for (int i_sp = 0; i_sp <= n_sp - 1; i_sp++) {
				if (!dst.empty()) {
					is_brought_in = false;
					int n_cluster = static_cast<int>(dst.size());
					for (auto i = n_cluster - 1; i >= 0; i--) {
						double dist = pow(dst[i].back().x() - src[i_col][i_sp].x(), 2)
							+ pow(dst[i].back().y() - src[i_col][i_sp].y(), 2);
						if (dist <= pow(THRESHOLD_CLUSTER_DIS, 2)) {
							dst[i].push_back(src[i_col][i_sp]);
							is_brought_in = true;
							break;
						}
					}
					if (!is_brought_in) {
						std::vector<SuperPillar> sps_temp;
						sps_temp.push_back(src[i_col][i_sp]);
						dst.push_back(std::move(sps_temp));
					}
				}
				else {
					std::vector<SuperPillar> sps_temp;
					sps_temp.push_back(src[i_col][i_sp]);
					dst.push_back(std::move(sps_temp));
				}
			}
		}
	}
	SetSuperPillarTopBottom(dst);
	FlatWall(dst);
}

// sets the visual top and bottom
void SetSuperPillarTopBottom(SuperPillarCluster &super_pillar_clusters) {
	auto n_cluster = super_pillar_clusters.size();
	for (auto i = 0; i < n_cluster; i++) {
		if(super_pillar_clusters[i].empty()){
			continue;
		}
		double z_max = super_pillar_clusters[i][0].top();
		double z_min = super_pillar_clusters[i][0].bottom();
		auto clusteri_num = super_pillar_clusters[i].size();
		for (int j = 0; j < clusteri_num; j++) {
			if (z_max < super_pillar_clusters[i][j].top()) {
				z_max = super_pillar_clusters[i][j].top();
			}
			if (z_min > super_pillar_clusters[i][j].bottom()) {
				z_min = super_pillar_clusters[i][j].bottom();
			}
		}
		for (int j = 0; j < clusteri_num; j++) {
			super_pillar_clusters[i][j].SetVirtualEnds(z_max, z_min);
		}
	}
}

void FlatWall(SuperPillarCluster &super_pillar_clusters) {
	for (auto &cluster : super_pillar_clusters) {
		if (!cluster.empty()) {
			double wall_top = cluster[0].top();
			double wall_bottom = cluster[0].bottom();
			for (auto &sp : cluster) {
				if (sp.is_top_beyond()) {
					if (wall_top < sp.real_top()) {
						wall_top = sp.real_top();
					}
				}
				if (sp.is_bottom_beyond()) {
					if (wall_bottom > sp.real_bottom()) {
						wall_bottom = sp.real_bottom();
					}
				}
			}
			for (auto &sp : cluster) {
				if (sp.is_top_beyond()) {
					sp.real_top(wall_top);
				}
				if (sp.is_bottom_beyond()) {
					sp.real_bottom(wall_bottom);
				}
			}
		}
	}
}

// cluters the super pillars in xy dimensions
void CluterSuperPillarsOpti(std::vector<std::vector<SuperPillar>>& superpillar_frame,
	std::vector<std::vector<SuperPillar>>& cluster_surperpillar) {
	bool is_brought_in = false;
	std::vector<SuperPillar > superpillar_frame_temp;
	auto size_superpillar = superpillar_frame.size();
	for (int superpillar_i = 0; superpillar_i <= size_superpillar - 1; superpillar_i++) {
		if (!superpillar_frame[superpillar_i].empty()) {
			auto size_in_superpillar = superpillar_frame[superpillar_i].size();
			for (int in_superpillar_i = 0; in_superpillar_i <= size_in_superpillar - 1; in_superpillar_i++) {
				superpillar_frame_temp.push_back(superpillar_frame[superpillar_i][in_superpillar_i]);
			}
		}
	}
	if (cluster_surperpillar.empty()) {
		std::vector<SuperPillar> superpillar_clustertemp;
		superpillar_clustertemp.push_back(superpillar_frame_temp[0]);
		cluster_surperpillar.push_back(superpillar_clustertemp);
	}
	auto superpillar_frame_temp_size = superpillar_frame_temp.size();
	for (int superpillar_i = 1; superpillar_i <= superpillar_frame_temp_size - 1; superpillar_i++) {
		is_brought_in = false;
		int n_cluster = static_cast<int>(cluster_surperpillar.size());
		for (auto i = n_cluster - 1; i >= 0; i--) {
			double dist = pow(cluster_surperpillar[i].back().x() - superpillar_frame_temp[superpillar_i].x(), 2)
				+ pow(cluster_surperpillar[i].back().y() - superpillar_frame_temp[superpillar_i].y(), 2);
			if (dist <= pow(THRESHOLD_CLUSTER_DIS, 2)) {
				cluster_surperpillar[i].push_back(superpillar_frame_temp[superpillar_i]);
				is_brought_in = true;
				break;
			}
		}
		if (!is_brought_in) {
			std::vector<SuperPillar> superpillar_clustertemp;
			superpillar_clustertemp.push_back(superpillar_frame_temp[superpillar_i]);
			cluster_surperpillar.push_back(superpillar_clustertemp);
		}
	}
}


/*
	coordinate system from AirSim to this
	AirSim:
        y
      /
    0------x
    |
	|
	z
	this:
	z
    |
    |   y
    | /
    0------x
 */
Point3D TransformAirsimCoor(double x, double y, double z) {
	//return Point3D{ y, x, -(z-1.5) }; // NOTE: -1.5 is an experience correction
	return Point3D{ y, x, -z };
}

Point3D TransformGPS2Plane(GPSPosition basePoint, GPSPosition point) {
	Point3D position_xy;
	double MACRO_AXIS = 6378137;
	double MINOR_AXIS = 6356752;
	double a = pow(MACRO_AXIS, 2.0);
	double b = pow(MINOR_AXIS, 2.0);
	double c = pow(tan(basePoint.latitude_), 2.0);
	double d = pow(1 / tan(basePoint.latitude_), 2.0);
	double x = a / sqrt(a + b * c);
	double y = b / sqrt(b + a * d);
	c = pow(tan(point.latitude_), 2.0);
	d = pow(1 / tan(point.latitude_), 2.0);

	double m = a / sqrt(a + b * c);
	double n = b / sqrt(b + a * d);
	double distance_Y = sqrt(abs(m - x)*abs(m - x) + abs(n - y)*abs(n - y));
	if (point.latitude_ > point.latitude_) {
		position_xy.y_ = distance_Y / 100;  //(mm -> m)
	}
	else {
		position_xy.y_ = -distance_Y / 100;
	}
	// return x axis which is in Longtitude
	a = pow(MACRO_AXIS, 2.0);
	b = pow(MINOR_AXIS, 2.0);
	c = pow(tan(basePoint.latitude_), 2.0);
	x = a / sqrt(a + b * c);
	position_xy.x_ = x * (point.longtitude_ - basePoint.longtitude_) / 100;

	return position_xy;
}

void DynamicWindowCluster(std::vector<SuperPillar> &sps,
	std::vector<std::vector<Pillar>> &clusters) {
	for (const auto& sp : sps) {

	}
}

const std::vector<cv::Point> neighbors = { {1, -1}, {1, 0}, {1, 1}, {0, -1}, {0, 1},  {-1, -1}, {-1, 0}, {-1, 1} };
//		 jump out of the loop on finding the first open neighbor point, save this point in vn and un, and return true
//		 if the loop ends, which means no open neighbor point found, return false
bool FindOpenNeighbor(int v, int u, cv::Mat &img,
	std::vector<std::vector<bool>> &mask, int &vn, int &un) {

	int cols = img.cols;
	int rows = img.rows;
	int v_temp;
	int u_temp;
	for (int i = -3; i < 4; i++) {
		for (int j = -3; j < 4; j++) {
			if ((i == 0) && (j == 0)) {
			}
			else {
				v_temp = v + i;
				u_temp = u + j;
				if (((v_temp >= 0) && (v_temp < rows)) && ((u_temp >= 0) && (u_temp < cols))) {
					if (mask[v_temp][u_temp] == false) {
						if (img.at<uchar>(v_temp, u_temp) != 0) {
							vn = v_temp;
							un = u_temp;
							return true;
						}
					}
				}
			}

		}
	}
	return false;

}

int NeighborNumber(int v, int u, cv::Mat &img) {
	int neighbor_cnt = 0;
	for (const auto& neighbor : neighbors) {
		if (img.at<uchar>(v + neighbor.x, u + neighbor.y) != 0) {
			neighbor_cnt = neighbor_cnt + 1;
		}
	}
	return neighbor_cnt;
}

bool IsTurnPoint(int v, int u, cv::Mat &img) {
	int neighbor_cnt = NeighborNumber(v, u, img);
	if (neighbor_cnt == 0) {
		return true;
	}
	else if (neighbor_cnt == 1) {
		return true;
	}
	else if ((img.at<uchar>(v + 1, u) && img.at<uchar>(v - 1, u)) ||
		(img.at<uchar>(v, u + 1) && img.at<uchar>(v, u - 1))) {
		return false;
	}
	else if ((neighbor_cnt == 2) && ((img.at<uchar>(v + 1, u + 1) && img.at<uchar>(v - 1, u - 1)) ||
		(img.at<uchar>(v - 1, u + 1) && img.at<uchar>(v + 1, u - 1)))) {
		return false;
	}
	else {
		for (const auto& neighbor1 : neighbors) {
			for (const auto& neighbor2 : neighbors) {
				auto cross = (double)(neighbor1.x * neighbor2.x + neighbor1.y * neighbor2.y) /
					(sqrt(pow(neighbor1.x, 2) + pow(neighbor1.y, 2)) + sqrt(pow(neighbor2.x, 2) + pow(neighbor2.y, 2)));
				if (cross == 0) {
					return true;
				}
				else if ((cross < -0.6) && (cross < -0.8)) {
					return true;
				}
			}
		}
	}
	return false;
}

void ClusterCore(int v, int u, cv::Mat &img,
	std::vector<std::vector<bool>> &mask, std::vector<cv::Point> &cluster) {
	if (mask[v][u] == true) {
		return;
	}
	else {
		mask[v][u] = true;
	}
	if (img.at<uchar>(v, u) != 0) {
		if (IsTurnPoint(v, u, img)) {
			// std::cout << " IsTurnPoint (" << v << " , " << u << " )" << std::endl;
			cluster.push_back(cv::Point{ v, u });
		}
		int vn, un;
		while (FindOpenNeighbor(v, u, img, mask, vn, un)) {
			ClusterCore(vn, un, img, mask, cluster);
		}
	}
	else {
		return;
	}

}

void Cluster(cv::Mat &img, std::vector<std::vector<cv::Point>> &dst) {
	int cols = img.cols;
	int rows = img.rows;
	std::vector<bool> temp(cols, false);
	std::vector<std::vector<bool>> mask(rows, temp);
	std::vector<cv::Point> cluster;
	for (auto v = 1; v < rows - 1; v++) {
		for (auto u = 1; u < cols - 1; u++) {
			ClusterCore(v, u, img, mask, cluster);
			if (!cluster.empty()) {
				dst.push_back(cluster);
				cluster.clear();
			}
		}
	}
}
// if the mask if all false, return true
bool MaskAllTrue(std::vector<bool> &mask) {
	int mask_cnt = 0;
	for (int i = 0; i < mask.size(); i++) {
		if (mask[i] == false) {
			mask_cnt = mask_cnt + 1;
		}
	}
	if (mask_cnt > 1) {
		return true;
	}
	return false;
}

void TracingPrism(ScaledDisparityFrame &disparity_frame, int width_, int height_, double disp_max_, double disp_min_,
	double focus_, double baseline_, double center_u_, double center_v_, std::vector<Plane> &plane_dst,
	int stixel_width, double noise_amp_, double resolution_gridmap_) {
	// camera coor to world coor
	double height_uav = disparity_frame.pos_camera_.z_;
	double area_width = 50;
	double area_length = 30;
	int cols = static_cast<int>(area_width / resolution_gridmap_);
	int rows = static_cast<int>(area_length / resolution_gridmap_);
	cv::Mat gridmap_temp(rows, cols, CV_8UC1, cv::Scalar(0));
	// store the height of gird map
	std::vector<double> height_temp(cols, (double)0);
	std::vector<std::vector<double>> plane_height(rows, height_temp);
	double z_temp;
	double y_temp;
	double x_temp;
	int u_temp;
	int v_temp;
	auto &stixel_temps = disparity_frame.data_;
	auto stixels_n = disparity_frame.size();
	Point2D point;
	// transform the disparity image to grid map
	for (int i = 0; i < stixels_n; i++) {
		for (int j = 0; j < height_; j++) {
			if ((stixel_temps[i][j] < disp_max_) && (stixel_temps[i][j] > disp_min_)) {
				// add noise noise_amp_*[-0.5,0.5]
				double y_real = baseline_ * focus_ / stixel_temps[i][j];
				double z_real = (j - center_v_) * y_temp / focus_ / width_ + height_uav;
				y_temp = baseline_ * focus_ / stixel_temps[i][j] + noise_amp_ * (rand() / double(RAND_MAX) - 0.5);
				z_temp = (j - center_v_) * y_temp / focus_ / width_ + height_uav;
				x_temp = static_cast<double>(i*stixel_width + floor(stixel_width / 2) - center_u_) * y_temp / focus_ / width_;
				if (x_temp > -20 && x_temp < 20 && z_temp > 0.5 && y_temp < 20 && y_temp > 0) {
					u_temp = floor(x_temp / resolution_gridmap_) + cols / 2;
					v_temp = floor(y_temp / resolution_gridmap_);
					gridmap_temp.at<uchar>(v_temp, u_temp) = 255;
					if (z_temp > plane_height[v_temp][u_temp]) {
						plane_height[v_temp][u_temp] = z_temp;
					}
				}
			}
		}
	}
	cv::Mat gridmap_edge(rows, cols, CV_8UC1, cv::Scalar(0));
	// get the edge image
	for (int i = 1; i < rows - 1; i++) {
		for (int j = 1; j < cols - 1; j++) {
			if (gridmap_temp.at<uchar>(i, j) != 0) {
				uchar unedge_flag = (gridmap_temp.at<uchar>(i + 1, j) && gridmap_temp.at<uchar>(i - 1, j))
					&& (gridmap_temp.at<uchar>(i, j + 1) && gridmap_temp.at<uchar>(i, j - 1));
				if (j >= (cols / 2)) {
					uchar shelter_flag = gridmap_temp.at<uchar>(i, j - 1) && gridmap_temp.at<uchar>(i - 1, j)
						&& gridmap_temp.at<uchar>(i - 1, j - 1);
					// belong to edge and unisolate
					if ((unedge_flag == 0) && (shelter_flag == 0)) {
						gridmap_edge.at<uchar>(i, j) = gridmap_temp.at<uchar>(i, j);
					}
				}
				else {
					uchar shelter_flag = gridmap_temp.at<uchar>(i, j + 1) && gridmap_temp.at<uchar>(i - 1, j)
						&& gridmap_temp.at<uchar>(i - 1, j + 1);
					// belong to edge and unisolate
					if ((unedge_flag == 0) && (shelter_flag == 0)) {
						gridmap_edge.at<uchar>(i, j) = gridmap_temp.at<uchar>(i, j);
					}
				}
			}
		}
	}
	// get the trun point and cluster the turn point according to the adjacent relationship
	std::vector<std::vector<cv::Point>> cluster_dst;
	cv::Mat gridmap_turnpoint(rows, cols, CV_8UC1, cv::Scalar(0));
	Cluster(gridmap_edge, cluster_dst);
	for (int i = 0; i < cluster_dst.size(); i++) {
		for (int j = 0; j < cluster_dst[i].size(); j++) {
			gridmap_turnpoint.at<uchar>(cluster_dst[i][j].x, cluster_dst[i][j].y) = 255;
		}
	}
	// erase the middle point
	for (int i = 0; i < cluster_dst.size(); i++) {
		if (cluster_dst[i].size() > 3)
		{
			for (int j = 0; j < cluster_dst[i].size(); j++) {
				for (int w = 0; w < cluster_dst[i].size(); w++) {
					if (((((cluster_dst[i][j].x - cluster_dst[i][w].x) == 1) && ((cluster_dst[i][j].y - cluster_dst[i][w].y) == 0)) &&
						(((cluster_dst[i][j].x - cluster_dst[i][w].x) == -1) && ((cluster_dst[i][j].y - cluster_dst[i][w].y) == 0))) ||
						((((cluster_dst[i][j].x - cluster_dst[i][w].x) == 0) && ((cluster_dst[i][j].y - cluster_dst[i][w].y) == 1)) &&
						(((cluster_dst[i][j].x - cluster_dst[i][w].x) == 0) && ((cluster_dst[i][j].y - cluster_dst[i][w].y) == -1)))) {
						cluster_dst[i].erase(cluster_dst[i].begin() + j);
						j = 0;
						break;
					}
				}
			}
		}

	}

	// erase the adjacent but away point
	for (int i = 0; i < cluster_dst.size(); i++) {
		if (cluster_dst[i].size() > 3)
		{
			for (int j = 0; j < cluster_dst[i].size(); j++) {
				if (cluster_dst[i][j].y <= cols / 2) {
					for (int w = 0; w < cluster_dst[i].size(); w++) {
						if ((((cluster_dst[i][j].x - cluster_dst[i][w].x) == 1) && ((cluster_dst[i][j].y - cluster_dst[i][w].y) == 0)) ||
							(((cluster_dst[i][j].x - cluster_dst[i][w].x) == 0) && ((cluster_dst[i][j].y - cluster_dst[i][w].y) == -1))) {
							cluster_dst[i].erase(cluster_dst[i].begin() + j);
							j = 0;
							break;
						}
					}
				}else{
					for (int w = 0; w < cluster_dst[i].size(); w++) {
						if ((((cluster_dst[i][j].x - cluster_dst[i][w].x) == 1) && ((cluster_dst[i][j].y - cluster_dst[i][w].y) == 0)) ||
							(((cluster_dst[i][j].x - cluster_dst[i][w].x) == 0) && ((cluster_dst[i][j].y - cluster_dst[i][w].y) == 1))) {
							cluster_dst[i].erase(cluster_dst[i].begin() + j);
							j = 0;
							break;
						}
					}
				}
			}
		}

	}
	cv::Mat gridmap_planepoint(rows, cols, CV_8UC1, cv::Scalar(0));
	for (int i = 0; i < cluster_dst.size(); i++) {
		for (int j = 0; j < cluster_dst[i].size(); j++) {
			gridmap_planepoint.at<uchar>(cluster_dst[i][j].x, cluster_dst[i][j].y) = 255;
		}
	}

	for (int i = 0; i < cluster_dst.size(); i++) {
		if (cluster_dst[i].size() > 1) {
			int max_x = INT_MIN;
			int min_x = INT_MAX;
			int max_y = INT_MIN;
			int min_y = INT_MAX;
			for (int j = 0; j < cluster_dst[i].size(); j++) {
				if (cluster_dst[i][j].x < min_x) {
					min_x = cluster_dst[i][j].x;
				}
				if (cluster_dst[i][j].x > max_x) {
					max_x = cluster_dst[i][j].x;
				}
				if (cluster_dst[i][j].y < min_y) {
					min_y = cluster_dst[i][j].y;
				}
				if (cluster_dst[i][j].y > max_y) {
					max_y = cluster_dst[i][j].y;
				}
			}
			int start_j = -1;
			for (int j = 0; j < cluster_dst[i].size(); j++) {
				if (((cluster_dst[i][j].x == max_x) && (cluster_dst[i][j].y == max_y)) ||
					((cluster_dst[i][j].x == max_x) && (cluster_dst[i][j].y == min_y)) ||
					((cluster_dst[i][j].x == min_x) && (cluster_dst[i][j].y == max_y)) ||
					((cluster_dst[i][j].x == min_x) && (cluster_dst[i][j].y == min_y))) {
					start_j = j;
					break;
				}
			}
			// the max method cound not find the end point in the cluster
			if (start_j == -1) {
				for (int j = 0; j < cluster_dst[i].size(); j++) {
					if (cluster_dst[i][j].x == max_x) {
						start_j = j;
						break;
					}
				}
			}
			double plane_z_max = 0;
			for (int j = 0; j < cluster_dst[i].size(); j++) {
				if (plane_z_max < plane_height[(cluster_dst[i][j].x)][(cluster_dst[i][j].y)]) {
					plane_z_max = plane_height[(cluster_dst[i][j].x)][(cluster_dst[i][j].y)];
				}
			}
			std::vector<bool> mask_temp(cluster_dst[i].size(), false);

			while (MaskAllTrue(mask_temp)) {
				double distance_min = cols * cols + rows * rows;
				int distance_min_j = 0;
				for (int j = 0; j < cluster_dst[i].size(); j++) {
					if ((start_j != j) && (mask_temp[j] == false)) {
						// mahalanobis distance
						double distance_temp = pow(abs((cluster_dst[i][start_j].x - cluster_dst[i][j].x)), 1) +
							pow(abs((cluster_dst[i][start_j].y - cluster_dst[i][j].y)), 1);
						if (distance_temp < distance_min) {
							distance_min = distance_temp;
							distance_min_j = j;
						}
					}
				}
				Point3D point1_{ (double)((cluster_dst[i][start_j].y + 0.5 - cols / 2)*resolution_gridmap_),
					(double)((cluster_dst[i][start_j].x + 0.5)*resolution_gridmap_), 0 };
				Point3D point2_{ (double)((cluster_dst[i][distance_min_j].y + 0.5 - cols / 2)*resolution_gridmap_),
					(double)((cluster_dst[i][distance_min_j].x + 0.5)*resolution_gridmap_), plane_z_max };
				Plane plane{ point1_ , point2_ };
				plane_dst.push_back(plane);
				mask_temp[start_j] = true;
				start_j = distance_min_j;
			}
		}
		else {
			Point3D point1_{ (double)((cluster_dst[i][0].y - cols / 2)*resolution_gridmap_),
					(double)((cluster_dst[i][0].x)*resolution_gridmap_), 0 };
			Point3D point2_{ (double)((cluster_dst[i][0].y + 1 - cols / 2)*resolution_gridmap_),
				(double)((cluster_dst[i][0].x + 1)*resolution_gridmap_), plane_height[(cluster_dst[i][0].x)][(cluster_dst[i][0].y)] };
			Plane plane{ point1_ , point2_ };
			plane_dst.push_back(plane);
		}

		/*
		std::cout << std::endl; std::cout << std::endl;
		for (int p = 0; p < cluster_dst[i].size(); p++) {
			std::cout << " ( " << cluster_dst[i][p].x << " , " << cluster_dst[i][p].y << " )" << std::endl;
		}
		std::cout << std::endl; std::cout << std::endl;*/
	}
	/*
	for (int p = 0; p < plane_dst.size(); p++) {
		plane_dst[p].Print();
	}*/
	// save plane to json file and show in matlab

	cv::imwrite("gridmap.jpg", gridmap_temp);
	cv::imwrite("gridmap_edge.jpg", gridmap_edge);
	cv::imwrite("gridmap_turnpoint.jpg", gridmap_turnpoint);
	cv::imwrite("gridmap_planepoint.jpg", gridmap_planepoint);
}
using json = nlohmann::json;
namespace fs = std::experimental::filesystem;
void SavePlaneData(std::vector<Plane> &plane_dst, std::string path) {
	json data_json;
	std::vector<std::vector<double>> coor;
	for (auto i = 0; i < plane_dst.size(); i++) {
		coor = plane_dst[i].GetCoor();
		data_json.push_back(coor);
	}
	// TODO: create a directory to store jsons
	if (fs::exists(path)) {
		fs::remove(path);
	}
	std::ofstream data_file{ path };
	data_file << std::setw(4) << data_json << std::endl;
	data_file.close();
}


cv::Point WorldToImg(Point3D point, double width_, double height_,
	double focus_, double baseline_, double center_u_, double center_v_) {

	double x_temp = point.x_;
	double y_temp = point.y_;
	double z_temp = point.z_;
	int u_temp = x_temp * focus_ * width_ / y_temp + center_u_;
	int v_temp = height_ - (z_temp * focus_ * width_ / y_temp + center_v_);
	cv::Point point_img{ u_temp, v_temp };
	return point_img;
}

// set the camera position to (0,0,0)
void DrawPlaneImage(std::vector<Plane> &planes, double height_uav,
	double width_, double height_, double focus_, double baseline_, double center_u_, double center_v_) {
	cv::Mat scene_left(height_, width_, CV_8UC3, cv::Scalar(0, 0, 0));
	for (auto plane_temp : planes) {
		cv::Point root_points[1][4];
		Point3D point_leftTop{ plane_temp.p1().x_, plane_temp.p1().y_, plane_temp.p2().z_ - height_uav };
		cv::Point imgpoint_leftTop = WorldToImg(point_leftTop, width_,
			height_, focus_, baseline_, center_u_, center_v_);

		Point3D point_leftBot{ plane_temp.p1().x_, plane_temp.p1().y_, plane_temp.p1().z_ - height_uav };
		cv::Point imgpoint_leftBot = WorldToImg(point_leftBot, width_,
			height_, focus_, baseline_, center_u_, center_v_);

		Point3D point_RightTop{ plane_temp.p2().x_, plane_temp.p2().y_, plane_temp.p1().z_ - height_uav };
		cv::Point imgpoint_RightTop = WorldToImg(point_RightTop, width_,
			height_, focus_, baseline_, center_u_, center_v_);

		Point3D point_RightBot{ plane_temp.p2().x_, plane_temp.p2().y_, plane_temp.p2().z_ - height_uav };
		cv::Point imgpoint_RightBot = WorldToImg(point_RightBot, width_,
			height_, focus_, baseline_, center_u_, center_v_);

		root_points[0][0] = imgpoint_leftTop;
		root_points[0][1] = imgpoint_leftBot;
		root_points[0][2] = imgpoint_RightTop;
		root_points[0][3] = imgpoint_RightBot;

		const cv::Point* ppt[1] = { root_points[0] };
		int color_rgb = floor(255 * (rand() / double(RAND_MAX)));
		CvScalar color = cvScalar(color_rgb, color_rgb, color_rgb);
		//int color = floor(255 * (rand() / double(RAND_MAX)));
		int npt[] = { 4 };
		cv::polylines(scene_left, ppt, npt, 1, 1, color, 1, 8, 0);
		cv::fillPoly(scene_left, ppt, npt, 1, color);
	}
	//cv::cvtColor(scene_left, scene_left, cv::COLOR_GRAY2BGRA);
	cv::imshow("Test", scene_left);
	cv::waitKey();
}

// error caclution in compact map
void ErrorCaculate(ScaledDisparityFrame &disparity_frame, int width_, int height_, double disp_max_, double disp_min_,
	double focus_, double baseline_, double center_u_, double center_v_, std::vector<Plane> &plane_dst,
	double& error_ave, double radius, double& per_error_in_radius, int stixel_width, int error_type) {

	// camera coor to world coor
	double cos_omega = cos(disparity_frame.angle_camera_.yaw_);
	double sin_omega = sin(disparity_frame.angle_camera_.yaw_);
	double height_uav = disparity_frame.pos_camera_.z_;


	double z_temp;
	double y_temp;
	double x_temp;

	auto &stixel_temps = disparity_frame.data_;
	auto stixels_n = stixel_temps.size();
	Point2D point;
	double error_sum = 0;
	error_ave = 0;
	per_error_in_radius = 0;
	int error_cnt = 0;
	int error_in_radius_cnt = 0;
	double p_world_x, p_world_y;
	// transform the disparity image to grid map
	for (int i = 0; i < stixels_n; i++) {
		for (int j = 0; j < height_; j++) {
			if ((stixel_temps[i][j] < disp_max_) && (stixel_temps[i][j] > disp_min_)) {
				// add noise [-0.5,0.5]
				y_temp = baseline_ * focus_ / stixel_temps[i][j];
				z_temp = (j - center_v_) * y_temp / focus_ / width_ + height_uav;
				x_temp = static_cast<double>(i*stixel_width + floor(stixel_width / 2) - center_u_) * y_temp / focus_ / width_;
				if (x_temp > -20 && x_temp < 20 && z_temp > 0.3) {
					if (error_type == compact_plane) {
						p_world_x = cos_omega * x_temp + sin_omega * y_temp
							+ disparity_frame.pos_camera_.x_;
						p_world_y = cos_omega * y_temp - sin_omega * x_temp
							+ disparity_frame.pos_camera_.y_;
					}
					else if (error_type == tracing_grid) {
						p_world_x = x_temp;
						p_world_y = y_temp;
					}
					else {
						std::cout << "error type is not exist" << std::endl;
					}
					//std::cout << "y_temp = " << y_temp << std::endl;
					Point3D point_temp{ p_world_x, p_world_y, z_temp };
					double error_min = HUGE_VAL;
					for (auto &plane_temp : plane_dst) {
						double error_temp = PointToPlane(point_temp, plane_temp);
						if (error_temp < error_min) {
							error_min = error_temp;
						}
					}
					error_sum = error_sum + error_min;
					error_cnt = error_cnt + 1;
					if (error_min < radius) {
						error_in_radius_cnt = error_in_radius_cnt + 1;
					}
				}
			}
		}
	}
	error_ave = error_sum / error_cnt;
	per_error_in_radius = error_in_radius_cnt / error_cnt;
}

} // namespace droneworld