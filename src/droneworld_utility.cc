#include "droneworld_utility.h"

namespace droneworld {

std::ostream& operator<<(std::ostream &os, ConfigMode mode) {
	switch (mode) {
		case ConfigMode::kOnline:{
			os << "Config Online";
			break;
		}
		case ConfigMode::kRecord:{
			os << "Config Record";
			break;
		}
		case ConfigMode::kOffline:{
			os << "Config Offline";
			break;
		}
		case ConfigMode::kKitti:{
			os << "Config Kitti";
			break;
		}
		case ConfigMode::kIdle:{
			os << "Config Idle";
			break;
		}
		default:{
			os << "no such a mode";
			dwtrap("no such a mode");
			break;
		}
	}
	return os;
}
std::ostream& operator<<(std::ostream &os, PlayMode mode){
	switch(mode){
		case PlayMode::kAuto:{
			os << "Play Auto";
			break;
		}
		case PlayMode::kSingleFrame:{
			os << "Play SingleFrame";
			break;
		}
		default:{
			os << "no such a play mode";
			dwtrap("no such a play mode");
			break;
		}
	}
	return os;
}

} // namespace droneworld