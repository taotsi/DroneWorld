// TODO: remove these inclusions
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
// #include <GL/gl3w.h>
// #include <GLFW/glfw3.h>
#define STB_IMAGE_IMPLEMENTATION
// #include <stb/stb_image.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "shader.h"
#include "world.h"

namespace droneworld{

World::World(){
	config_ = std::make_shared<Config>(config_path_);
	gui_ = std::make_shared<Gui>(config_);

	tc_.AddSubscriber({MsgType::kOut, "sensor_1"}, gui_);
	tc_.AddSubscriber({MsgType::kOut, "sensor_2"}, gui_);
	tc_.AddPublisher({MsgType::kOut, "test"}, gui_);
}

World::~World() {

}

std::string World::config_path_ = "./conf/config.json";

void World::Configure(std::string config_path){
    config_path_ = config_path;
}
void World::Begin(){
    is_on_ = true;
	gui_->Begin();
}
void World::Loop(){
    Begin();
    dwinfo("World initialized, starting looping...");
    switch(config_->mode()){
        case ConfigMode::kOnline:{
            LoopOnline();
            break;
        }
        case ConfigMode::kOffline:{
            LoopOffline();
            break;
        }
        // TODO: There's a bug that if the drone is changing its pose too fast,
        //       the raw angle retreived wont be quite accurate.
        //		 You'll have to hold up your retreiving untill it hovers or flies straight
        case ConfigMode::kRecord:{
            LoopOnline();
            break;
        }
        case ConfigMode::kKitti:{
            LoopOffline();
            break;
        }
        default:{
            dwtrap();
            break;
        }
    }
	tc_.TurnOff();
    dwinfo("World loop ends now");
}
void World::LoopOnline(){
    auto previous = std::chrono::system_clock::now();
    while (is_on_ && gui_->IsOn()){
        auto current = std::chrono::system_clock::now();
        std::chrono::duration<double, std::milli> elapsed = current - previous;
        Update(elapsed.count());
        previous = current;
    }
    dwinfo("Loop Online exit now");
}
void World::Update(double delta_time){
	gui_->Update(delta_time);
}
void World::LoopOffline(){
    if(config_->play_mode() == PlayMode::kAuto){
        dwinfo("Offline Mode, be patient please...");
        /* put World::Test() here, otherwise the program will do nothing */
        /* for example:
            auto sw= [this](int w){this->gui_->drone_manager_->stixel_width(w);};
            Test(sw, 1, 10, 1);
         */

        // auto sw= [this](int w){this->gui_->drone_manager_->stixel_width(w);};
		auto dummy = [](int x){};
        Test(dummy, 1, 1, 1);

        dwinfo("Offline Mode out, waiting for other thread now...");
    }else{ // PlayMode::kSingleFrame
        while(!gui_->drone_manager_->GetDrone()->is_dataset_all_run() && is_on_){
            Update(0);
        }
    }
    while(gui_->IsOn());
}


/* GUI */
// void World::GuiThreadMain(){
//     glfwSetErrorCallback(glfw_error_callback);
// 	if (!glfwInit())
// 		return;

//     const char* glsl_version = "#version 130";
// 	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
// 	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
// 	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

// 	// create windows (M*N)
// 	GLFWwindow* window_ = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Drone World", NULL, NULL);
// 	if (window_ == NULL) {
// 		glfwTerminate();
// 		return;
// 	}

// 	glfwMakeContextCurrent(window_);
// 	glfwSetFramebufferSizeCallback(window_, framebuffer_size_callback);
// 	glfwSwapInterval(1); // Enable vsync

// 	bool err = gl3wInit() != 0;
// 	if (err){
// 		fprintf(stderr, "Failed to initialize OpenGL loader!\n");
// 		return;
// 	}

//     glEnable(GL_DEPTH_TEST);

//     Shader shader_compact_model{"./src/shader/compact_model.vs", "./src/shader/compact_model.fs"};
//     Shader shader_flight_path{"./src/shader/path.vs", "./src/shader/path.fs"};
//     Shader shader_point_cloud{"./src/shader/point_cloud.vs", "./src/shader/point_cloud.fs"};

// 	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

//     unsigned int texture1;
//     glGenTextures(1, &texture1);
//     glBindTexture(GL_TEXTURE_2D, texture1);
//     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
//     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
//     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//     int width, height, nrChannels;
//     stbi_set_flip_vertically_on_load(true); // tell stb_image.h to flip loaded texture's on the y-axis.
//     // NOTE: I failed to load .png I made using photoshop 2019, .jpg works though
//     unsigned char *data = stbi_load("./misc/yb256.jpg", &width, &height, &nrChannels, 0);
//     if (data){
//         glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
//         glGenerateMipmap(GL_TEXTURE_2D);
//     }else{
//         std::cout << "Failed to load texture" << std::endl;
//     }
//     stbi_image_free(data);
//     shader_compact_model.use();
//     shader_compact_model.setInt("texture1", 0);

// 	IMGUI_CHECKVERSION();
// 	ImGui::CreateContext();
// 	ImGuiIO& io = ImGui::GetIO(); (void)io;
// 	//io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;  // Enable Keyboard Controls
// 	//io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;   // Enable Gamepad Controls

// 	ImGui_ImplGlfw_InitForOpenGL(window_, true);
// 	ImGui_ImplOpenGL3_Init(glsl_version);

// 	io.Fonts->AddFontFromFileTTF("./misc/DroidSans.ttf", 20.0f);

// 	bool show_demo_window = false;

// 	// GUI Loop
// 	while (!glfwWindowShouldClose(window_) && is_on_){
// 		processInput(window_);

// 		ImGui_ImplOpenGL3_NewFrame();
// 		ImGui_ImplGlfw_NewFrame();
// 		ImGui::NewFrame();

// 		if (show_demo_window)
// 			ImGui::ShowDemoWindow(&show_demo_window);
//         // gets new pose, later used for flight path
//         static DronePose pose;
//         if(config_->mode() != ConfigMode::kOffline){
//             pose = CmdPose();
//             UpdatePath(pose.x(), pose.y(), pose.z(), 0.25);
//         }
// 		// GUI interface window
// 		GuiPanel(pose);
// 		//GuiNodeInput();

// 		ImGui::Render();
// 		// glfwMakeContextCurrent(window);
// 		glClearColor(0.9f, 0.9f, 0.9f, 1.f);
// 		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

//         glActiveTexture(GL_TEXTURE0);
//         glBindTexture(GL_TEXTURE_2D, texture1);

//         // create transformations
//         glm::mat4 model         = glm::mat4(1.0f); // make sure to initialize matrix to identity matrix first
//         glm::mat4 view;//          = glm::mat4(1.0f);
//         glm::mat4 projection    = glm::mat4(1.0f);
//         model = glm::rotate(model, 0.f, glm::vec3(0.f, 1.0f, 0.0f));
// 		float camX = sin(glfwGetTime()) * node_map_range;
// 		float camZ = cos(glfwGetTime()) * node_map_range;
//         auto p_center = glm::vec3(*(flight_path_gl_.rbegin()+2), *(flight_path_gl_.rbegin()+1), *flight_path_gl_.rbegin());
//         auto p_eye = glm::vec3(*(flight_path_gl_.rbegin()+2)+camX, *(flight_path_gl_.rbegin()+1)+1.f, *flight_path_gl_.rbegin()+camZ);
// 		view = glm::lookAt(p_eye, p_center, glm::vec3(0.0f, 1.0f, 0.0f));
//         projection = glm::perspective(glm::radians(60.0f), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
//         /* render map data */
//         if(render_compact_model_){
//             RenderCompactModel(shader_compact_model, model, view, projection);
//         }
//         if(render_flight_path_ && config_->mode() != ConfigMode::kOffline){
//             RenderFlightPath(shader_flight_path, model, view, projection);
//         }
//         if(render_point_cloud_ && config_->mode() != ConfigMode::kOffline){
//             RenderPointCloud(shader_point_cloud, model, view, projection);
//         }

// 		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

// 		//glfwMakeContextCurrent(window_);
// 		glfwSwapBuffers(window_);
// 		// Poll and handle events (inputs, window resize, etc.)
// 		// You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
// 		// - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
// 		// - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
// 		// Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
// 		glfwPollEvents();
// 	}

// 	ImGui_ImplOpenGL3_Shutdown();
// 	ImGui_ImplGlfw_Shutdown();
// 	ImGui::DestroyContext();

// 	glfwDestroyWindow(window_);
// 	glfwTerminate();

//     is_on_ = false;
// }

/* GUI */
// void World::GuiTrajectoryMain(){
// 	//GuiThreadMain();
// 	glfwSetErrorCallback(glfw_error_callback);
// 	if (!glfwInit())
// 		return;

// 	const char* glsl_version = "#version 130";
// 	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
// 	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
// 	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

// 	// create windows (M*N)
// 	GLFWwindow* window_ = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "trajectory draw", NULL, NULL);
// 	if (window_ == NULL) {
// 		glfwTerminate();
// 		return;
// 	}

// 	glfwMakeContextCurrent(window_);
// 	glfwSetFramebufferSizeCallback(window_, framebuffer_size_callback);
// 	glfwSwapInterval(1); // Enable vsync

// 	bool err = gl3wInit() != 0;
// 	if (err) {
// 		fprintf(stderr, "Failed to initialize OpenGL loader!\n");
// 		return;
// 	}

// 	glEnable(GL_DEPTH_TEST);

// 	Shader shader_compact_model{ "./src/shader/compact_model.vs", "./src/shader/compact_model.fs" };
// 	Shader shader_flight_path{ "./src/shader/path.vs", "./src/shader/path.fs" };
// 	Shader shader_point_cloud{ "./src/shader/point_cloud.vs", "./src/shader/point_cloud.fs" };

// 	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

// 	unsigned int texture1;
// 	glGenTextures(1, &texture1);
// 	glBindTexture(GL_TEXTURE_2D, texture1);
// 	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
// 	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
// 	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
// 	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
// 	int width, height, nrChannels;
// 	stbi_set_flip_vertically_on_load(true); // tell stb_image.h to flip loaded texture's on the y-axis.
// 	// NOTE: I failed to load .png I made using photoshop 2019, .jpg works though
// 	unsigned char *data = stbi_load("./misc/yb256.jpg", &width, &height, &nrChannels, 0);
// 	if (data) {
// 		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
// 		glGenerateMipmap(GL_TEXTURE_2D);
// 	}
// 	else {
// 		std::cout << "Failed to load texture" << std::endl;
// 	}
// 	stbi_image_free(data);
// 	shader_compact_model.use();
// 	shader_compact_model.setInt("texture1", 0);

// 	IMGUI_CHECKVERSION();
// 	ImGui::CreateContext();
// 	ImGuiIO& io = ImGui::GetIO(); (void)io;
// 	//io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;  // Enable Keyboard Controls
// 	//io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;   // Enable Gamepad Controls

// 	ImGui_ImplGlfw_InitForOpenGL(window_, true);
// 	ImGui_ImplOpenGL3_Init(glsl_version);

// 	io.Fonts->AddFontFromFileTTF("./misc/DroidSans.ttf", 20.0f);

// 	bool show_demo_window = false;

// 	//const ImVec2 pos(0, 0);
// 	//ImGui::SetWindowPos(pos, 0);

// 	// GUI Loop
// 	while (!glfwWindowShouldClose(window_) && is_on_) {
// 		processInput(window_);

// 		ImGui_ImplOpenGL3_NewFrame();
// 		ImGui_ImplGlfw_NewFrame();
// 		ImGui::NewFrame();

// 		if (show_demo_window)
// 			ImGui::ShowDemoWindow(&show_demo_window);
// 		// gets new pose, later used for flight path
// 		static DronePose pose;
// 		if (config_->mode() != ConfigMode::kOffline) {
// 			pose = CmdPose();
// 			UpdatePath(pose.x(), pose.y(), pose.z(), 0.25);
// 		}
// 		// GUI interface window
// 		GuiNodeInput();
// 		SensorSample();
// 		GuiPanel(pose);

// 		ImGui::Render();
// 		// glfwMakeContextCurrent(window);
// 		glClearColor(0.9f, 0.9f, 0.9f, 1.f);
// 		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

// 		glActiveTexture(GL_TEXTURE0);
// 		glBindTexture(GL_TEXTURE_2D, texture1);

// 		// create transformations
// 		glm::mat4 model = glm::mat4(1.0f); // make sure to initialize matrix to identity matrix first
// 		glm::mat4 view;//          = glm::mat4(1.0f);
// 		glm::mat4 projection = glm::mat4(1.0f);
// 		model = glm::rotate(model, 0.f, glm::vec3(0.f, 1.0f, 0.0f));
// 		auto p_center = glm::vec3(*(flight_path_gl_.rbegin() + 2), *(flight_path_gl_.rbegin() + 1), *flight_path_gl_.rbegin());
// 		auto p_eye = glm::vec3(*(flight_path_gl_.rbegin() + 2), *(flight_path_gl_.rbegin() + 1) + 1.f, *flight_path_gl_.rbegin());
// 		view = glm::lookAt(p_eye, p_center, glm::vec3(0.0f, 0.0f, -1.0f));
// 		//projection = glm::perspective(glm::radians(60.0f), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
// 		projection = glm::ortho(-WINDOW_SIZE, WINDOW_SIZE, -WINDOW_SIZE, WINDOW_SIZE, -20.0f, 100.0f);

// 		// glfwMakeContextCurrent(window);
// 		glClearColor(0.9f, 0.9f, 0.9f, 1.f);
// 		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

// 		glActiveTexture(GL_TEXTURE0);
// 		glBindTexture(GL_TEXTURE_2D, texture1);
// 		// draw sensor position on the 2D map
// 		glViewport(0, 0, SCR_WIDTH / 2, SCR_HEIGHT / 2);
// 		RenderNodePosition(shader_compact_model, model, view, projection);
// 		// draw trajectory of UAV
// 		RenderTrajectory(shader_flight_path, model, view, projection);

// 		// create transformations
// 		float camX = sin(glfwGetTime()) * map_range_;
// 		float camZ = cos(glfwGetTime()) * map_range_;
// 		p_center = glm::vec3(*(flight_path_gl_.rbegin() + 2), *(flight_path_gl_.rbegin() + 1), *flight_path_gl_.rbegin());
// 		p_eye = glm::vec3(*(flight_path_gl_.rbegin() + 2) + camX, *(flight_path_gl_.rbegin() + 1) + 1.f, *flight_path_gl_.rbegin() + camZ);
// 		view = glm::lookAt(p_eye, p_center, glm::vec3(0.0f, 1.0f, 0.0f));
// 		projection = glm::perspective(glm::radians(45.f), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
// 		/* render map data */
// 		glViewport(SCR_WIDTH / 2, 0, SCR_WIDTH / 2, SCR_HEIGHT / 2);
// 		if (render_compact_model_) {
// 			RenderCompactModel(shader_compact_model, model, view, projection);
// 		}
// 		if (render_flight_path_ && config_->mode() != ConfigMode::kOffline) {
// 			RenderFlightPath(shader_flight_path, model, view, projection);
// 		}
// 		if (render_point_cloud_ && config_->mode() != ConfigMode::kOffline) {
// 			RenderPointCloud(shader_point_cloud, model, view, projection);
// 		}


// 		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

// 		//glfwMakeContextCurrent(window_);
// 		glfwSwapBuffers(window_);
// 		// Poll and handle events (inputs, window resize, etc.)
// 		// You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
// 		// - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
// 		// - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
// 		// Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
// 		glfwPollEvents();
// 	}

// 	ImGui_ImplOpenGL3_Shutdown();
// 	ImGui_ImplGlfw_Shutdown();
// 	ImGui::DestroyContext();

// 	glfwDestroyWindow(window_);
// 	glfwTerminate();

// 	is_on_ = false;
// }

// void World::GuiNodeInput() {
// 	if (!ImGui::Begin("Trajectory Input")) {
// 		ImGui::End();
// 		return;
// 	}
// 	static float x = 0, y = 0, z = 0;
// 	ImGui::PushItemWidth(180);
// 	ImGui::InputFloat("x", &x, 1, 3, "%.1f");
// 	ImGui::SameLine();
// 	ImGui::Text("now at %.1f", x); 
// 	ImGui::SameLine(); ImGui::Text("m");
// 	ImGui::InputFloat("y", &y, 1, 3, "%.1f");
// 	ImGui::SameLine();
// 	ImGui::Text("now at %.1f", y);
// 	ImGui::SameLine(); ImGui::Text("m");
// 	ImGui::InputFloat("z", &z, 1, 3, "%.1f");
// 	ImGui::SameLine();
// 	ImGui::Text("now at %.1f", z);
// 	ImGui::SameLine(); ImGui::Text("m");
// 	ImGui::PopItemWidth();

// 	static int button_id = 2;   //color set
// 	ImGui::PushID(button_id);
// 	ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(button_id / 7.0f, 0.6f, 0.6f));
// 	ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(button_id / 7.0f, 0.7f, 0.7f));
// 	ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(button_id / 7.0f, 0.8f, 0.8f));
// 	if (ImGui::Button("Add Sensor")) {
// 		ImGui::PopStyleColor(3);
// 		Point3D node(x,z,(y- OFF_SET_Y));
// 		node_position.push_back(node);
// 		nodes_num = nodes_num + 1;
// 	}
// 	ImGui::PopID();
// 	ImGui::SameLine();
// 	if (ImGui::Button("Clear Sensor")) {
// 		node_position.clear();
// 		nodes_num = 0;
// 	}

// 	ImGui::Separator();
// 	static float pre_map_range_;
// 	pre_map_range_ = node_map_range;
// 	ImGui::Text("Map Range");
// 	if ((ImGui::InputFloat(" ", &node_map_range, 2.0, 5.0, "%.0f"))) {
// 		auto coe = [coe = pre_map_range_ / node_map_range](auto &ele){return coe * ele; };
// 		std::transform(flight_path_gl_.begin(), flight_path_gl_.end(), flight_path_gl_.begin(), coe);
// 		std::transform(point_cloud_gl_.begin(), point_cloud_gl_.end(), point_cloud_gl_.begin(), coe);
// 	}
// 	ImGui::SameLine(); ImGui::Text("Map Range");

// 	ImGui::Separator();
// 	ImGui::Text("Period of UAV's trajectory");
// 	static float T = 0;
// 	ImGui::PushItemWidth(180);
// 	ImGui::InputFloat("T", &T, 1, 3, "%.1f");
// 	ImGui::SameLine();
// 	ImGui::Text("= %.1f", T);
// 	ImGui::SameLine(); ImGui::Text("s");
// 	ImGui::PopItemWidth();
	
// 	ImGui::Separator();
// 	ImGui::TextColored(ImVec4(1.0f, 0.0f, 1.0f, 1.0f), "UAV Trajectroy --");
// 	ImGui::TextColored(ImVec4(0.5, 0.5, 0.8, 1.0), "Sensor Position *");

// 	static bool show_demo_window = false;
// 	ImGui::Checkbox("Imgui Demo Window", &show_demo_window);
// 	if (show_demo_window) {
// 		ImGui::ShowDemoWindow(&show_demo_window);
// 	}

// }

// void World::SensorSample() {
// 	if (!ImGui::Begin("Sensor Sample")) {
// 		ImGui::End();
// 		return;
// 	}
// 	ImGui::Text("Temperature (C) ");
// 	ImGui::Separator();
// 	ImGui::Separator();
// 	float tem[] = { 23.f, 30.f, 20.f, 15.f, 26.f, 28.f, 12.f, 17.f };
// 	ImGui::PlotHistogram("Temperature range: [0,100]", tem, IM_ARRAYSIZE(tem), 0, NULL, 0.0f, 100.0f, ImVec2(0, 90));

// 	ImGui::Separator();
// 	ImGui::Text("Salinity (S) [0,2000]");
// 	float sal[] = { 203.f, 300.f, 200.f, 105.f, 206.f, 208.f, 102.f, 107.f };
// 	ImGui::PlotHistogram("Salinity range: [0,2000]", sal, IM_ARRAYSIZE(sal), 0, NULL, 0.0f, 2000.f, ImVec2(0, 90));
// 	ImGui::Separator();
// 	ImGui::Text("Note: The Sensor data collected by UAV");
// }

// void World::RenderNodePosition(Shader &shader, glm::mat4 &model, glm::mat4 &view, glm::mat4 &projection){
 
//     auto &data = node_position;
//     /* draw model data */
//     for(auto &it : data){
//         auto &node = it.SafeScale(static_cast<double>(1/ node_map_range / WINDOW_SIZE));
// 		float node_size = 0.3 / node_map_range;
// 		GLfloat vertices[] = {
// 			node.X_() - 0.7*node_size, node.Z_() - 0.5*node_size, -node.Y_(),
// 			node.X_(), node.Z_() + node_size, -node.Y_(),
// 			node.X_() + 0.7*node_size, node.Z_() - 0.5*node_size, -node.Y_()
// 		};
// 		//std::cout << node.X_() - 0.966*node_size << " " << node.Z_() - 0.5*node_size << " " << -node.Y_() << std::endl;
// 		// �����������
// 		GLuint VAOId, VBOId;
// 		// Step1: ��������VAO����
// 		glGenVertexArrays(1, &VAOId);
// 		glBindVertexArray(VAOId);
// 		// Step2: ��������VBO����
// 		glGenBuffers(1, &VBOId);
// 		glBindBuffer(GL_ARRAY_BUFFER, VBOId);
// 		// Step3: ����ռ� ��������
// 		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
// 		// Step4: ָ��������ʽ  �����ö�������
// 		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GL_FLOAT), (GLvoid*)0);
// 		glEnableVertexAttribArray(0);
// 		glBindBuffer(GL_ARRAY_BUFFER, 0);
// 		glBindVertexArray(0);

// 		// Section2 ׼����ɫ������
// 		// Step1: ׼����ɫ��Դ����
// 		const GLchar* vertexShaderSource = "#version 330\n"
// 			"layout(location = 0) in vec3 position;\n"
// 			"void main()\n"
// 			"{\n gl_Position = vec4(position, 1.0);\n}";
// 		const GLchar* fragShaderSource = "#version 330\n"
// 			"out vec4 color;\n"
// 			"void main()\n"
// 			"{\n color = vec4(0.5, 0.5, 0.8, 1.0);\n}";
// 		// Step2 ����Shader object
// 		// ������ɫ��
// 		GLuint vertexShaderId = glCreateShader(GL_VERTEX_SHADER);
// 		glShaderSource(vertexShaderId, 1, &vertexShaderSource, NULL);
// 		glCompileShader(vertexShaderId);
// 		GLint compileStatus = 0;
// 		glGetShaderiv(vertexShaderId, GL_COMPILE_STATUS, &compileStatus); // ������״̬
// 		if (compileStatus == GL_FALSE) // ��ȡ���󱨸�
// 		{
// 			GLint maxLength = 0;
// 			glGetShaderiv(vertexShaderId, GL_INFO_LOG_LENGTH, &maxLength);
// 			std::vector<GLchar> errLog(maxLength);
// 			glGetShaderInfoLog(vertexShaderId, maxLength, &maxLength, &errLog[0]);
// 			std::cout << "Error::shader vertex shader compile failed," << &errLog[0] << std::endl;
// 		}
// 		// ƬԪ��ɫ��
// 		GLuint fragShaderId = glCreateShader(GL_FRAGMENT_SHADER);
// 		glShaderSource(fragShaderId, 1, &fragShaderSource, NULL);
// 		glCompileShader(fragShaderId);
// 		glGetShaderiv(fragShaderId, GL_COMPILE_STATUS, &compileStatus);
// 		if (compileStatus == GL_FALSE)
// 		{
// 			GLint maxLength = 0;
// 			glGetShaderiv(fragShaderId, GL_INFO_LOG_LENGTH, &maxLength);
// 			std::vector<GLchar> errLog(maxLength);
// 			glGetShaderInfoLog(fragShaderId, maxLength, &maxLength, &errLog[0]);
// 			std::cout << "Error::shader fragment shader compile failed," << &errLog[0] << std::endl;
// 		}
// 		// Step3 �����γ� shader program object
// 		GLuint shaderProgramId = glCreateProgram();
// 		glAttachShader(shaderProgramId, vertexShaderId);
// 		glAttachShader(shaderProgramId, fragShaderId);
// 		glLinkProgram(shaderProgramId);
// 		GLint linkStatus;
// 		glGetProgramiv(shaderProgramId, GL_LINK_STATUS, &linkStatus);
// 		if (linkStatus == GL_FALSE)
// 		{
// 			GLint maxLength = 0;
// 			glGetProgramiv(shaderProgramId, GL_INFO_LOG_LENGTH, &maxLength);
// 			std::vector<GLchar> errLog(maxLength);
// 			glGetProgramInfoLog(shaderProgramId, maxLength, &maxLength, &errLog[0]);
// 			std::cout << "Error::shader link failed," << &errLog[0] << std::endl;
// 		}
// 		// ������ɺ�detach
// 		glDetachShader(shaderProgramId, vertexShaderId);
// 		glDetachShader(shaderProgramId, fragShaderId);
// 		// ����Ҫ���ӵ���������ʱ �ͷſռ�
// 		glDeleteShader(vertexShaderId);
// 		glDeleteShader(fragShaderId);
// 		// �����ɫ������ ����Ϊָ����ɫ
// 		//glClearColor(0.18f, 0.04f, 0.14f, 1.0f);
// 		//glClear(GL_COLOR_BUFFER_BIT);

// 		// ������д�������ƴ���
// 		glBindVertexArray(VAOId);
// 		glUseProgram(shaderProgramId);
// 		glDrawArrays(GL_TRIANGLES, 0, 3);

// 		glBindVertexArray(0);
// 		glUseProgram(0);
//     }
// 	/* draw model data */
// 	for (auto &it : data) {
// 		auto &node = it.SafeScale(static_cast<double>(1 / node_map_range / WINDOW_SIZE));
// 		float node_size = 0.3 / node_map_range;
// 		GLfloat vertices[] = {
// 			node.X_() - 0.7*node_size, node.Z_() + 0.5*node_size, -node.Y_(),
// 			node.X_(), node.Z_() - node_size, -node.Y_(),
// 			node.X_() + 0.7*node_size, node.Z_() + 0.5*node_size, -node.Y_()
// 		};
// 		//std::cout << node.X_() - 0.966*node_size << " " << node.Z_() - 0.5*node_size << " " << -node.Y_() << std::endl;
// 		// �����������
// 		GLuint VAOId, VBOId;
// 		// Step1: ��������VAO����
// 		glGenVertexArrays(1, &VAOId);
// 		glBindVertexArray(VAOId);
// 		// Step2: ��������VBO����
// 		glGenBuffers(1, &VBOId);
// 		glBindBuffer(GL_ARRAY_BUFFER, VBOId);
// 		// Step3: ����ռ� ��������
// 		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
// 		// Step4: ָ��������ʽ  �����ö�������
// 		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GL_FLOAT), (GLvoid*)0);
// 		glEnableVertexAttribArray(0);
// 		glBindBuffer(GL_ARRAY_BUFFER, 0);
// 		glBindVertexArray(0);

// 		// Section2 ׼����ɫ������
// 		// Step1: ׼����ɫ��Դ����
// 		const GLchar* vertexShaderSource = "#version 330\n"
// 			"layout(location = 0) in vec3 position;\n"
// 			"void main()\n"
// 			"{\n gl_Position = vec4(position, 1.0);\n}";
// 		const GLchar* fragShaderSource = "#version 330\n"
// 			"out vec4 color;\n"
// 			"void main()\n"
// 			"{\n color = vec4(0.5, 0.5, 0.8, 1.0);\n}";
// 		// Step2 ����Shader object
// 		// ������ɫ��
// 		GLuint vertexShaderId = glCreateShader(GL_VERTEX_SHADER);
// 		glShaderSource(vertexShaderId, 1, &vertexShaderSource, NULL);
// 		glCompileShader(vertexShaderId);
// 		GLint compileStatus = 0;
// 		glGetShaderiv(vertexShaderId, GL_COMPILE_STATUS, &compileStatus); // ������״̬
// 		if (compileStatus == GL_FALSE) // ��ȡ���󱨸�
// 		{
// 			GLint maxLength = 0;
// 			glGetShaderiv(vertexShaderId, GL_INFO_LOG_LENGTH, &maxLength);
// 			std::vector<GLchar> errLog(maxLength);
// 			glGetShaderInfoLog(vertexShaderId, maxLength, &maxLength, &errLog[0]);
// 			std::cout << "Error::shader vertex shader compile failed," << &errLog[0] << std::endl;
// 		}
// 		// ƬԪ��ɫ��
// 		GLuint fragShaderId = glCreateShader(GL_FRAGMENT_SHADER);
// 		glShaderSource(fragShaderId, 1, &fragShaderSource, NULL);
// 		glCompileShader(fragShaderId);
// 		glGetShaderiv(fragShaderId, GL_COMPILE_STATUS, &compileStatus);
// 		if (compileStatus == GL_FALSE)
// 		{
// 			GLint maxLength = 0;
// 			glGetShaderiv(fragShaderId, GL_INFO_LOG_LENGTH, &maxLength);
// 			std::vector<GLchar> errLog(maxLength);
// 			glGetShaderInfoLog(fragShaderId, maxLength, &maxLength, &errLog[0]);
// 			std::cout << "Error::shader fragment shader compile failed," << &errLog[0] << std::endl;
// 		}
// 		// Step3 �����γ� shader program object
// 		GLuint shaderProgramId = glCreateProgram();
// 		glAttachShader(shaderProgramId, vertexShaderId);
// 		glAttachShader(shaderProgramId, fragShaderId);
// 		glLinkProgram(shaderProgramId);
// 		GLint linkStatus;
// 		glGetProgramiv(shaderProgramId, GL_LINK_STATUS, &linkStatus);
// 		if (linkStatus == GL_FALSE)
// 		{
// 			GLint maxLength = 0;
// 			glGetProgramiv(shaderProgramId, GL_INFO_LOG_LENGTH, &maxLength);
// 			std::vector<GLchar> errLog(maxLength);
// 			glGetProgramInfoLog(shaderProgramId, maxLength, &maxLength, &errLog[0]);
// 			std::cout << "Error::shader link failed," << &errLog[0] << std::endl;
// 		}
// 		// ������ɺ�detach
// 		glDetachShader(shaderProgramId, vertexShaderId);
// 		glDetachShader(shaderProgramId, fragShaderId);
// 		// ����Ҫ���ӵ���������ʱ �ͷſռ�
// 		glDeleteShader(vertexShaderId);
// 		glDeleteShader(fragShaderId);
// 		// �����ɫ������ ����Ϊָ����ɫ
// 		//glClearColor(0.18f, 0.04f, 0.14f, 1.0f);
// 		//glClear(GL_COLOR_BUFFER_BIT);

// 		// ������д�������ƴ���
// 		glBindVertexArray(VAOId);
// 		glUseProgram(shaderProgramId);
// 		glDrawArrays(GL_TRIANGLES, 0, 3);

// 		glBindVertexArray(0);
// 		glUseProgram(0);
// 	}
// }

// void World::RenderFlightPath(Shader &shader, glm::mat4 &model, glm::mat4 &view, glm::mat4 &projection){
//     shader.use();
//     shader.setMat4("model", model);
//     shader.setMat4("view", view);
//     shader.setMat4("projection", projection);

//     if(is_flight_path_updated_){
//         // NOTE: haven't been fully tested
//         flight_path_gl_ = flight_path_;
//         std::transform(flight_path_gl_.begin(), flight_path_gl_.end(),
//             flight_path_gl_.begin(), [coe=1.f/map_range_](auto &ele){return coe*ele;});
//         is_flight_path_updated_ = false;
//     }
//     auto size = flight_path_gl_.size();
//     unsigned int vao_path, vbo_path;
//     glGenVertexArrays(1, &vao_path);
//     glGenBuffers(1, &vbo_path);
//     glBindVertexArray(vao_path);
//     glBindBuffer(GL_ARRAY_BUFFER, vbo_path);
//     glBufferData(GL_ARRAY_BUFFER, size*sizeof(float),
//         flight_path_gl_.data(), GL_STATIC_DRAW);
//     glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
//         3*sizeof(float), (void*)0);
//     glEnableVertexAttribArray(0);

//     glLineWidth(2);

//     glBindVertexArray(vao_path);
//     glDrawArrays(GL_LINE_STRIP, 0, size/3);
//     glDeleteVertexArrays(1, &vao_path);
//     glDeleteBuffers(1, &vbo_path);
// }

// void World::RenderTrajectory(Shader &shader, glm::mat4 &model, glm::mat4 &view, glm::mat4 &projection) {
// 		shader.use();
// 		shader.setMat4("model", model);
// 		shader.setMat4("view", view);
// 		shader.setMat4("projection", projection);

// 		if (is_flight_path_updated_) {
// 			 //NOTE: haven't been fully tested
// 			flight_path_gl_ = flight_path_opti_;
// 			std::transform(flight_path_gl_.begin(), flight_path_gl_.end(),
// 				flight_path_gl_.begin(), [coe = 1.f / node_map_range](auto &ele){return coe * ele; });
// 			is_flight_path_updated_ = false;
// 		}
// 		auto size = flight_path_gl_.size();
// 		unsigned int vao_path, vbo_path;
// 		glGenVertexArrays(1, &vao_path);
// 		glGenBuffers(1, &vbo_path);
// 		glBindVertexArray(vao_path);
// 		glBindBuffer(GL_ARRAY_BUFFER, vbo_path);
// 		glBufferData(GL_ARRAY_BUFFER, size * sizeof(float),
// 			flight_path_gl_.data(), GL_STATIC_DRAW);
// 		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
// 			3 * sizeof(float), (void*)0);
// 		glEnableVertexAttribArray(0);

// 		glLineWidth(2);

// 		glBindVertexArray(vao_path);
// 		glDrawArrays(GL_LINE_STRIP, 0, size / 3);
// 		glDeleteVertexArrays(1, &vao_path);
// 		glDeleteBuffers(1, &vbo_path);
// }

// void World::UpdatePath(float x, float y, float z, float epsilon){
//     if(!flight_path_.empty()){
//         if(abs((-y)-*flight_path_.rbegin()) >= epsilon ||
//             abs(z-*(flight_path_.rbegin()+1)) >= epsilon ||
//             abs(x-*(flight_path_.rbegin()+2)) >= epsilon){
//             flight_path_.push_back(x);     // ogl world X*map_range_
//             flight_path_.push_back(z);     // ogl world Y*map_range_
//             flight_path_.push_back(-y);    // ogl world Z*map_range_
//             is_flight_path_updated_ = true;
//         }
//     }else{
//         flight_path_.push_back(x);     // ogl world X*map_range_
//         flight_path_.push_back(z);     // ogl world Y*map_range_
//         flight_path_.push_back(-y);    // ogl world Z*map_range_
//     }
//     if(flight_path_.size() >= FLIGHT_PATH_CAPACITY){
//         flight_path_ = {flight_path_.begin()+FLIGHT_PATH_CAPACITY/5, flight_path_.end()};
//     }
// }

// void World::TransformPosition(Point3D node) {
// 	Point3D point_temp(node.X_(),node.Z_(),-node.Y_());
// 	node_position.push_back(point_temp);
// }

} // namespace droneworld
