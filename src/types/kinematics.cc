#include "types/kinematics.h"

namespace droneworld{

/* transforms the point position under the image coordinate
    to the camera coordinate */
Point3D ImgToCameraCoor(double disp_normalized, int u, int v,
    int u0, int v0, double baseline, double focus, double width){
	Point3D p;
    // y is the dead ahead of camera
    p.y_ = baseline * focus / disp_normalized;
    // z is the height of observed object
    p.z_ = (v-v0) * p.y_ / focus / width;
    // +x is the right direction
    p.x_ = (u-u0) * p.y_ / focus / width;
	return p;
}
/* transforms the point position under the camera coordinate
    to the world coordinate */
// uses rad instead of degree
Point3D CameraToWorldCoor(Point3D p_pos_camera_coor,
    Point3D camera_pos_world_coor, EulerAngle angle){
    Point3D p_pos_world_coor;
	double cos_omega = cos(angle.yaw_);
	double sin_omega = sin(angle.yaw_);
	// rotation
    // because of gimbal, only yaw changes
    p_pos_world_coor.x_ = cos_omega*p_pos_camera_coor.x_
        + sin_omega*p_pos_camera_coor.y_;
    p_pos_world_coor.y_ = cos_omega*p_pos_camera_coor.y_
        - sin_omega*p_pos_camera_coor.x_;
    p_pos_world_coor.z_ = p_pos_camera_coor.z_;
	// translation
	p_pos_world_coor.x_ += camera_pos_world_coor.x_;
	p_pos_world_coor.y_ += camera_pos_world_coor.y_;
	p_pos_world_coor.z_ += camera_pos_world_coor.z_;
	return p_pos_world_coor;
}

/*
    drone self-coordinate is like
    z
    |
    |   y
    | /
    0------x
    y is the direction the drone heads to.
    The world coordinate is the self-coordinate when the drone's all
    three pose angle are zeros, and point O is where the drone is born.
 */
void ImgToWorldCoor(SuperPillar &sp, double disp_normalized,
	    int u, std::vector<std::pair<int, int>> v, int u0, int v0,
	    double baseline, double focus, double width,
	    Point3D camera_pos, EulerAngle angle) {
    // image coor to camera coor
    double p_camera_y = baseline*focus/disp_normalized;
    double p_camera_x = (u-u0)*p_camera_y/focus/width;
    std::vector<std::pair<double, double>> p_camera_z;
    std::pair<double, double> temp_z;
    for(auto &it : v){
        temp_z.first = (it.first-v0)*p_camera_y/focus/width;
        temp_z.second = (it.second-v0)*p_camera_y/focus/width;
		//temp_z.second = std::max((temp_z.second), 0.0);
		//temp_z.first = std::max((temp_z.first), 0.0);
        p_camera_z.push_back(temp_z);
    }
    // camera coor to world coor
    double cos_omega = cos(angle.yaw_);
	double sin_omega = sin(angle.yaw_);
    double p_world_x = cos_omega*p_camera_x + sin_omega*p_camera_y
        + camera_pos.x_;
    sp.x(p_world_x);
    double p_world_y = cos_omega*p_camera_y - sin_omega*p_camera_x
        + camera_pos.y_;
    sp.y(p_world_y);
    for(auto &it : p_camera_z){
         temp_z.first = it.first + camera_pos.z_;
         temp_z.second = it.second + camera_pos.z_;
         sp.AddZPair(temp_z);
    }
}
/*
    drone self-coordinate is like
    z
    |
    |   y
    | /
    0------x
    y is the direction the drone heads to.
    The world coordinate is the self-coordinate when the drone's all
    three pose angle are zeros, and point O is where the drone is born.
 */
Point3D ImgToWorldCoor(double disp_normalized, int u, int v, int u0, int v0,
	double baseline, double focus, double width,
	Point3D camera_pos, EulerAngle angle){
    // image coor to camera coor
    double p_camera_y = baseline*focus/disp_normalized;
    double p_camera_x = (u-u0)*p_camera_y/focus/width;
    double p_camera_z = (v-v0)*p_camera_y/focus/width;
    // camera coor to world coor
    double cos_omega = cos(angle.yaw_);
	double sin_omega = sin(angle.yaw_);
    double p_world_x = cos_omega*p_camera_x + sin_omega*p_camera_y
        + camera_pos.x_;
    double p_world_y = cos_omega*p_camera_y - sin_omega*p_camera_x
        + camera_pos.y_;
    double p_world_z = p_camera_z + camera_pos.z_;
    return Point3D{p_world_x, p_world_y, p_world_z};
}
} // namespace droneworld