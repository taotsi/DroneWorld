#include "types/pillar.h"

namespace droneworld{

Pillar::Pillar(){

}
Pillar::Pillar(double x, double y, double z1, double z2)
    : x_(x), y_(y), z1_(z1), z2_(z2){
    if(z1_ > z2_){
        std::swap(z1_, z2_);
    }
}
Pillar::Pillar(const Pillar &other){
    x_ = other.x_;
    y_ = other.y_;
    z1_ = other.z1_;
    z2_ = other.z2_;
}
Pillar::Pillar(Pillar &&other){
    x_ = other.x_;
    y_ = other.y_;
    z1_ = other.z1_;
    z2_ = other.z2_;
}
Pillar& Pillar::operator=(const Pillar &other){
    if(this != &other){
        x_ = other.x_;
        y_ = other.y_;
        z1_ = other.z1_;
        z2_ = other.z2_;
        return *this;
    }else{
        return *this;
    }
}
Pillar& Pillar::operator=(Pillar &&other){
    if(this != &other){
        x_ = other.x_;
        y_ = other.y_;
        z1_ = other.z1_;
        z2_ = other.z2_;
        return *this;
    }else{
        return *this;
    }
}
double Pillar::DistHorizon(Pillar &other){
    return sqrt(pow(x() - other.x(), 2) + pow(y() - other.y(), 2));
}
void Pillar::x(double x){
    x_ = x;
}
double Pillar::x(){
    return x_;
}
void Pillar::y(double y){
    y_ = y;
}
double Pillar::y(){
    return y_;
}
void Pillar::z1(double z){
    z1_ = z;
    if(z1_ > z2_){
        std::swap(z1_, z2_);
    }
}
double Pillar::z1(){
    return z1_;
}
void Pillar::z2(double z){
    z2_ = z;
    if(z1_ > z2_){
        std::swap(z1_, z2_);
    }
}
double Pillar::z2(){
    return z2_;
}
std::ostream& operator<<(std::ostream &os, const Pillar &pl){
    os  << "( " << std::fixed << std::setw(7) << std::setprecision(3) << pl.x_
        << ", " << std::fixed << std::setw(7) << std::setprecision(3) << pl.y_
        << ", " << std::fixed << std::setw(7) << std::setprecision(3) << pl.z1_
        << ", " << std::fixed << std::setw(7) << std::setprecision(3) << pl.z2_
        << " )";
    return os;
}
std::vector<double> Pillar::GetCoor(){
    return std::vector<double>{x_, y_, z1_, z2_};
}

} // namespace droneworld