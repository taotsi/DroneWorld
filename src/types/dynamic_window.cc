#include "types/dynamic_window.h"

namespace droneworld {

DynamicWindow::DynamicWindow(){

}
DynamicWindow::DynamicWindow(double top, double bottom,
    int left, double lx, double ly)
    : top_{top}, bottom_{bottom},
    left_{left}, right_{left}, lx_{lx}, ly_{ly}, rx_{lx}, ry_{ly}{

}
DynamicWindow::~DynamicWindow(){

}
double DynamicWindow::height(){
    return top_ - bottom_;
}
double DynamicWindow::width(){
    return sqrt(pow(lx_-rx_, 2) + pow(ly_-ry_, 2));
}
double DynamicWindow::top(){
    return top_;
}
void DynamicWindow::top(double t){
    if(t < top_){
        if(t <= bottom_){
            top_ = bottom_;
        }else{
            top_ = t;
        }
    }
}
double DynamicWindow::bottom(){
    return bottom_;
}
void DynamicWindow::bottom(double b){
    if(b > bottom_){
        if(b >= top_){
            bottom_ = top_;
        }else{
            bottom_ = b;
        }
    }
}
int DynamicWindow::left(){
    return left_;
}
int DynamicWindow::right(){
    return right_;
}
void DynamicWindow::right(int r){
    if(r > right_){
        right_ = r;
    }
}
double DynamicWindow::lx(){
    return lx_;
}
// void DynamicWindow::lx(double x){
//     lx_ = x;
// }
double DynamicWindow::ly(){
    return ly_;
}
// void DynamicWindow::ly(double y){
//     ly_ = y;
// }
double DynamicWindow::rx(){
    return rx_;
}
// void DynamicWindow::rx(double x){
//     rx_ = x;
// }
double DynamicWindow::ry(){
    return ry_;
}
// void DynamicWindow::ry(double y){
//     ry_ = y;
// }
void DynamicWindow::TopDescend(double step){
    top(top_-step);
}
void DynamicWindow::BottomAscend(double step){
    bottom(bottom_+step);
}
void DynamicWindow::MarchRight(int r, double rx, double ry){
    right(r);
    rx_ = rx;
    ry_ = ry;
}
void DynamicWindow::MarchRight(double rx, double ry){
    right_++;
    rx_ = rx;
    ry_ = ry;
}
bool DynamicWindow::IsFixed(){
    return is_fixed_;
}
void DynamicWindow::Fix(){
    is_fixed_ = true;
}
MatchStat DynamicWindow::MatchComplement(double top, double bottom){
    if(top < bottom){
        std::swap(top, bottom);
    }
    if(bottom < this->bottom_){
        if(top <= this->bottom_){
            return MatchStat::kNotMatch;
        }else if(top < this->top_){
            return MatchStat::kMatchPartial;
        }else{
            return MatchStat::kMatchPartial;
        }
    }else if(bottom < this->top_){
        if(top <= this->top_){
            return MatchStat::kMatch;
        }else{
            return MatchStat::kMatchPartial;
        }
    }else{
        return MatchStat::kNotMatch;
    }
}
MatchStat DynamicWindow::MatchComplement(Complement &comp){
    auto stat = MatchComplement(comp.top(), comp.bottom());
    if(stat == MatchStat::kMatch || stat == MatchStat::kMatchPartial){
        comp.match_stat(stat);
    }
    return stat;
}
std::ostream& operator<<(std::ostream &os, const DynamicWindow &dw){
    os << "(" << dw.left_ << ", " << dw.right_ << ", [("
        << dw.lx_ << ", " << dw.ly_ << "), (" << dw.rx_ << ", " << dw.ry_
        << "); " << dw.bottom_ << ", " << dw.top_ << "])";
    return os;
}

SuperDynamicWindow::SuperDynamicWindow(){

}
SuperDynamicWindow::SuperDynamicWindow(double safe_height, double safe_width)
    : safe_height_{safe_height}, safe_width_{safe_width}{

}
SuperDynamicWindow::~SuperDynamicWindow(){

}
void SuperDynamicWindow::clear(){
    windows_.clear();
}
int SuperDynamicWindow::size(){
    return static_cast<int>(windows_.size());
}
void SuperDynamicWindow::SetSafeParam(double height, double width){
    safe_height_ = height;
    safe_width_ = width;
}
void SuperDynamicWindow::Cluster(SuperPillars &src,
        PillarCluster &dst, double epsilon){
    auto n_pl = src.size();
    for(auto i=0; i<n_pl-1; i++){
        //dwmsg("scan super pillar ", i);
        // dwmsg("sp ", i, ", wins size ", windows_.size());
        // std::cout << "before scanning\n" << *this;
        // dwi0("this pillar is like ", src[i]);
        ScanSuperPillar(i, src[i]);
        //dwi1("now spawning new windows for the unmatched complements, if any");
        int i_comp = 0;
        while(i_comp < src[i].complements_.size()){
            // NOTE: only unmatched complements will spawn new windows,
            //       and the partially matched ones will not
            auto &comp = src[i].complements_[i_comp];
            if(comp.match_stat() == MatchStat::kNotMatch){
                if(comp.height() >= safe_height_){
                    windows_.push_back(DynamicWindow{comp.top(), comp.bottom(),
                        i, src[i].x(), src[i].y()});
                    i_comp++;
                    // dwi0("add a new window from the left ones");
                }else{
                    src[i].FillComplement(i_comp);
                }
            }else{
                i_comp++;
            }
        }
    }
    if(n_pl > 1){
        ScanSuperPillar(static_cast<int>(n_pl)-1, src[static_cast<int>(n_pl)-1]);
    }

    // dwmsg("super pillar all scaned, windows all generated, ther are like\n", *this);
    for(auto i=0; i<n_pl; i++){
        //dwmsg("refine super pillar ", i, ": ", src[i]);
        RefineSuperPillar(i, src[i]);
    }
    Msg::dwmute();
    std::vector<std::vector<Pillar>> temp_pillars;
    temp_pillars.resize(n_pl, {});
    //dwmsg("spawning pillars now...");
    for(auto i=0; i<n_pl; i++){
        src[i].SpawnPillars(temp_pillars[i]);
    }
    //dwmsg("here are the spawned pillars");
    for(auto i=0; i<temp_pillars.size(); i++){
        //dwi1("mother super pillar ", i);
        for(auto &pl : temp_pillars[i]){
            //dwi2(pl);
        }
    }
    //dwmsg("clustering pillars now...");
    ClusterPillars(temp_pillars, dst, epsilon);
    //dwmsg("here are the clustered pillars:");
    for(auto i=0; i<dst.size(); i++){
        //dwi1("cluster ", i);
        for(auto &pl : dst[i]){
            //dwi2(pl);
        }
    }
    //dwmsg("cluster done");
    Msg::dwunmute();
}
// TODO: add a argument indicating the number of superpillars,
//       and if there's any window crossing the superpillars horizontally,
//       it should be left alive even if it's too narrow
// TODO: and if there's any window starting from the first superpillar,
//       don't remove it even if it's too narrow
void SuperDynamicWindow::ScanSuperPillar(int index, SuperPillar &sp){
    if(!windows_.empty()){
        //dwi1(windows_.size(), " windows now, they are\n", *this);
        int i_win = 0;
        while(i_win < windows_.size()){
            //dwi1("  window ", i_win);
            if(windows_[i_win].IsFixed()){
                //dwi2("fixed window, continue");
                i_win++;
                continue;
            }
            std::stack<Complement> stack_comp;
            for(auto &comp : sp.complements_){
                auto match_stat = windows_[i_win].MatchComplement(comp);
                if(match_stat == MatchStat::kMatch
                    || match_stat == MatchStat::kMatchPartial){
                    stack_comp.push(comp);
                }
            }
            auto n_match = stack_comp.size();
            //dwi2("number of matched complements ", n_match);
            if(n_match == 0){
                //dwi2("no matched complement in this super pillar");
                if(windows_[i_win].width() <= safe_width_){
                    //dwi2("    window width is NOT ok, it's dead now, ", windows_[i_win].width());
                    windows_.erase(windows_.begin()+i_win);
                }else{
                    //dwi2("    window width IS ok, it's fixed now, ", windows_[i_win].width());
                    windows_[i_win].Fix();
                    i_win++;
                }
            }else if(n_match >= 1){
                // for the first matched complement
                //dwi2("for the first matched complement:");
                auto &temp_comp = stack_comp.top();
                stack_comp.pop();
                //dwi2("this complement is like ", temp_comp);
				auto top_temp = std::min(windows_[i_win].top(), temp_comp.top());
				auto bottom_temp = std::max(windows_[i_win].bottom(), temp_comp.bottom());
				auto height_temp = top_temp - bottom_temp;
                if(height_temp >= safe_height_){
                    //dwi2("window height IS ok, ", windows_[i_win].height());
                    //dwi2("    before window ", i_win, " marching right, it's like", windows_[i_win]);
                    windows_[i_win].top(top_temp);
                    windows_[i_win].bottom(bottom_temp);
                    windows_[i_win].MarchRight(index, sp.x(), sp.y());
                    //dwi2("    now it's like ", windows_[i_win]);
                    i_win++;
                }else{
                    //dwi2("window height is NOT ok, ", windows_[i_win].height());
                    if(windows_[i_win].width() >= safe_width_){
                        //dwi2("    window width IS ok, it's fixed now, ", windows_[i_win].width());
                        windows_[i_win].Fix();
                    }else{
                        //dwi2("    window width is NOT ok, it's dead now, ", windows_[i_win].width());
                        windows_.erase(windows_.begin()+i_win);
                    }
                }
                // for the rest of the matched complements
                //dwi2("for the rest of the matched complements, if any:");
                for(auto j=1; j<n_match-1; j++){ // n_match-1 beause one has been popped
                    auto &temp_comp = stack_comp.top();
                    // NOTE: you can comment this if scope to simplify the algorithm
                    if(temp_comp.height() >= safe_height_){
                        // dwi2("    generate a new window");
                        windows_.insert(windows_.begin()+i_win, DynamicWindow{
                            temp_comp.top(), temp_comp.bottom(),
                            index, sp.x(), sp.y()});
                        i_win++;
                    }
                    stack_comp.pop();
                }
            }
        }
    }else{
        // dwi1("0 windows yet, gonna spawn some");
        SpawnWindwos(index, sp);
        // dwi1("now ", size(), " spawned, they are:\n", *this);
        //std::cout << *this;
    }
}
void SuperDynamicWindow::SpawnWindwos(int index, SuperPillar &sp){
    int i=0;
    while(i < sp.complements_.size()){
        auto &comp = sp.complements_[i];
        if(comp.height() >= safe_height_){
            windows_.push_back(DynamicWindow{comp.top(), comp.bottom(),
                index, sp.x(), sp.y()});
            comp.match_stat(MatchStat::kMatch);
            i++;
        }else{
            sp.FillComplement(i);
        }
    }
}
void SuperDynamicWindow::RefineSuperPillar(int index, SuperPillar &sp){
    auto n_window = windows_.size();
    int i = 0;
    // dwi0("super pillar ", index, " is like ", sp);
    // dwi0("before refining, there are ", sp.complements_.size(), " complements");
    while(i < sp.complements_.size()){
        // dwi1("complement ", i, " is like ", sp.complements_[i]);
        std::vector<DynamicWindow> fit_windows;
        for(auto j=0; j<n_window; j++){
            if(index >= windows_[j].left() && index <= windows_[j].right()){
                auto temp_stat = windows_[j].MatchComplement(sp.complements_[i]);
                // dwi2("found a window across this complement: ", temp_stat);
                if(temp_stat != MatchStat::kNotMatch){
                    // dwi2("and this window matches this complement");
                    fit_windows.push_back(windows_[j]);
                }
            }
        }
        auto n_fit_win = fit_windows.size();
        if(n_fit_win != 0){
            // dwi1(n_fit_win, " fit windwos");
            double max_top = fit_windows[0].top();
            double min_bottom = fit_windows[0].bottom();
            for(auto k=1; k<n_fit_win; k++){
                if(max_top < fit_windows[k].top()){
                    max_top = fit_windows[k].top();
                }
                if(min_bottom > fit_windows[k].bottom()){
                    min_bottom = fit_windows[k].bottom();
                }
            }
            // dwi1("  this complement will be narrowed to ", min_bottom, " and ", max_top);
            sp.NarrowComplement(i, max_top, min_bottom);
            // dwi1("  this super pillar now is like ", sp);
            i++;
        }else{
            // dwi1("NO fit windows, this complement will be filled and killed");
            sp.FillComplement(i);
            // dwi1("  this super pillar now is like ", sp);
        }
    }
}
void SuperDynamicWindow::ClusterPillars(std::vector<std::vector<Pillar>> &src,
        PillarCluster &dst, double epsilon){
    if(!src.empty()){
        //dwi0("initializes cluster containter");
        auto n_temp = src[0].size();
        for(int i=0; i<n_temp; i++){
            dst.push_back({src[0][i]});
        }
        //dwi0("  there're ", dst.size(), " clusters now");
        std::vector<int> cluster_flag;
        cluster_flag.resize(n_temp, 1);
        std::vector<int> pillar_flag;
        auto n_sp = src.size();
       // dwmsg(n_sp, " mother super pillars");
        for(int i=1; i<n_sp; i++){
            auto n_cluster = dst.size();
            auto n_pl = src[i].size();
            //dwi0("  children of mother super pillar ", i, ", and ", n_pl, " pillars");
            //dwi1("there're ", n_cluster, " clusters now");
            pillar_flag.clear();
            pillar_flag.resize(n_pl, 1);
            bool is_any_fit = false;
            for(auto j=0; j<n_cluster; j++){
                //dwi1("cluster ", j, ", end.z1 = ", dst[j].back().z1(), ", end.z2 = ", dst[j].back().z2());
                if(cluster_flag[j] == 1){
                    //dwi1("  cluster is active");
                    for(auto k=0; k<n_pl; k++){
                        //dwi1("  pillar ", k, ", z1 = ", src[i][k].z1(), ", z2 = ", src[i][k].z2());
                        if(pillar_flag[k] == 1){
                            if(abs(src[i][k].z1() - dst[j].back().z1()) <= epsilon
                                && abs(src[i][k].z2() - dst[j].back().z2()) <= epsilon){
                                //dwi2("found a pillar, pushing it back the cluster");
                                dst[j].push_back(src[i][k]);
                                pillar_flag[k] = 0;
                                is_any_fit = true;
                                break;
                            }
                        }
                    }
                    if(!is_any_fit){
                        //dwi2("No pillar clustered, this cluster is closed now");
                        cluster_flag[j] = 0;
                    }
                }else{
                    //dwi1("cluster is closed");
                }
            }
            auto n_pl_flag = pillar_flag.size();
            //dwi1("now spanwning new clusters ", n_pl_flag, " pillar flags");
            for(auto m=0; m<n_pl_flag; m++){
                if(pillar_flag[m] == 1){
                    //dwi2("this pillar hasn't been clusterd, ", m);
                    dst.push_back({src[i][m]});
                    cluster_flag.push_back(1);
                }else{
                    //dwi2("this pillar has been clustered, ", m);
                }
            }
            //dwi1("there're ", dst.size(), " clusters now");
        }
    }
}
// TODO:
void SuperDynamicWindow::SortWindows(){

}
// TODO:
void SuperDynamicWindow::MergeWindows(double epsilon){

}
std::ostream& operator<<(std::ostream &os, const SuperDynamicWindow &sdw){
    for(const auto &win : sdw.windows_){
        os << win << "\n";
    }
    return os;
}
} // namespace droneworld