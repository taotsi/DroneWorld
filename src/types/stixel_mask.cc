#include <cassert>
#include <cstdlib>
#include "types/stixel_mask.h"
#include "droneworld_utility.h"

namespace droneworld{

static std::string merge_stat_str[] = {
    "kLeft", "kRight", "kIntersect"
};

/* BaseMask */
BaseMask::BaseMask(){

}
BaseMask::BaseMask(int start, int end)
    : start_{start}, end_{end}{

}
BaseMask::BaseMask(int range_len)
    : start_{0}, end_{range_len-1}{

}
BaseMask::~BaseMask(){

}
void BaseMask::Mark(int left, int right){
    if(left > right){
        std::swap(left, right);
    }
    if(left < start_){
        left = start_;
    }
    if(right > end_){
        right = end_ ;
    }
    if(!mask_.empty()){
        MergeStat merge_stat;
        size_t i = 0;
        bool is_done = false;
        while(i <= mask_.size() && !is_done){
            if(i == mask_.size()){
                mask_.push_back({left, right});
                break;
            }
            merge_stat = Merge(mask_[i], left, right);
            switch(merge_stat){
                case MergeStat::kRight:{
                    i++;
                    break;
                }
                case MergeStat::kIntersect:{
                    mask_.erase(mask_.begin()+i);
                    break;
                }
                case MergeStat::kLeft:{
                    mask_.insert(mask_.begin()+i, {left, right});
                    is_done =  true;
                    break;
                }
                default:{
                    assert(false);
                    break;
                }
            }
        }
    }else{
        mask_.push_back({left, right});
    }
}
MergeStat BaseMask::Merge(std::pair<int, int> pair, int &left, int &right){
    if(right < pair.first-1){
        return MergeStat::kLeft;
    }
    if(left > pair.second+1){
        return MergeStat::kRight;
    }
    MergeStat stat = MergeStat::kIntersect;
    if(left >= pair.first-1 && left <= pair.second+1){
        if(left != pair.first-1){
            left = pair.first;
        }
    }
    if(right >= pair.first-1 && right <= pair.second+1){
        if(right != pair.second+1){
            right = pair.second;
        }
    }
    return stat;
}
void BaseMask::UnMark(int left, int right){
    if(left > right){
        std::swap(left, right);
    }
    if(left < start_){
        left = start_;
    }
    if(right > end_){
        right = end_ ;
    }
    if(!mask_.empty()){
        size_t i=0;
        bool is_done = false;
        while(i < mask_.size() && !is_done){
            if(i == mask_.size()){
                break;
            }
            auto del_stat = Del(mask_[i], left, right);
            switch(del_stat){
                case DelStat::kLeft:{
                    is_done = true;
                    break;
                }
                case DelStat::kRight:{
                    i++;
                    break;
                }
                case DelStat::kSplit:{
                    mask_.insert(mask_.begin()+i, {mask_[i].first, left-1});
                    mask_[i+1].first = right + 1;
                    i += 2;
                    break;
                }
                case DelStat::kTruncLeft:{
                    mask_[i].first = right + 1;
                    i++;
                    break;
                }
                case DelStat::kTruncRight:{
                    mask_[i].second = left - 1;
                    i++;
                    break;
                }
                case DelStat::kAll:{
                    mask_.erase(mask_.begin()+i);
                    break;
                }
                default:{
                    assert(false);
                    break;
                }
            }
        }
    }
}
DelStat BaseMask::Del(std::pair<int, int> pair, int left, int right){
    if(right < pair.first){
        return DelStat::kLeft;
    }else if(left > pair.second){
        return DelStat::kRight;
    }else if(left <= pair.first){ // while right >= pair.first
        if(right < pair.second){
            return DelStat::kTruncLeft;
        }else{
            return DelStat::kAll;
        }
    }else if(left > pair.first){ // while left <= pair.second
        if(right < pair.second){
            return DelStat::kSplit;
        }else{
            return DelStat::kTruncRight;
        }
    }else{
        dwerr("this msg should've never appeared");
        assert(false);
        return DelStat::kRight;
    }
}
int BaseMask::start() const{
    return start_;
}
void BaseMask::start(int s){
    start_ = s;
}
int BaseMask::end() const{
    return end_;
}
void BaseMask::end(int e){
    end_ = e;
}
bool BaseMask::empty(){
    return mask_.empty();
}
size_t BaseMask::size(){
    return mask_.size();
}
void BaseMask::clear(){
    mask_.clear();
}
std::pair<int, int> BaseMask::operator[](int i){
    return mask_[i];
}
BaseMask& BaseMask::operator+=(BaseMask &other){
    start_ = std::min(start_, other.start_);
    end_ = std::max(end_, other.end_);
    for(auto &it : other.mask_){
        Mark(it.first, it.second);
    }
    return *this;
}
BaseMask operator+(BaseMask &a, BaseMask &b){
    return a+=b;
}
std::ostream& operator<<(std::ostream &os, const BaseMask &mask){
    os << "[ ";
    for(auto &pair : mask.mask_){
        os << "(" << pair.first << ", " << pair.second << ") ";
    }
    os << "]";
    return os;
}

/* ObjectMask */
ObjectMask::ObjectMask(){

}
ObjectMask::ObjectMask(int start, int end, double d)
    : BaseMask{start, end},disp_{d}{

}
ObjectMask::ObjectMask(int range_len, double d)
    : BaseMask{range_len},disp_{d}{

}
ObjectMask::~ObjectMask(){

}
double ObjectMask::disp() const{
    return disp_;
}
void ObjectMask::disp(double d){
    disp_ = d;
}
void ObjectMask::clear(){
    mask_.clear();
    disp_ = 0;
}

/* StixelMask */
StixelMask::StixelMask(int start, int end)
    : sky_{start, end}, gnd_{start, end},
      all_masks_{start, end}, start_{start}, end_{end}{

}
StixelMask::StixelMask(int range_len)
    : sky_{range_len}, gnd_{range_len},
      all_masks_{range_len}, start_{0}, end_{range_len-1}{

}
StixelMask::~StixelMask(){

}
void StixelMask::MarkSky(int left, int right){
    sky_.Mark(left, right);
    all_masks_.Mark(left, right);
}
void StixelMask::MarkGnd(int left, int right){
    gnd_.Mark(left, right);
    all_masks_.Mark(left, right);
}
void StixelMask::MarkWall(double disp, int left, int right){
    if(wall_.find(disp) == wall_.end()){
        wall_[disp].start(start_);
        wall_[disp].end(end_);
        wall_[disp].disp(disp);
    }
    wall_[disp].Mark(left, right);
    all_masks_.Mark(left, right);
}
void StixelMask::MarkObj(double disp, int left, int right){
    if(obj_.find(disp) == obj_.end()){
        obj_[disp].start(start_);
        obj_[disp].end(end_);
        obj_[disp].disp(disp);
    }
    obj_[disp].Mark(left, right);
    all_masks_.Mark(left, right);
}
void StixelMask::UnMarkSky(int left, int right){
    sky_.UnMark(left, right);
    RefreshAllMasks();
}
void StixelMask::UnMarkGnd(int left, int right){
    gnd_.UnMark(left, right);
    RefreshAllMasks();
}
void StixelMask::UnMarkWall(double disp, int left, int right){
    if(wall_.find(disp) != wall_.end()){
        wall_[disp].UnMark(left, right);
        if(wall_[disp].size() == 0){
            wall_.erase(disp);
        }
        RefreshAllMasks();
    }else{
        dwwarn("no such disparity value");
    }
}
void StixelMask::UnMarkObj(double disp, int left, int right){
    if(obj_.find(disp) != obj_.end()){
        obj_[disp].UnMark(left, right);
        if(obj_[disp].size() == 0){
            obj_.erase(disp);
        }
        RefreshAllMasks();
    }else{
        dwwarn("no such disparity value");
    }
}
void StixelMask::ChangeWallDisp(double old_disp, double new_disp){
    if(wall_.find(old_disp) != wall_.end()){
        auto node = wall_.extract(old_disp);
        node.key() = new_disp;
        wall_.insert(std::move(node));
    }else{
        dwwarn("no such disparity value");
    }
}
void StixelMask::ChangeObjDisp(double old_disp, double new_disp){
    if(obj_.find(old_disp) != obj_.end()){
        auto node = obj_.extract(old_disp);
        node.key() = new_disp;
        obj_.insert(std::move(node));
    }else{
        dwwarn("no such disparity value");
    }
}
void StixelMask::RefreshAllMasks(){
    all_masks_ = sky_ + gnd_;
    for(auto &[disp, mask] : wall_){
        all_masks_ += mask;
    }
    for(auto &[disp, mask] : obj_){
        all_masks_ += mask;
    }
}
std::vector<double> StixelMask::ListWall(){
    std::vector<double> result;
    for(const auto& [disp, mask] : wall_){
        result.push_back(disp);
    }
    return result;
}
std::vector<double> StixelMask::ListObj(){
    std::vector<double> result;
    for(const auto& [disp, mask] : obj_){
        result.push_back(disp);
    }
    return result;
}
int StixelMask::FindWall(double disp){
    auto result = wall_.find(disp);
    if(result != wall_.end()){
        return static_cast<int>(std::distance(wall_.begin(), result));
    }else{
        return -1;
    }
}
int StixelMask::FindObj(double disp){
    auto result = obj_.find(disp);
    if(result != obj_.end()){
        return static_cast<int>(std::distance(obj_.begin(), result));
    }else{
        return -1;
    }
}
void StixelMask::SpawnSuperPillars(std::vector<SuperPillar> &nest,
        int u0, int v0, double baseline, double focus, int width, int height,
        Point3D camera_pos, EulerAngle angle){
    SuperPillar sp;
    // TODO: set ends which are beyond the image scope
    /* for vertical object */
    for(auto& [disp, mask] : wall_){
        sp.clear();
        /* spawn superpillar */
        ImgToWorldCoor(sp, disp, u_, mask.mask_, u0, v0, baseline,
            focus, static_cast<double>(width), camera_pos, angle);
        // NOTE: this bool check might be redundant
        /* spawn complement */
        if(!sp.z_.empty()){
            double temp_bottom = sp.z_.front().first;
            sp.PushComplement(temp_bottom, temp_bottom);
            auto n_zpair = sp.z_.size();
            for(auto i=0; i<n_zpair-1; i++){
                sp.PushComplement(sp.z_[i].second, sp.z_[i+1].first);
            }
            double temp_top = sp.z_.back().second;
            sp.PushComplement(temp_top, temp_top);
            // TEMP: temporary parameter, 5 pixels as tolarance
            const int tolarance = 15;
            sp.SetVirtualEnds(temp_top, temp_bottom);
            if(mask.mask_.front().first<= tolarance){
                sp.is_bottom_beyond(true);
            }
            if(height - mask.mask_.back().second <= tolarance){
                sp.is_top_beyond(true);
            }
        }
        nest.push_back(sp);
    }
    // NOTE: StixelComponent::RefineObjByD() has turn object mask into wall mask
    //       so there's no need to do it again here, but that's not a good strategy
}
void StixelMask::SpawnPointCloud(std::vector<Point3D> &dst, int u0, int v0, double baseline,
    double focus, int width, int height, Point3D camera_pos, EulerAngle angle,
    std::vector<std::vector<double>> &disp_frame, int stixel_width){
    // if(!wall_.empty()){
        for(auto& [disp, mask]: wall_){
            // if(!mask.mask_.empty()){
                for(auto &pair : mask.mask_){
                    for(auto v=pair.first; v<=pair.second; v++){
                        auto i_u = (u_-stixel_width/2)/stixel_width;
                        auto &disp = disp_frame[i_u][v];
                        auto p = ImgToWorldCoor(disp, u_, v, u0, v0, baseline, focus, width, camera_pos, angle);
                        dst.push_back(p);
                    }
                }
            // }
        }
    // }
}
int StixelMask::start(){
    return start_;
}
int StixelMask::end(){
    return end_;
}
void StixelMask::u(int u){
	u_ = u;
}
int StixelMask::u(){
    return u_;
}
std::ostream& operator<<(std::ostream &os, const StixelMask &masks){
    os << "TYPE\tDISP\tMASK\n";
    os << "sky\t" << "-\t" << masks.sky_ << "\n";
    os << "gnd\t" << "-\t"<< masks.gnd_ << "\n";
    os << "wall";
    if(!masks.wall_.empty()){
        for(auto &it : masks.wall_){
            os << "\t" << it.second.disp()
                << "\t" << it.second << "\n";
        }
    }else{
        os << "\t?\t[ ]\n";
    }
    os << "obj";
    if(!masks.obj_.empty()){
        for(auto &it : masks.obj_){
            os << "\t" << it.second.disp()
                << "\t" << it.second << "\n";
        }
    }else{
        os << "\t?\t[ ]\n";
    }
    os << "all\t" << "-\t" << masks.all_masks_ << "\n";
    return os;
}
int StixelMask::it_i(){
    return it_i_;
}
int StixelMask::it_m(){
    return it_m_;
}
int StixelMask::ItInit(){
    if(!all_masks_.empty()){
        if(all_masks_[0].first == 0){
            it_i_ = all_masks_[0].second + 1;
            it_m_ = all_masks_.size() > 1 ? 1 : 0;
        }else{
            it_i_ = 0;
            it_m_ = 0;
        }
    }else{
        it_i_ = all_masks_.start();
    }
    return it_i_;
}
bool StixelMask::ItIsIn(){
    return it_i_ <= end_;
}
bool StixelMask::ItIsIn(int i){
	return i <= end_;
}
int StixelMask::ItInc(){
    if(!all_masks_.empty()){
        if(it_i_+1 < all_masks_.mask_.front().first
            || it_i_+1 > all_masks_.mask_.back().second){
            it_i_++;
        }else{
            auto n_mask = all_masks_.mask_.size();
            for(auto i=0; i<n_mask; i++){
                if(it_i_+1 >= all_masks_.mask_[i].first
                    && it_i_+1 <= all_masks_.mask_[i].second){
                    it_i_ = all_masks_.mask_[i].second + 1;
                    return it_i_;
                }else if(i+1 < n_mask){
                    if(it_i_+1 > all_masks_.mask_[i].second
                        && it_i_+1 < all_masks_.mask_[i+1].first){
                        it_i_++;
                        return it_i_;
                    }
                }
            }
        }
    }else{
        it_i_++;
    }
    return it_i_;
}
int StixelMask::ItInc(int step){
    for(int i=0; i<step; i++){
        ItInc();
        if(it_i_ > end_){
            return it_i_;
        }
    }
    return it_i_;
}
int StixelMask::ItJump(){
    if(!all_masks_.mask_.empty()){
        if(it_i_ < all_masks_.mask_.front().first){
            it_i_ = all_masks_.mask_.front().second + 1;
        }else if(it_i_ > all_masks_.mask_.back().second){
            it_i_ = end_ + 1;
        }else{
            auto n_mask = all_masks_.mask_.size();
            for(auto i=0; i<n_mask; i++){
                if(it_i_ >= all_masks_.mask_[i].first
                    && it_i_ <= all_masks_.mask_[i].second){
                    it_i_ = all_masks_.mask_[i].second + 1;
                    return it_i_;
                }else if(i+1 < n_mask){
                    if(it_i_ > all_masks_.mask_[i].second
                        && it_i_ < all_masks_.mask_[i+1].first){
                        it_i_ = all_masks_.mask_[i+1].second + 1;
                        return it_i_;
                    }
                }
            }
        }
    }else{
        it_i_ = end_ + 1;
    }
    return it_i_;
}
int StixelMask::ItBlockLen(){
    if(!all_masks_.empty()){
        if(it_i_ < all_masks_.mask_.front().first){
            return all_masks_.mask_.front().first - it_i_ - 1;
        }else if(it_i_ > all_masks_.mask_.back().second){
            return end_-it_i_ > 0 ? end_-it_i_ : 0;
        }else{
            auto n_mask = all_masks_.mask_.size();
            for(auto i=0; i<n_mask; i++){
                if(it_i_ >= all_masks_.mask_[i].first
                    && it_i_ <= all_masks_.mask_[i].second){
                    return -1;
                }else if(i+1 < n_mask){
                    if(it_i_ > all_masks_.mask_[i].second
                        && it_i_ < all_masks_.mask_[i+1].first){
                        return all_masks_.mask_[i+1].first - it_i_ - 1;
                    }
                }
            }
            dwerr("this msg should've never appeared");
            assert(false);
            return -1;
        }
    }else{
        return end_-it_i_ > 0 ? end_-it_i_ : 0;
    }
}
const BaseMask& StixelMask::all_mask(){
    return all_masks_;
}
const BaseMask& StixelMask::sky(){
    return sky_;
}
const BaseMask& StixelMask::gnd(){
    return gnd_;
}
const ObjectMask& StixelMask::wall(double disp){
    if(wall_.find(disp) != wall_.end()){
        return wall_[disp];
    }else{
        dwerr("this msg should've never appeared");
        assert(false);
    }
}
const ObjectMask& StixelMask::obj(double disp){
    if(obj_.find(disp) != wall_.end()){
        return obj_[disp];
    }else{
        dwerr("this msg should've never appeared");
        assert(false);
    }
}
void StixelMask::clear(){
    sky_.clear();
    gnd_.clear();
    wall_.clear();
    obj_.clear();
    all_masks_.clear();
    it_i_ = 0;
    it_m_ = 0;
    u_ = -1;
}
// TODO:
bool StixelMask::empty(){
    return wall_.empty() && obj_.empty();
}

} // namespace droneworld