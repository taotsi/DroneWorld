#include "types/super_pillar.h"

namespace droneworld {

std::ostream& operator<<(std::ostream& os, MatchStat stat){
    switch(stat){
        case MatchStat::kNotMatch:{
            os << "kNotMatch";
            break;
        }
        case MatchStat::kMatchPartial:{
            os << "kMatchPartial";
            break;
        }
        case MatchStat::kMatch:{
            os << "kMatch";
            break;
        }
        default:{
            dwtrap("There's NO such a MatchStat");
        }
    }
    return os;
}

Complement::Complement() {

}
Complement::Complement(double top, double bottom){
    if(top < bottom){
        std::swap(top, bottom);
    }
    top_ = top;
    bottom_ = bottom;
}
//Complement::~Complement(){
//
//}
double Complement::height(){
    return top_ - bottom_;
}
double Complement::top() const{
    return top_;
}
void Complement::top(double t){
    top_ = t;
}
double Complement::bottom() const{
    return bottom_;
}
void Complement::bottom(double b){
    bottom_ = b;
}
void Complement::SetEnds(double top, double bottom){
    if(top < bottom){
        std::swap(top, bottom);
    }
    top_ = top;
    bottom_ = bottom;
}
void Complement::match_stat(MatchStat stat){
    match_stat_ = stat;
}
MatchStat Complement::match_stat(){
    return match_stat_;
}
std::ostream& operator<<(std::ostream& os, Complement &comp){
    os << "<" << comp.bottom_ << ", " << comp.top_ << ">";
    return os;
}

SuperPillar::SuperPillar(){

}
double SuperPillar::x() const{
    return x_;
}
void SuperPillar::x(double x){
    x_ = x;
}
double SuperPillar::y() const{
    return y_;
}
void SuperPillar::y(double y){
    y_ = y;
}
const std::vector<std::pair<double, double>>& SuperPillar::z(){
    return z_;
}
const std::vector<Complement>& SuperPillar::complements(){
    return complements_;
}
void SuperPillar::AddZPair(double zh, double zl){
    z_.push_back({zh, zl});
}
void SuperPillar::AddZPair(std::pair<double, double> z){
    z_.push_back(z);
}
void SuperPillar::PushComplement(double top, double bottom){
    complements_.push_back(Complement{top, bottom});
}
void SuperPillar::FillComplement(int index){
    auto &temp = complements_[index];
    if(temp.top() == z_.front().first){
        // dwi2("fill comp, begin");
        z_.front().first = temp.bottom();    // TODO: decide this
    }else if(temp.bottom() == z_.back().second){
        // dwi2("fill comp, end");
        z_.back().second = temp.top();       // TODO: decide this
    }else{
        // dwi2("fill comp, middle");
        auto n_zpair = z_.size();
        for(auto i=0; i<n_zpair-1; i++){
            if(temp.bottom() == z_[i].second){
                if(temp.top() == z_[i+1].first){
                    z_[i].second = z_[i+1].second;
                    z_.erase(z_.begin() + i+1);
                }else{
                    dwtrap("if this occurred, deal with it. don't ever comment this");
                }
                break;
            }
        }
    }
    complements_.erase(complements_.begin() + index);
}
void SuperPillar::NarrowComplement(int index, double top, double bottom){
    if(top < bottom){
        std::swap(top, bottom);
    }
    auto &comp = complements_[index];
    if(top > comp.top() || bottom < comp.bottom()){
        dwtrap("Narrow, don't expand! don't ever comment this");
    }
    // dwi2("comp top, bottom = ", comp.top(), ", ", comp.bottom());
    // dwi2("front 1st, 2nd = ", z_.front().first, ", ", z_.front().second);
    // dwi2("back 1st, 2nd = ", z_.back().first, ", ", z_.back().second);
    if(comp.bottom() == z_.back().second){
        // dwi2("found a top virtual complement");
        comp.SetEnds(top, bottom);
        z_.back().second = comp.bottom();
    }else if(comp.top() == z_.front().first){
        // dwi2("found a bottom virtual complement");
        comp.SetEnds(top, bottom);
        z_.front().first = comp.top();
    }else{
        // dwi2("found a middle virtual complement");
        auto n_zpair = z_.size();
        for(auto i=0; i<n_zpair-1; i++){
            if(comp.bottom() == z_[i].second){
                if(comp.top() == z_[i+1].first){
                    comp.SetEnds(top, bottom);
                    z_[i].second = comp.bottom();
                    z_[i+1].first = comp.top();
                }else{
                    dwtrap("if this occurred, deal with it. don't ever comment this");
                }
                break;
            }
        }
    }
}
void SuperPillar::clear(){
    x_ = 0;
    y_ = 0;
    z_.clear();
    complements_.clear();
}
void SuperPillar::SetVirtualEnds(double top, double bottom){
    if(top < bottom){
        std::swap(top, bottom);
    }
    if(!z_.empty()){
        bottom_ = bottom < z_.front().first ? bottom : z_.front().first;
        complements_.front().SetEnds(bottom_, z_.front().first);
        top_ = top > z_.back().second ? top : z_.back().second;
        complements_.back().SetEnds(z_.back().second, top_);
    }
}
double SuperPillar::top(){
    return complements_.back().top();
}
double SuperPillar::bottom(){
    return complements_.front().bottom() ;
}
double SuperPillar::real_top(){
    return z_.back().second;
}
void SuperPillar::real_top(double top){
    if(top > z_.back().second){
        if(complements_.back().top() > z_.back().second){
            if(complements_.back().top() < top){
                complements_.back().SetEnds(top, top);
            }else{
                complements_.back().bottom(top);
            }
        }
        z_.back().second = top;
    }
}
double SuperPillar::real_bottom(){
    return z_.front().first;
}
void SuperPillar::real_bottom(double bottom){
    if(bottom < z_.front().first){
        if(complements_.front().bottom() < z_.front().first){
            if(complements_.front().bottom() < bottom){
                complements_.front().SetEnds(bottom, bottom);
            }else{
                complements_.front().top(bottom);
            }
        }
        z_.front().first = bottom;
    }
}
bool SuperPillar::empty(){
    return z_.empty();
}
int SuperPillar::NPillar(){
    return static_cast<int>(z_.size());
}
int SuperPillar::NComplement(){
    return static_cast<int>(complements_.size());
}
void SuperPillar::SpawnPillars(Pillars &pls){
    for(auto &z : z_){
        pls.push_back(Pillar{x_, y_, z.first, z.second});
    }
}
void SuperPillar::SetBeyondEnds(bool is_top, bool is_bottom){
    is_top_beyond_ = is_top;
    is_bottom_beyond_ = is_bottom;
}
bool SuperPillar::is_top_beyond(){
    return is_top_beyond_;
}
void SuperPillar::is_top_beyond(bool flag){
    is_top_beyond_ = flag;
}
bool SuperPillar::is_bottom_beyond(){
    return is_bottom_beyond_;
}
void SuperPillar::is_bottom_beyond(bool flag){
    is_bottom_beyond_ = flag;
}
std::ostream& operator<<(std::ostream &os, const SuperPillar &sp){
    os << "(" << sp.x_ << ", " << sp.y_ << ", ";
    for(auto &z : sp.z_){
        os << "(" << z.first << ", " << z.second << ") ";
    }
    for(auto &cp : sp.complements_){
        os << "<" << cp.bottom() << ", " << cp.top() << "> ";
    }
    os << ")";
    return os;
}
} // namespace droneworld