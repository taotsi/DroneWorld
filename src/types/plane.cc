#include "types/plane.h"
#include "droneworld_algorithm.h"

namespace droneworld{

void RefinePlanes(std::vector<Plane> &planes_refined,
	std::vector<Plane> &planes_to_refine) {
	double i_x;
	double i_y;
	if (!planes_to_refine.empty()) {
		for (auto i = 0; i < planes_to_refine.size(); i++) {
			if (!planes_refined.empty()) {
				for (auto j = 0; j < planes_refined.size(); j++) {
					auto stat_temp = JudgeP2P(planes_to_refine[i], planes_refined[j], i_x, i_y);
					if (stat_temp == kCoplane) {
						// merge the plane and return the state
						if (FitTwoPlanes(planes_to_refine[i], planes_refined[j]) == kCoplaneMerge) {
							// if merge two plane , delate plane2 outside the function
							//std::cout << "have Merged planes" << std::endl;
							planes_refined.erase(planes_refined.begin() + j);
						}
						else {
							//std::cout << "have fitted planes" << std::endl;
						}
					}
				}
				for (auto j = 0; j < planes_refined.size(); j++) {
					auto stat_temp = JudgeP2P(planes_to_refine[i], planes_refined[j], i_x, i_y);
					if (stat_temp == kIntersection) {
						RefineIntersectionPlanes(planes_to_refine[i], planes_refined[j], i_x, i_y);
						//std::cout << "have Refine Intersection planes" << std::endl;
					}
				}
				
				for (auto j = 0; j < planes_refined.size(); j++) {
					auto stat_temp = JudgeP2P(planes_to_refine[i], planes_refined[j], i_x, i_y);
					if (stat_temp == kAdjacent) {
						if (RefineAdjacentPlanes(planes_to_refine[i], planes_refined[j]) == kInsert) {
							Plane addplane;
							InsertAdjacentPlanes(planes_to_refine[i], planes_refined[j], addplane);
							//addplane.Print();
							//planes_refined.insert(planes_refined.begin() + j, addplane);
							//std::cout << "need kInsert planes" << std::endl;
						}
						else {
							//std::cout << "have Refine kAdjacent planes" << std::endl;
						}
					}
				}
				
				planes_refined.push_back(planes_to_refine[i]);
			}
			else {
				planes_refined.push_back(planes_to_refine[i]);
			}
		}
	}
}

void RefinePlanesTwins(std::vector<Plane> &planes_refined,
	std::vector<Plane> &planes_to_refine) {
	if (!planes_to_refine.empty()) {
		for (auto i = 0; i < planes_to_refine.size(); i++) {
			if (!planes_refined.empty()) {
				for (auto j = 0; j < planes_refined.size(); j++) {
					auto state_temp = RefineTwoPlane(planes_to_refine[i], planes_refined[j], kCoplane);
					if (state_temp == kCoplaneMerge) {
						planes_refined.erase(planes_refined.begin() + j);
					}
				}
				for (auto j = 0; j < planes_refined.size(); j++) {
					RefineTwoPlane(planes_refined[i], planes_refined[j], kIntersection);
				}
				for (auto j = 0; j < planes_refined.size(); j++) {
					if (RefineTwoPlane(planes_refined[i], planes_refined[j], kAdjacent) == kInsert){
						//Plane addplane;
						//InsertAdjacentPlanes(planes_to_refine[i], planes_refined[j], addplane);
						//planes_refined.insert(planes_refined.begin() + j, addplane);
						//std::cout << "need kInsert planes" << std::endl;
					}else {
						//std::cout << "have Refine kAdjacent planes" << std::endl;
					}
				}
				planes_refined.push_back(planes_to_refine[i]);
			}
			else {
				planes_refined.push_back(planes_to_refine[i]);
			}
		}
	}
}

// refine the planes from the ICRA2011 algorithm (grid map)
void RefineGridPlane(std::vector<Plane> &planes_refined) {
	if (!planes_refined.empty()) {
		for (auto i = 0; i < planes_refined.size(); i++) {
			for (auto j = 0; j < planes_refined.size(); j++) {
				if (i != j) {
					if (IsConnectedCoplane(planes_refined[i], planes_refined[j]) == true) {
						MergeNewPlane(planes_refined[i], planes_refined[j]);
						planes_refined.erase(planes_refined.begin() + j);
						i = 0;
						break;
					}
				}
			}
		}
	}
}


// refine two plane if they are intersected or adjacent
PlanetoPlaneStatus RefineTwoPlane(Plane &plane1, Plane &plane2, double command) {
	// intersection point of two planes
	double i_x;
	double i_y;
	auto stat_temp = JudgeP2P(plane1, plane2, i_x, i_y);
	if (stat_temp == kUnrelated) {
		return kUnrelated;
	}
	else if (stat_temp == kConnected) {
		return kConnected;
	}
	else if (stat_temp == kCoplane) {
		if (command == kCoplane) {
			// merge the plane and return the state
			if (FitTwoPlanes(plane1, plane2) == kCoplaneMerge) {
				// if merge two plane , delate plane2 outside the function
				//std::cout << "have Merged planes" << std::endl;
				return kCoplaneMerge;
			}
			else {
				//std::cout << "have fitted planes" << std::endl;
				return kCoplane;
			}
		}
	}
	else if (stat_temp == kIntersection) {
		if (command == kIntersection) {
			RefineIntersectionPlanes(plane1, plane2, i_x, i_y);
			//std::cout << "have Refine Intersection planes" << std::endl;
		}
	}
	else if (stat_temp == kAdjacent) {
		if (command == kAdjacent) {
			if (RefineAdjacentPlanes(plane1, plane2) == kInsert) {
				//std::cout << "need kInsert planes" << std::endl;
				return kInsert;
			}
			else {
				//std::cout << "have Refine kAdjacent planes" << std::endl;
				return kAdjacent;
			}
		}
	}
	else {
		//std::cout << "There is an error in planes refine program" << std::endl;
		return ERROR_plane;
	}
}

bool IsConnectedCoplane(Plane &plane1, Plane &plane2) {
	auto plane1_z_min = plane1.p1_.z_;
	auto plane1_z_max = plane1.p2_.z_;

	auto plane2_z_min = plane2.p1_.z_;
	auto plane2_z_max = plane2.p2_.z_;

	std::vector<double> distance_P2P;
	double min_distance_P2P;
	double angle_between_planes = 0;

	// judge the relationship in z axis

	if (((plane1_z_min <= plane2_z_max) && (plane1_z_min >= plane2_z_min))
		|| ((plane1_z_max <= plane2_z_max) && (plane1_z_max >= plane2_z_min))
		|| ((plane2_z_min <= plane1_z_max) && (plane2_z_min >= plane1_z_min))
		|| ((plane2_z_max <= plane1_z_max) && (plane2_z_max >= plane1_z_min))) {
		// whether the two plane  intersect with each other
		if ((abs(plane1.p1_.x_ - plane2.p1_.x_) < 0.01 && abs(plane1.p1_.y_ - plane2.p1_.y_)< 0.01)  ||
			(abs(plane1.p1_.x_ - plane2.p2_.x_) < 0.01 && abs(plane1.p1_.y_ - plane2.p2_.y_)< 0.01)  ||
			(abs(plane1.p2_.x_ - plane2.p1_.x_) < 0.01 && abs(plane1.p2_.y_ - plane2.p1_.y_)< 0.01)  ||
			(abs(plane1.p2_.x_ - plane2.p2_.x_) < 0.01 && abs(plane1.p2_.y_ - plane2.p2_.y_)< 0.01))  {
			angle_between_planes = AngleBetweenSmallPlanes(plane1, plane2);
			//std::cout << "angle = " << angle_between_planes << std::endl;
			if (abs(angle_between_planes) < 0.01) {
				return true;
			}else{
				return false;
			}
		}else {
			return false;
		}
	}
}

// detection the relationship between the two plane
PlanetoPlaneStatus JudgeP2P(Plane &plane1, Plane &plane2, double &i_x, double &i_y) {
	auto plane1_z_min = plane1.p1_.z_;
	auto plane1_z_max = plane1.p2_.z_;

	auto plane2_z_min = plane2.p1_.z_;
	auto plane2_z_max = plane2.p2_.z_;

	std::vector<double> distance_P2P;
	double min_distance_P2P;
	double angle_between_planes = 0;

	// judge the relationship in z axis

	if (((plane1_z_min <= plane2_z_max) && (plane1_z_min >= plane2_z_min))
		|| ((plane1_z_max <= plane2_z_max) && (plane1_z_max >= plane2_z_min))
		|| ((plane2_z_min <= plane1_z_max) && (plane2_z_min >= plane1_z_min))
		|| ((plane2_z_max <= plane1_z_max) && (plane2_z_max >= plane1_z_min))) {
		// whether the two plane  intersect with each other
		if (((plane1.p1_.x_ == plane2.p1_.x_) && (plane1.p1_.y_ == plane2.p1_.y_)) ||
			((plane1.p1_.x_ == plane2.p2_.x_) && (plane1.p1_.y_ == plane2.p2_.y_)) ||
			((plane1.p2_.x_ == plane2.p1_.x_) && (plane1.p2_.y_ == plane2.p1_.y_)) ||
			((plane1.p2_.x_ == plane2.p2_.x_) && (plane1.p2_.y_ == plane2.p2_.y_))) {
			angle_between_planes = AngleBetweenPlanes(plane1, plane2);
			if (angle_between_planes < 5) {
				//std::cout << "kConnected kCoplane planes" << std::endl;
				return kCoplane;
			}
			else {
				//std::cout << "kConnected planes" << std::endl;
				return kConnected;
			}

		}
		else if (kIntersection == GetLineIntersection(plane1, plane2, i_x, i_y)) {
			angle_between_planes = AngleBetweenPlanes(plane1, plane2);
			if (angle_between_planes < 5) {
				//std::cout << "kCoplane planes" << std::endl;
				return kCoplane;
			}
			else {
				//std::cout << "kIntersection planes" << std::endl;
				return kIntersection;
			}
		}
		else {
			distance_P2P.push_back(PointToLine(plane1.p1_, plane2));
			distance_P2P.push_back(PointToLine(plane1.p2_, plane2));
			distance_P2P.push_back(PointToLine(plane2.p1_, plane1));
			distance_P2P.push_back(PointToLine(plane2.p2_, plane1));
			min_distance_P2P = *min_element(distance_P2P.begin(), distance_P2P.end());
			//std::cout << "min_distance_P2P = " << min_distance_P2P << std::endl;
			if (min_distance_P2P < ((double)SAFE_WIDTH/2)) {
				angle_between_planes = AngleBetweenPlanes(plane1, plane2);
				// distance less than 0.5m and angle less than 5
				if ((angle_between_planes < THRESHOLD_ANGLE_COPLANE)) {
					//std::cout << "kCoplane planes" << std::endl;
					return kCoplane;
				}
				else {
					//std::cout << "kAdjacent planes" << std::endl;
					return kAdjacent;
				}
			}
			else {
				//std::cout << "kUnrelated planes beyond the distace in xy direction" << std::endl;
				return kUnrelated;
			}
		}
	}
	else {
		return kUnrelated;
	}

}

// caculate the distance from point to plane
double PointToLine(Point3D &point, Plane  &testplane) {
	// the point p in xy plane to the line AB    ==>   xy direction
	double point_rect_xy = 0;
	double AB_x = testplane.p2_.x_ - testplane.p1_.x_;
	double AB_y = testplane.p2_.y_ - testplane.p1_.y_;
	double AP_x = point.x_ - testplane.p1_.x_;
	double AP_y = point.y_ - testplane.p1_.y_;
	double AB_d = (AB_x * AB_x + AB_y * AB_y);
	double t = AB_x * AP_x + AB_y * AP_y;
	if (AB_d > 0)
	{
		t = t / AB_d;
	}
	else
	{
		// the width of plane is zero, return false
		return false;
	}
	if (t < 0)
		t = 0;
	else if (t > 1)
	{
		t = 1;
	}
	double point2plane_x = testplane.p1_.x_ + t * AB_x - point.x_;
	double point2plane_y = testplane.p1_.y_ + t * AB_y - point.y_;
	point_rect_xy = std::sqrt(point2plane_x * point2plane_x + point2plane_y * point2plane_y);
	return point_rect_xy;
}

// caculate the distance from point to plane
double PointToPlane(Point3D &point, Plane  &testplane) {
	// caculate the distance at xy direction and z direction
	// the point p in xy plane to the line AB    ==>   xy direction
	double point_rect_xy = 0;
	double point_rect_z = 0;
	double AB_x = testplane.p2_.x_ - testplane.p1_.x_;
	double AB_y = testplane.p2_.y_ - testplane.p1_.y_;
	double AP_x = point.x_ - testplane.p1_.x_;
	double AP_y = point.y_ - testplane.p1_.y_;
	double AB_d = (AB_x * AB_x + AB_y * AB_y);
	double t = AB_x * AP_x + AB_y * AP_y;
	if (AB_d > 0)
	{
		t = t / AB_d;
	}
	else
	{
		// the width of plane is zero, return false
		return false;
	}
	if (t < 0)
		t = 0;
	else if (t > 1)
	{
		t = 1;
	}

	double point2plane_x = testplane.p1_.x_ + t * AB_x - point.x_;
	double point2plane_y = testplane.p1_.y_ + t * AB_y - point.y_;
	point_rect_xy = sqrt(point2plane_x * point2plane_x + point2plane_y * point2plane_y);
	// the two diagonal piont of rectangle
	// testplane.p1_;
	// testplane.p2_;
	// caculate the distance oin the z axis
	if ((point.z_ < testplane.p2_.z_) && (point.z_ > testplane.p1_.z_))
	{
		point_rect_z = 0;
		return point_rect_xy;
	}
	else
	{
		point_rect_z = std::min(abs(point.z_ - testplane.p2_.z_), abs(point.z_ - testplane.p1_.z_));
		return sqrt(point_rect_z * point_rect_z + point_rect_xy * point_rect_xy);
	}

}

// judge the two line intersection
double GetLineIntersection(Plane plane1, Plane plane2, double &i_x, double &i_y) {
	double p0_x = plane1.p1_.x_;
	double p0_y = plane1.p1_.y_;
	double p1_x = plane1.p2_.x_;
	double p1_y = plane1.p2_.y_;

	double p2_x = plane2.p1_.x_;
	double p2_y = plane2.p1_.y_;
	double p3_x = plane2.p2_.x_;
	double p3_y = plane2.p2_.y_;

	double s02_x, s02_y, s10_x, s10_y, s32_x, s32_y, s_numer, t_numer, denom, t;
	s10_x = p1_x - p0_x;
	s10_y = p1_y - p0_y;
	s32_x = p3_x - p2_x;
	s32_y = p3_y - p2_y;

	denom = s10_x * s32_y - s32_x * s10_y;
	if (denom == 0)
		return kUnIntersection; // Collinear
	bool denomPositive = denom > 0;

	s02_x = p0_x - p2_x;
	s02_y = p0_y - p2_y;
	s_numer = s10_x * s02_y - s10_y * s02_x;
	if ((s_numer < 0) == denomPositive)
		return kUnIntersection; // No collision

	t_numer = s32_x * s02_y - s32_y * s02_x;
	if ((t_numer < 0) == denomPositive)
		return kUnIntersection; // No collision

	if (fabs(s_numer) > fabs(denom) || fabs(t_numer) > fabs(denom))
		return kUnIntersection; // No collision
	// Collision detected
	t = t_numer / denom;
	i_x = p0_x + (t * s10_x);
	i_y = p0_y + (t * s10_y);

	return kIntersection;
}

double AngleBetweenSmallPlanes(Plane &plane1, Plane &plane2) {
	double theta = atan2(plane1.p1_.x_ - plane1.p2_.x_, plane1.p1_.y_ - plane1.p2_.y_) -
		atan2(plane2.p1_.x_ - plane2.p2_.x_, plane2.p1_.y_ - plane2.p2_.y_);
	//double CV_PI = 3.1415926;
	if (theta > CV_PI)
		theta -= 2 * CV_PI;
	if (theta < -CV_PI)
		theta += 2 * CV_PI;

	theta = theta * 180.0 / CV_PI;
	return theta;
}

// caculate the angle between planes
double AngleBetweenPlanes(Plane &plane1, Plane &plane2) {
	// caculate the angle and distance between two planes
	double p0_x = plane1.p1_.x_;
	double p0_y = plane1.p1_.y_;
	double p1_x = plane1.p2_.x_;
	double p1_y = plane1.p2_.y_;

	double p2_x = plane2.p1_.x_;
	double p2_y = plane2.p1_.y_;
	double p3_x = plane2.p2_.x_;
	double p3_y = plane2.p2_.y_;
	double s10_x, s10_y, s32_x, s32_y, d_10, d_32;
	double angle = 0;
	s10_x = p1_x - p0_x;
	s10_y = p1_y - p0_y;
	s32_x = p3_x - p2_x;
	s32_y = p3_y - p2_y;
	d_10 = std::sqrt(s10_x * s10_x + s10_y * s10_y);
	d_32 = std::sqrt(s32_x * s32_x + s32_y * s32_y);
	angle = (std::acos(std::abs(s10_x * s32_x - s10_y * s32_y) / (d_10 * d_32))) * 57.29578;
	if (angle > 90) {
		angle = 180 - angle;
	}
	return angle;
}

//  fit the two planes
double FitTwoPlanes(Plane &plane1, Plane &plane2) {
	auto n1 = plane1.line_.n();
	auto x_var1 = plane1.line_.x_avr();
	auto y_var1 = plane1.line_.y_avr();
	auto xx_var1 = plane1.line_.xx_avr();
	auto xy_var1 = plane1.line_.xy_avr();

	auto n2 = plane2.line_.n();
	auto x_var2 = plane2.line_.x_avr();
	auto y_var2 = plane2.line_.y_avr();
	auto xx_var2 = plane2.line_.xx_avr();
	auto xy_var2 = plane2.line_.xy_avr();

	auto n_new = n1 + n2;
	auto x_var_new = (x_var1 * n1 + x_var2 * n2) / n_new;
	auto y_var_new = (y_var1 * n1 + y_var2 * n2) / n_new;
	auto xy_var_new = (xy_var1 * n1 + xy_var2 * n2) / n_new;
	auto xx_var_new = (xx_var1 * n1 + xx_var2 * n2) / n_new;

	auto plane1_z_min = plane1.p1_.z_;
	auto plane1_z_max = plane1.p2_.z_;
	auto plane2_z_min = plane2.p1_.z_;
	auto plane2_z_max = plane2.p2_.z_;

	double p0_x = plane1.p1_.x_;
	double p0_y = plane1.p1_.y_;
	double p1_x = plane1.p2_.x_;
	double p1_y = plane1.p2_.y_;

	double p2_x = plane2.p1_.x_;
	double p2_y = plane2.p1_.y_;
	double p3_x = plane2.p2_.x_;
	double p3_y = plane2.p2_.y_;

	double s10_x, s10_y, s21_x, s21_y, s03_x, s03_y;
	double s12_x, s12_y, s32_x, s32_y;
	s10_x = p1_x - p0_x;
	s10_y = p1_y - p0_y;
	s32_x = p3_x - p2_x;
	s32_y = p3_y - p2_y;
	s21_x = p2_x - p1_x;
	s21_y = p2_y - p1_y;
	s03_x = p0_x - p3_x;
	s03_y = p0_y - p3_y;
	double distance_p1p2, distance_p0p3;
	double distance_p0p1, distance_p2p3;
	double overlap_area;
	distance_p1p2 = std::sqrt(abs(s21_x)*abs(s21_x) + abs(s21_y)*abs(s21_y));
	distance_p0p3 = std::sqrt(abs(s03_x)*abs(s03_x) + abs(s03_y)*abs(s03_y));
	distance_p0p1 = std::sqrt(abs(s10_x)*abs(s10_x) + abs(s10_y)*abs(s10_y));
	distance_p2p3 = std::sqrt(abs(s32_x)*abs(s32_x) + abs(s32_y)*abs(s32_y));

	if (distance_p1p2 <= distance_p0p3) {
		s12_x = p1_x - p2_x;
		s12_y = p1_y - p2_y;
		overlap_area = (s12_x * s10_x + s12_y * s10_y) / distance_p0p1;
	}
	else {
		s03_x = p0_x - p3_x;
		s03_y = p0_y - p3_y;
		overlap_area = (s03_x * -s10_x + s03_y * -s10_y) / distance_p0p1;
	}

	// caculate the relationship in z axis
	if ((std::abs(plane1_z_min - plane2_z_min) < Thresh_difference_planes)
		&& (std::abs(plane1_z_max - plane2_z_max) < Thresh_difference_planes)) {
		plane1.line_.update(n_new, x_var_new, y_var_new, xy_var_new, xx_var_new);
		MergeNewPlane(plane1, plane2);
		return kCoplaneMerge;

	}
	else if (overlap_area > 1) {
		//std::cout << "overlap_area more than 1" << std::endl;
		// partly overlap
		Point3D p1_new;
		Point3D p2_new;
		plane1.line_.update(n_new, x_var_new, y_var_new, xy_var_new, xx_var_new);
		MergeNewPlane(plane1, plane2);
		p1_new.z_ = std::min(plane1.p1_.z_, plane2.p1_.z_);
		p2_new.z_ = std::max(plane1.p2_.z_, plane2.p2_.z_);
		plane1.FromPoints(p1_new, p2_new);
		return kCoplaneMerge;

	}
	else {
		// one plane is small than another and does not overlap
		plane1.line_.update(n_new, x_var_new, y_var_new, xy_var_new, xx_var_new);
		plane2.line_.update(n_new, x_var_new, y_var_new, xy_var_new, xx_var_new);
		//sstd::cout << "enlarge one plane" << std::endl;
		if ((plane1_z_max - plane1_z_min) > (plane2_z_max - plane2_z_min)) {
			// plane1 is larger than plane2, change plane2
			EnlargePlane(plane1, plane2);
		}
		else {
			EnlargePlane(plane2, plane1);
		}
		return kCoplane;
	}
}

// caculate the points p1_ and p2_ belong to new plane
void MergeNewPlane(Plane &plane1, Plane &plane2) {

	Point3D p1_new;
	Point3D p2_new;

	p1_new.x_ = std::min(plane1.p1_.x_, plane2.p1_.x_);
	p1_new.x_ = std::min(p1_new.x_, plane2.p2_.x_);
	p1_new.x_ = std::min(p1_new.x_, plane1.p2_.x_);
	p2_new.x_ = std::max(plane1.p1_.x_, plane2.p1_.x_);
	p2_new.x_ = std::max(p2_new.x_, plane2.p2_.x_);
	p2_new.x_ = std::max(p2_new.x_, plane1.p2_.x_);

	p1_new.y_ = std::min(plane1.p1_.y_, plane2.p1_.y_);
	p1_new.y_ = std::min(p1_new.y_, plane2.p2_.y_);
	p1_new.y_ = std::min(p1_new.y_, plane1.p2_.y_);
	p2_new.y_ = std::max(plane1.p1_.y_, plane2.p1_.y_);
	p2_new.y_ = std::max(p2_new.y_, plane2.p2_.y_);
	p2_new.y_ = std::max(p2_new.y_, plane1.p2_.y_);

	double distance_p1p2_x = std::abs(p1_new.x_ - p2_new.x_);
	double distance_p1p2_y = std::abs(p1_new.y_ - p2_new.y_);

	if (plane1.p1_.x_ >= plane1.p2_.x_) {
		ChangeValue(p1_new.x_, p2_new.x_);
	}
	if (plane1.p1_.y_ >= plane1.p2_.y_) {
		ChangeValue(p1_new.y_, p2_new.y_);
	}
	// MUST JUDGE because if the slpoe is too large, the EstimateY is not accuracy
	if (abs(plane1.line_.slope()) > 10000) {
		p1_new.x_ = (p1_new.x_ + p2_new.x_) / 2;
		p2_new.x_ = p1_new.x_;
	}else if (distance_p1p2_x >= distance_p1p2_y) {
		p1_new.y_ = plane1.line_.EstimateY(p1_new.x_, p1_new.y_);
		p2_new.y_ = plane1.line_.EstimateY(p2_new.x_, p2_new.y_);
	}
	else {
		p1_new.x_ = plane1.line_.EstimateX(p1_new.x_, p1_new.y_);
		p2_new.x_ = plane1.line_.EstimateX(p2_new.x_, p2_new.y_);
	}
	p1_new.z_ = std::min(plane1.p1_.z_, plane2.p1_.z_);
	p2_new.z_ = std::max(plane1.p2_.z_, plane2.p2_.z_);
	// TODO::
	//p1_new.z_ = AverageValue(plane1.p1_.z_, plane2.p1_.z_);
	//p2_new.z_ = AverageValue(plane1.p2_.z_, plane2.p2_.z_);

	plane1.FromPoints(p1_new, p2_new);
}

// enlarge the smaller plane , plane2 is the smaller one
void EnlargePlane(Plane &plane1, Plane &plane2) {
	double distance_p1_P1 = PointToLine(plane2.p1_, plane1);
	double distance_p2_P1 = PointToLine(plane2.p2_, plane1);
	Point3D plane1_p1_new(plane1.p1_);
	Point3D plane1_p2_new(plane1.p2_);
	double distance_p1p2_x = std::abs(plane1_p1_new.x_ - plane1_p2_new.x_);
	double distance_p1p2_y = std::abs(plane1_p1_new.y_ - plane1_p2_new.y_);
	if (distance_p1p2_x >= distance_p1p2_y) {
		plane1_p1_new.y_ = plane1.line_.EstimateY(plane1_p1_new.x_, plane1_p1_new.y_);
		plane1_p2_new.y_ = plane1.line_.EstimateY(plane1_p2_new.x_, plane1_p2_new.y_);
	}
	else {
		plane1_p1_new.x_ = plane1.line_.EstimateX(plane1_p1_new.x_, plane1_p1_new.y_);
		plane1_p2_new.x_ = plane1.line_.EstimateX(plane1_p2_new.x_, plane1_p2_new.y_);
	}
	plane1_p1_new.y_ = plane1.line_.EstimateY(plane1_p1_new.x_, plane1_p1_new.y_);
	plane1_p2_new.y_ = plane1.line_.EstimateY(plane1_p2_new.x_, plane1_p2_new.y_);
	Point3D plane2_p1_new(plane2.p1_);
	Point3D plane2_p2_new(plane2.p2_);
	double distance_plane2_p1_p1 = DistancePointToPoint(plane2_p1_new, plane1.p1_);
	double distance_plane2_p1_p2 = DistancePointToPoint(plane2_p1_new, plane1.p2_);
	double distance_plane2_p2_p1 = DistancePointToPoint(plane2_p2_new, plane1.p1_);
	double distance_plane2_p2_p2 = DistancePointToPoint(plane2_p2_new, plane1.p2_);
	// chose the point in plane2 which is closest to plane1
	if (distance_p1_P1 < distance_p2_P1) {
		if (distance_plane2_p1_p1 < distance_plane2_p1_p2) {
			plane2_p1_new.x_ = plane1_p1_new.x_;
			plane2_p1_new.y_ = plane1_p1_new.y_;
		}
		else {
			plane2_p1_new.x_ = plane1_p2_new.x_;
			plane2_p1_new.y_ = plane1_p2_new.y_;
		}
		if (distance_p1p2_x >= distance_p1p2_y) {
			plane2_p2_new.y_ = plane1.line_.EstimateY(plane2_p2_new.x_, plane2_p2_new.y_);
		}
		else {
			plane2_p2_new.x_ = plane1.line_.EstimateX(plane2_p2_new.x_, plane2_p2_new.y_);
		}
	}
	else {
		if (distance_plane2_p2_p1 < distance_plane2_p2_p2) {
			plane2_p2_new.x_ = plane1_p1_new.x_;
			plane2_p2_new.y_ = plane1_p1_new.y_;
		}
		else {
			plane2_p2_new.x_ = plane1_p2_new.x_;
			plane2_p2_new.y_ = plane1_p2_new.y_;
		}
		if (distance_p1p2_x >= distance_p1p2_y) {
			plane2_p1_new.y_ = plane1.line_.EstimateY(plane2_p1_new.x_, plane2_p1_new.y_);
		}
		else {
			plane2_p1_new.x_ = plane1.line_.EstimateX(plane2_p1_new.x_, plane2_p1_new.y_);
		}
	}

	plane1.FromPoints(plane1_p1_new, plane1_p2_new);
	plane2.FromPoints(plane2_p1_new, plane2_p2_new);
}

// delate redundance of the intersection part of the planes
void RefineIntersectionPlanes(Plane &plane1, Plane &plane2, double &i_x, double &i_y) {
	Point3D intersection_point(i_x, i_y, 0);
	Point3D plane1_p1_new(plane1.p1_);
	Point3D plane1_p2_new(plane1.p2_);
	Point3D plane2_p1_new(plane2.p1_);
	Point3D plane2_p2_new(plane2.p2_);
	double distance_Plane1_p1_i = DistancePointToPoint(intersection_point, plane1.p1_);
	double distance_Plane1_p2_i = DistancePointToPoint(intersection_point, plane1.p2_);
	double distance_Plane2_p1_i = DistancePointToPoint(intersection_point, plane2.p1_);
	double distance_Plane2_p2_i = DistancePointToPoint(intersection_point, plane2.p2_);
	if (distance_Plane1_p1_i <= distance_Plane1_p2_i) {
		plane1_p1_new.x_ = i_x;
		plane1_p1_new.y_ = i_y;
	}
	else {
		plane1_p2_new.x_ = i_x;
		plane1_p2_new.y_ = i_y;
	}
	if (distance_Plane2_p1_i <= distance_Plane2_p2_i) {
		plane2_p1_new.x_ = i_x;
		plane2_p1_new.y_ = i_y;
	}
	else {
		plane2_p2_new.x_ = i_x;
		plane2_p2_new.y_ = i_y;
	}
	plane1.FromPoints(plane1_p1_new, plane1_p2_new);
	plane2.FromPoints(plane2_p1_new, plane2_p2_new);
}

// deal with the Adjacent planes
double RefineAdjacentPlanes(Plane &plane1, Plane &plane2) {
	double p0_x = plane1.p1_.x_;
	double p0_y = plane1.p1_.y_;
	double p1_x = plane1.p2_.x_;
	double p1_y = plane1.p2_.y_;
	double p2_x = plane2.p1_.x_;
	double p2_y = plane2.p1_.y_;
	double p3_x = plane2.p2_.x_;
	double p3_y = plane2.p2_.y_;
	double s02_x, s02_y, s10_x, s10_y, s32_x, s32_y, t_numer, denom, t;
	s10_x = p1_x - p0_x;
	s10_y = p1_y - p0_y;
	s32_x = p3_x - p2_x;
	s32_y = p3_y - p2_y;

	double i_x, i_y;
	//double angle = AngleBetweenPlanes(plane1, plane2);
	denom = s10_x * s32_y - s32_x * s10_y;

	s02_x = p0_x - p2_x;
	s02_y = p0_y - p2_y;
	t_numer = s32_x * s02_y - s32_y * s02_x;
	t = t_numer / denom;

	i_x = p0_x + (t * s10_x);
	i_y = p0_y + (t * s10_y);

	double extend_length1;
	double extend_length2;
	Point3D point_intersect(i_x, i_y, 0);
	Point3D plane1_p1_new(plane1.p1_);
	Point3D plane1_p2_new(plane1.p2_);
	Point3D plane2_p1_new(plane2.p1_);
	Point3D plane2_p2_new(plane2.p2_);
	double distace_P1p1_intersect = DistancePointToPoint(plane1.p1_, point_intersect);
	double distace_P1p2_intersect = DistancePointToPoint(plane1.p2_, point_intersect);
	double distace_P2p1_intersect = DistancePointToPoint(plane2.p1_, point_intersect);
	double distace_P2p2_intersect = DistancePointToPoint(plane2.p2_, point_intersect);
	extend_length1 = std::min(distace_P1p1_intersect, distace_P1p2_intersect);
	extend_length2 = std::min(distace_P2p1_intersect, distace_P2p2_intersect);
	//std::cout << extend_length1 << std::endl;
	//std::cout << extend_length2 << std::endl;
	if ((extend_length1 < width_refine_plane ) && (extend_length2 < width_refine_plane)) {
		if (distace_P1p1_intersect < distace_P1p2_intersect) {
			plane1_p1_new.x_ = i_x;
			plane1_p1_new.y_ = i_y;
		}
		else {
			plane1_p2_new.x_ = i_x;
			plane1_p2_new.y_ = i_y;
		}
		if (distace_P2p1_intersect < distace_P2p2_intersect) {
			plane2_p1_new.x_ = i_x;
			plane2_p1_new.y_ = i_y;
		}
		else {
			plane2_p2_new.x_ = i_x;
			plane2_p2_new.y_ = i_y;
		}
		plane1.FromPoints(plane1_p1_new, plane1_p2_new);
		plane2.FromPoints(plane2_p1_new, plane2_p2_new);
	}else{
		return kInsert; // Collinear
	}

}

void InsertAdjacentPlanes(Plane &plane1, Plane &plane2, Plane &addplane){
	auto plane1_z_min = plane1.p1_.z_;
	auto plane1_z_max = plane1.p2_.z_;
	auto plane2_z_min = plane2.p1_.z_;
	auto plane2_z_max = plane2.p2_.z_;
	double distance_Plane1p1_Plane2 = PointToPlane(plane1.p1_, plane2);
	double distance_Plane1p2_Plane2 = PointToPlane(plane1.p2_, plane2);
	double distance_Plane2p1_Plane1 = PointToPlane(plane2.p1_, plane1);
	double distance_Plane2p2_Plane1 = PointToPlane(plane2.p2_, plane1);
	if (distance_Plane1p1_Plane2 < distance_Plane1p2_Plane2) {
		addplane.p2_ = plane1.p1_;
		if (distance_Plane2p1_Plane1 < distance_Plane2p2_Plane1) {
			addplane.p1_ = plane2.p1_;
		}
		else {
			addplane.p1_ = plane2.p2_;
		}
	}else{
		addplane.p1_ = plane1.p2_;
		if (distance_Plane2p1_Plane1 < distance_Plane2p2_Plane1) {
			addplane.p2_ = plane2.p1_;
		}
		else {
			addplane.p2_ = plane2.p2_;
		}
	}

	if ((plane1_z_max - plane1_z_min) <= (plane2_z_max - plane2_z_min)) {
		addplane.p1_.z_ = plane1.p1_.z_;
		addplane.p2_.z_ = plane1.p2_.z_;
	}else{
		addplane.p1_.z_ = plane2.p1_.z_;
		addplane.p2_.z_ = plane2.p2_.z_;
	}
}

inline double DistancePointToPoint(Point3D &point1, Point3D &point2){
	double p1_x = point1.x_;
	double p1_y = point1.y_;
	double p2_x = point2.x_;
	double p2_y = point2.y_;

	double s12_x, s12_y;
	s12_x = abs(p1_x - p2_x);
	s12_y = abs(p1_y - p2_y);
	double distance_p1_p1 = std::sqrt(s12_x * s12_x + s12_y * s12_y);
	return distance_p1_p1;
}

inline void ChangeValue(double &value1, double &value2){
	double temp;
	temp = value2;
	value2 = value1;
	value1 = temp;
}

inline double AverageValue(double &value1, double &value2){
	double average_value = (value1 + value2) / 2;
	return average_value;
}


} // namespace droneworld