#include "types/line2dfitted.h"

namespace droneworld{

Line2dFitted::Line2dFitted(){

}
Line2dFitted::Line2dFitted(std::vector<Pillar> &pillars){
    int end = static_cast<int>(pillars.size() - 1);
    Reset(pillars, 0, end);
}
Line2dFitted::Line2dFitted(std::vector<Pillar> &pillars, int start, int end){
    Reset(pillars, start, end);
}

Line2dFitted::Line2dFitted(std::vector<double> &vec, int start, int end){
    Reset(vec, start, end);
}
Line2dFitted::Line2dFitted(double x1, double y1, double x2, double y2){
    Reset(x1, y1, x2, y2);
}
Line2dFitted::Line2dFitted(Point3D &p1, Point3D &p2){
    Reset(p1.x_, p1.y_, p2.x_, p2.y_);
}
Line2dFitted::Line2dFitted(Pillar &pl1, Pillar &pl2){
    Reset(pl1.x(), pl1.y(), pl2.x(), pl2.y());
}
Line2dFitted::Line2dFitted(const Line2dFitted &other){
    n_ = other.n_;
    a_ = other.a_;
    b_ = other.b_;
    c_ = other.c_;
    x_avr_ = other.x_avr_;
    y_avr_ = other.y_avr_;
    xx_avr_ = other.xx_avr_;
    xy_avr_ = other.xy_avr_;
}
// Line2dFitted::Line2dFitted(Line2dFitted &&other){
//
// }
Line2dFitted& Line2dFitted::operator=(const Line2dFitted &other){
    if(this != &other){
        n_ = other.n_;
        a_ = other.a_;
        b_ = other.b_;
        c_ = other.c_;
        x_avr_ = other.x_avr_;
        y_avr_ = other.y_avr_;
        xx_avr_ = other.xx_avr_;
        xy_avr_ = other.xy_avr_;
        return *this;
    }else{
        return *this;
    }
}
// Line2dFitted& Line2dFitted::operator=(Line2dFitted &&other){
//
// }
Line2dFitted::~Line2dFitted(){

}
void Line2dFitted::AddPoint(double x, double y){
    n_ += 1.0;
    x_avr_ += (x - x_avr_) / n_;
    y_avr_ += (y - y_avr_) / n_;
    xx_avr_ += (x*x - xx_avr_) / n_;
    xy_avr_ += (x*y - xy_avr_) / n_;
    CalcCoef();
}
// TODO: this implementation looks stupid, update it later
void Line2dFitted::AddPoints(std::vector<Pillar> &pillars, int start, int end){
    for (int i = start; i <= end; i++){
        AddPoint(pillars[i].x(), pillars[i].y());
    }
}
double Line2dFitted::EstimateY(double x, double y){
    if(!IsZero(a_)){
        if(!IsZero(b_)){
            if(!IsZero(b_ / a_)){
                return -a_ / b_ * x - c_ / b_;
            }else{
                return this->y_avr();
            }
        }else{
            return this->y_avr();
        }
    }else{
        return this->y_avr();
    }
}
double Line2dFitted::EstimateX(double x, double y){
	if (!IsZero(b_)) {
		if (!IsZero(a_)) {
			if (!IsZero(a_ / b_)) {
				return -b_ / a_ * y - c_ / a_;
			}
			else {
				return this->x_avr();
			}
		}
		else {
			return this->x_avr();
		}
	}
	else {
		return this->x_avr();
	}
}
// counld not ignore the detail even though the b_ is too small
double Line2dFitted::SignedDist(double x, double y) {
	if (!IsZero(c_)) {
		if (!IsZero(b_)) {
			if (!IsZero(a_)) {
				if (!IsZero(a_ / b_)) {
					return ((a_ * x + b_ * y + c_) / sqrt_a2b2);
				}
				else {
					return x - this->x_avr();
				}
			}
			else {
				return y - this->y_avr();
			}
		}
		else if (!IsZero(a_)) {
			return x -  this->x_avr();
		}
		else {
			if (!IsZero(a_ / b_)) {
				return ((a_ * x + b_ * y + c_) / sqrt_a2b2);
			}
			else {
				return x - this->x_avr();
			}
		}
	}else{
		return ((a_ * x + b_ * y + c_) / sqrt_a2b2);
	}
	
}
double Line2dFitted::Dist(double x, double y){
    return abs(SignedDist(x, y));
}
double Line2dFitted::DistClipped(double x, double y, double epsilon){
    auto dist = SignedDist(x, y);
    epsilon = abs(epsilon);
    if(dist > epsilon){
        return dist - epsilon;
    }else if(dist < -epsilon){
        return dist + epsilon;
    }else{
        return 0;
    }
}
double Line2dFitted::slope(){
	if (this->b() < 0.001) {
		return INFINITY;
	}else {
		return -a_ / b_;
	}
	
}
double Line2dFitted::intercept(){
    return -c_ / b_;
}
void Line2dFitted::update(double n_new, double x_avr_new,
    double y_avr_new, double xy_avr_new, double xx_avr_new){
    n_ = n_new;
    x_avr_ = x_avr_new;
    y_avr_ = y_avr_new;
    xx_avr_ = xx_avr_new;
    xy_avr_ = xy_avr_new;
    CalcCoef();
}
void Line2dFitted::Reset(){
    n_ = 0.0;
    a_ = 0.0;
    b_ = 0.0;
    c_ = 0.0;
    x_avr_ = 0.0;
    y_avr_ = 0.0;
    xx_avr_ = 0.0;
    xy_avr_ = 0.0;
}
void Line2dFitted::Reset(double x1, double y1, double x2, double y2){
    n_ = 2;
    x_avr_ = (x1 + x2) / 2;
    y_avr_ = (y1 + y2) / 2;
    xx_avr_ = (x1*x1 + x2 * x2) / 2;
    xy_avr_ = (x1*y1 + x2 * y2) / 2;
    CalcCoef();
}
void Line2dFitted::Reset(std::vector<Pillar> &pillars, int start, int end){
    if(start < 0){ start = 0; }
    if(end >= pillars.size()){ end = pillars.size(); }
    if(start > end){ std::swap(start, end); }
    n_ = static_cast<double>(end - start + 1);
    double x_sum = 0, y_sum = 0, xx_sum = 0, xy_sum = 0;
    double x_temp, y_temp;
    for (auto i = start; i <= end; i++){
        x_temp = pillars[i].x();
        y_temp = pillars[i].y();
        x_sum += x_temp;
        y_sum += y_temp;
        xx_sum += x_temp * x_temp;
        xy_sum += x_temp * y_temp;
    }
    x_avr_ = x_sum / n_;
    y_avr_ = y_sum / n_;
    xx_avr_ = xx_sum / n_;
    xy_avr_ = xy_sum / n_;
    CalcCoef();
}
void Line2dFitted::Reset(std::vector<double> vec, int start, int end){
    if(start < 0){ start = 0; }
    if(end >= vec.size()){ end = vec.size(); }
    if(start > end){ std::swap(start, end); }
    n_ = static_cast<double>(end-start+1);
    double x_sum = 0, y_sum = 0, xx_sum = 0, xy_sum = 0;
    for(int i=start; i<=end; i++){
        x_sum += i;
        y_sum += vec[i];
        xx_sum += i*i;
        xy_sum += static_cast<double>(i)*vec[i];
    }
    x_avr_ = x_sum / n_;
    y_avr_ = y_sum / n_;
    xx_avr_ = xx_sum / n_;
    xy_avr_ = xy_sum / n_;
    CalcCoef();
}
int Line2dFitted::n(){
    return static_cast<int>(n_);
}
double Line2dFitted::x_avr(){
    return x_avr_;
}
double Line2dFitted::y_avr(){
    return y_avr_;
}
double Line2dFitted::xx_avr(){
    return xx_avr_;
}
double Line2dFitted::xy_avr(){
    return xy_avr_;
}
double Line2dFitted::a() {
	return a_;
}
double Line2dFitted::b() {
	return b_;
}
double Line2dFitted::c() {
	return c_;
}
void Line2dFitted::Print(){
    std::cout << "line: "
        << std::setprecision(2) << a_ << " * x + "
        << std::setprecision(2) << b_ << " * y + "
        << std::setprecision(2) << c_ << " = 0,\t\t";
    std::cout << "x_avr_ = " << x_avr_ << ", y_avr_ = " << y_avr_ << ", xx_avr_ = " << xx_avr_ << ", xy_avr_ = " << xy_avr_ << "\n";
}
void Line2dFitted::CalcCoef(){
    a_ = x_avr_ * y_avr_ - xy_avr_;
    b_ = xx_avr_ - x_avr_ * x_avr_;
    c_ = x_avr_ * xy_avr_ - xx_avr_ * y_avr_;
	sqrt_a2b2 = sqrt(a_ * a_ + b_ * b_);
}
bool Line2dFitted::IsZero(double x){
    return abs(x) < 1.0e-3;
}

} // namespace droneworld
