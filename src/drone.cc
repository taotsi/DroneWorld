#pragma once

#include <memory>
#include <string>
#include <fstream>
#include <sstream>
#include <experimental/filesystem>
#include "nlohmann/json.hpp"
#include "drone.h"
#include "world.h"
#include "droneworld_utility.h"

namespace droneworld{

using json = nlohmann::json;
namespace fs = std::experimental::filesystem;

Drone::Drone(std::string name, std::shared_ptr<Config> conf)
  : name_{name}, config_{conf} {
    // TODO: maybe I should use shared_ptr for the data queues
	movement_ = std::make_shared<MovementComponent>(config_);
	image_input_ = std::make_shared<ImageInputComponent>(config_);
	stixel_ = std::make_shared<StixelComponent>(
		&(image_input_->scaled_disparity_frame_queue_),
        &(image_input_->scene_mat_queue_),
        &point_cloud_, config_, name_);
    cluster_ = std::make_shared<PillarClusterComponent>(
        &(stixel_->super_pillar_frame_queue_));
    compact_ = std::make_shared<CompactPlaneComponent>(
        &(cluster_->pillar_cluster_queue_), &compact_model_);
    // refine_ = std::make_unique<RefinePlaneComponent>(
    //     &(compact_->planes_));
    // cv::namedWindow(name_);
}

Drone::~Drone() {
    movement_->Stop();
    image_input_->Stop();
    stixel_->Stop();
    cluster_->Stop();
    compact_->Stop();
    // refine_->Stop();
    // cv::destroyWindow(name_);
}

void Drone::Begin() {
    dwinfo(config_->mode(), ", ", config_->play_mode());
    switch(config_->mode()){
        case ConfigMode::kOnline:{
            if(config_->play_mode() == PlayMode::kSingleFrame){
                should_go_on_ = false;
            }
            movement_->Begin();
            image_input_->Begin();
            stixel_->Begin();
            cluster_->Begin();
            compact_->Begin();
            // refine_->Begin();
            break;
        }
        case ConfigMode::kRecord:{
            image_input_->Begin();
            movement_->Begin();
            break;
        }
        case ConfigMode::kOffline:{
            image_input_->Begin();
            stixel_->Begin();
            cluster_->Begin();
            compact_->Begin();
            // refine_->Begin();
            break;
        }
        default:{
            dwtrap("No such a mode!");
        }
    }
}
void Drone::Update(double delta_time) {
    switch(config_->mode()){
        case ConfigMode::kOnline:{
            if(config_->play_mode() == PlayMode::kAuto){
                movement_->Update(delta_time);
                if(movement_->IsBusy()){
                    static bool is_first = true;
                    if(!is_first){ QueuePop(); }
                    else{ is_first = false; }
                    image_input_->Update(delta_time);
                    stixel_->Update(delta_time);
                    cluster_->Update(delta_time);
                    compact_->Update(delta_time);
                    // refine_->Update(delta_time);
                    is_point_cloud_updated_ = true;
                }
            }else{ // kSingleFrame mode
                if(should_go_on_){
                    should_go_on_ = false;
                    static bool is_first = true;
                    if(!is_first){ QueuePop(); }
                    else{ is_first = false; }
                    dwmsg("online singleframe, go on now...");
                    Timer t;
                    t.tStart();
                    image_input_->Update(delta_time);
                    stixel_->Update(delta_time);
                    cluster_->Update(delta_time);
                    compact_->Update(delta_time);
                    t.tPutNow();
                    // refine_->Update(delta_time);
                    is_point_cloud_updated_ = true;
                }
            }
            break;
        }
        case ConfigMode::kRecord:{
            static bool flag = false;
            if(movement_->IsBusy()){
                if(!flag){
                    flag = true;
                    dwinfo("Start recording");
                }
                image_input_->Update(delta_time);
            }else{
                if(flag){
                    flag = false;
                    dwinfo("Stop recording");
                }
            }
            break;
        }
        case ConfigMode::kOffline:{
            if(config_->play_mode() == PlayMode::kAuto){
                static bool is_first = true;
                if(!is_first){ QueuePop(); }
                else{ is_first = false; }
                image_input_->Update(delta_time);
                Timer::Mute();
                Timer t;
                t.tStart();
                stixel_->Update(delta_time);
                cluster_->Update(delta_time);
                compact_->Update(delta_time);
                Recorder::Add(t.tPutNow());
                Timer::UnMute();
                // refine_->Update(delta_time);
                // stixel_->DrawStixelShow(10);
                is_point_cloud_updated_ = true;
            }else{  // kSingleFrame mode
                if(should_go_on_){
                    should_go_on_ = false;
                    static bool is_first = true;
                    if(!is_first){ QueuePop(); }
                    else{ is_first = false; }
                    image_input_->Update(delta_time);
                    stixel_->Update(delta_time);
                    cluster_->Update(delta_time);
                    compact_->Update(delta_time);
                    // refine_->Update(delta_time);
					// stixel_->DrawStixelShow(10);
                    is_point_cloud_updated_ = true;
                }
            }
            break;
        }
        default:{
            dwtrap("No such a mode!");
        }
    }
}

std::vector<Plane>& Drone::GetCompactModelData(){
    return compact_model_;
}
std::vector<Point3D>& Drone::GetPointCloudData(){
    is_point_cloud_updated_ = false;
    return point_cloud_;
}
bool Drone::is_point_cloud_updated(){
    return static_cast<bool>(is_point_cloud_updated_);
}

void Drone::GoOn(){
    should_go_on_ = true;
}
void Drone::QueuePop(){
    stixel_->QueuePop();
    cluster_->QueuePop();
    compact_->QueuePop();
    // refine_->QueuePop();
}
bool Drone::is_dataset_all_run(){
    return image_input_->idx_frame() > config_->stop_idx();
}

void Drone::stixel_width(int w){
    image_input_->stixel_width(w);
    stixel_->stixel_width(w);
}
void Drone::pillar_fit_max_distance(int d){
    // TODO:
    dwwarn("empty function here, define me first");
}
void Drone::disparity_noise(double n){
    // TODO:
    dwwarn("empty function here, define me first");
}

} // namespace droneworld
