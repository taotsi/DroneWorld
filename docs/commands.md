
## Commands

- `exit`

- `ls`

    List the drones available right now. Currently there's only one drone, so this command is useless.

- `select [drone_name]`

    Select the drone you'd like to control. Currently there's only one drone, so this command is useless.

- `go x y z`

    For example, `go 0 5 2` means flying to point (0, 5, 2). ENU coordinate system, +x, +y and +z means east, north, and up.

- `setspd [speed_val]`

- `takeoff`

- `land`

- `pose`

    print the position and pose angle of the selected drone.

- `disp [column_index_number]`

    `disp 45` for example, prints the 45th column of a diparity frame.

- `kde [column_index_number]`

    `kde 45` for example, prints the 45th column of a kde frame.

- `save`


For more commands, go to src/world.cc, see the definition of ProcessInput().